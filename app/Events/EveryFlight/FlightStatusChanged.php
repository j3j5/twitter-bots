<?php

namespace App\Events\EveryFlight;

use App\Models\EveryFlight\Flight;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class FlightStatusChanged
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public Flight $flight;
    public string $oldStatus;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Flight $flight, string $oldStatus)
    {
        $this->flight = $flight;
        $this->oldStatus = $oldStatus;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
