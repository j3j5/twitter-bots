<?php

namespace App\Services\Bicisenda;

use Amenadiel\JpGraph\Plot\IconPlot;
use Amenadiel\JpGraph\Plot\LinePlot;
use Amenadiel\JpGraph\Plot\ScatterPlot;
use Amenadiel\JpGraph\Text\Text;
use App\Graphs\Themes\BiciTheme;
use App\Traits\Graphs\GeneratesGraphs;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use RuntimeException;

/** @property \App\Graphs\Themes\BiciTheme $theme  */
class Graphs
{
    use GeneratesGraphs;

    private int $width;
    private int $height;

    // public function generateHourlyChartWithAvg(Collection $data, array $labels, int $width, int $height, string $title = '', string $subtitle = '', string $subsubtitle = '')
    // {
    //     // Reset the theme on each graph
    //     $this->setThemeAndSize($width, $height);

    //     $max = $data->map(fn (Collection $data) => $data->max())->max();

    //     $barData = [];
    //     // Average values go as line
    //     foreach($data->keys() as $key) {
    //         if (Str::startsWith(haystack: $key, needles: 'Media')) {
    //             $barData[$key] = $data->get($key)->values()->toArray();
    //             $data->forget($key);
    //         }
    //     }

    //     $graph = $this->createGraph($width, $height);

    //     $this->addWeather($data, $labels, $max, $graph);

    //     // Get manual tick every second hour
    //     $graph->SetTickDensity(TICKD_VERYSPARSE);

    //     list($majTickPos, $minTickPos) = $this->getHourlyTicks($labels);
    //     $graph->xaxis->SetTickPositions($majTickPos, $minTickPos);
    //     $graph->xaxis->SetLabelFormatCallback(fn (int $ts) => Carbon::createFromTimestamp($ts, 'America/Montevideo')->format('H:i'));

    //     $icon = $this->getLicenseIcon($width);
    //     $graph->AddIcon($icon);

    //     // $logo = $this->getLogoIcon();
    //     // $graph->AddIcon($logo);

    //     return $this->getGraphImage($graph);
    // }

    // public function generateStackedBarchartWithLine(Collection $data, array $labels, int $width, int $height, string $title = '', string $subtitle = '', string $subsubtitle = '')
    // {
    //     $this->setThemeAndSize($width, $height);

    //     $lineData = [];
    //     if ($data->has('Media')) {
    //         /** @phpstan-ignore-next-line */
    //         $lineData = $data->get('Media')->values()->toArray();
    //         $data->forget('Media');
    //     }

    //     $graph = $this->createGraph($width, $height);
    //     $graph = $this->createAccBarChart($data->toArray(), $labels, $title, $subtitle, $subsubtitle, '', '', 'textlin', $graph);

    //     if (!empty($lineData)) {
    //         $demandLine = new LinePlot($lineData);
    //         $demandLine->SetLegend('Media total bicis últimas 4 semanas');

    //         $graph->Add($demandLine);
    //     }

    //     $graph->SetTickDensity(TICKD_VERYSPARSE);
    //     $icon = $this->getLicenseIcon($width);
    //     $graph->AddIcon($icon);

    //     $logo = $this->getLogoIcon();
    //     $graph->AddIcon($logo);

    //     return $this->getGraphImage($graph);
    // }

    public function generateGroupedBarChartWithLine(
        Collection $data,
        array $labels,
        int $width,
        int $height,
        string $title = '',
        string $subtitle = '',
        string $subsubtitle = '',
        bool $addWeather = false,
        string $type = 'daily'
    ) {
        $this->setThemeAndSize($width, $height);

        $max = $data->map(fn (Collection $data) => $data->max())->max();

        $lines = [];
        // Average values go as line
        foreach($data->keys() as $key) {
            if (Str::startsWith(haystack: $key, needles: 'Media')) {
                $lines[$key] = $data->get($key)->values()->toArray();
                $data->forget($key);
            }
        }

        $graph = $this->createGraph($this->width, $this->height);
        $labelsText = $labels;
        $xScale = 'intlin';
        if ($type === 'daily') {
            $labelsText = collect($labels)->map(
                fn (int $ts) => Carbon::createFromTimestamp($ts, 'America/Montevideo')
                    ->locale('es_UY')
                    ->isoFormat('ddd D MMM')
            )->all();
            $xScale = 'textlin';

        } elseif ($type === 'hourly') {
            $labelsText = collect($labels)->map(
                fn (int $ts) => Carbon::createFromTimestamp($ts, 'America/Montevideo')
                    ->format('H:i')
            )->all();
            $xScale = 'textlin';
        }

        $graph = $this->createBarChart(
            data: $data->all(),
            labels: $labelsText,
            title: $title,
            subtitle: $subtitle,
            subsubtitle: $subsubtitle,
            xScale: $xScale,
            graph: $graph
        );

        if (!empty($lines)) {
            foreach($lines as $key => $lineData) {
                $avgLine = new LinePlot($lineData);
                if (is_string($key)) {
                    $avgLine->SetLegend($key);
                }
                $graph->Add($avgLine);
            }
        }

        if ($addWeather) {
            $this->addWeather(data: $data, labels: $labels, max: $max, graph: $graph, type: $type);
        }

        $graph->SetTickDensity(TICKD_VERYSPARSE);

        if ($type === 'hourly') {
            $graph->xaxis->SetTextLabelInterval(2);
        }

        $icon = $this->getLicenseIcon($width);
        $graph->AddIcon($icon);

        // $logo = $this->getLogoIcon();
        // $graph->AddIcon($logo);

        return $this->getGraphImage($graph);
    }

    public function generateWeeklyLineGraph(Collection $data, int $width = 1500, int $height = 500, string $title = '', string $subtitle = '', string $subsubtitle = '', string $bottomMsg = '') : string
    {
        // Get the labels
        $labels = $data->first()->keys()->values()->all();

        $lineData = [];
        if ($data->has('Media')) {
            $lineData = $data->get('Media')->values()->toArray();
            $data->forget('Media');
        }

        $data = $data->mapWithKeys(
            // Remove timestamps from the keys
            fn (Collection $series, string $key) => [$key => $series->values()]
        )->mapWithKeys(fn (Collection $series, string $key) => [
            // Make 0 small values for a cleaner graph
            $key => $series->map(
                fn ($value) => $value < 10 ? 0 : $value
            ),
        ])->toArray();

        $graph = $this->createGraph($width, $height);
        $graph = $this->createStackedLinePlot($data, $labels, $title, $subtitle, $subsubtitle, $graph);
        if (!empty($lineData)) {
            $demandLine = new LinePlot($lineData, $labels);
            $demandLine->SetLegend('Media');
            $graph->Add($demandLine);
        }

        $start = Carbon::createFromTimestamp($labels[0]);
        $end = Carbon::createFromTimestamp($labels[count($labels) - 1]);
        if ($start->diffInDays($end) < 2) {
            list($majTickPos, $minTickPos) = $this->getCustomIntervalTicks($labels, CarbonInterval::fromString('2 hours'), 'America/Montevideo');
            $graph->xaxis->SetLabelFormatCallback(
                fn ($ts) => Carbon::createFromTimestamp($ts, 'America/Montevideo')->format('H:i')
            );
        } else {
            list($majTickPos, $minTickPos) = $this->getCustomIntervalTicks($labels, CarbonInterval::fromString('24 hours'), 'America/Montevideo');

            $graph->xaxis->SetLabelFormatCallback(
                fn ($ts) => Carbon::createFromTimestamp($ts, 'America/Montevideo')->locale('es_UY')->isoFormat('D MMM')
            );
        }

        $graph->xaxis->SetTickPositions($majTickPos, $minTickPos);
        $graph->yaxis->SetTitle('Bicis por día', 'middle');

        $license = $this->getLicenseIcon($width);
        $graph->AddIcon($license);

        // $logo = $this->getLogoIcon();
        // $graph->AddIcon($logo);

        // $updatedTxt = new Text($bottomMsg);
        // $updatedTxt->SetPos($width - 80, $height - 20, 'right');
        // $updatedTxt->SetFont(FF_USERFONT3, FS_NORMAL, 11);
        // $graph->Add($updatedTxt);

        return $this->getGraphImage($graph);
    }

    public function generateWeeklyGroupBarChart(Collection $data, array $labels, string $title, string $subtitle, string $subsubtitle)
    {
        $xLabel = 'Día';
        $yLabel = 'Bicis';

        $lineData = [];
        if ($data->has('Media')) {
            $lineData = $data->get('Media');
            $data->forget('Media');
        }

        $graph = $this->createGraph();

        $graph = $this->createBarChart(
            data: $data->all(),
            labels: $labels,
            title: $title,
            subtitle: $subtitle,
            subsubtitle: $subsubtitle,
            xTitle: $xLabel,
            yTitle: $yLabel,
            graph: $graph
        );

        if (!empty($lineData)) {
            $demandLine = new LinePlot($lineData, $labels);
            $demandLine->SetLegend('Media');
            $graph->Add($demandLine);
        }
        // $graph->SetScale('textlin');

        $graph->SetTickDensity(TICKD_SPARSE);
        $graph->yaxis->SetLabelFormatCallback(fn ($tick) => sprintf('%d', $tick));

        $icon = $this->getLicenseIcon();
        $graph->AddIcon($icon);

        return $this->getGraphImage($graph);
    }

    private function addWeather(Collection $data, array $labels, float $max, string $type = 'daily', &$graph)
    {
        $weather = new ScatterPlot(array_fill(0, $data->first()->count(), $max * 1.2));
        $weather = new LinePlot(array_fill(0, $data->first()->count(), $max * 1.2));

        // Add an empty image mark as default
        $weather->mark->SetType(MARK_IMG, resource_path('images/empty.png'), 1);
        $weather->setColor('#ffffff@0.0');
        $weather->SetBarCenter();
        $weather->SetWeight(0);

        if ($type === 'daily') {
            $weatherSeries = $this->getDailyWeatherIconSeries(collect($labels));
        } elseif ($type === 'hourly') {
            $weatherSeries = $this->getHourlyWeatherIconSeries(collect($labels));
        } else {
            throw new RuntimeException("unsupported type $type");
        }

        $weather->mark->SetCallbackYX(function ($y, $x) use ($weatherSeries, $labels) {
            $icon = $weatherSeries->get($labels[$x]);
            $width = '';
            $color = '';
            $fcolor = '';
            $imgscale = 1;
            $filename = '';
            if (empty($icon)) {
                return [$width, $color, $fcolor, $filename, $imgscale];
            }

            return [$width, $color, $fcolor, $icon, $imgscale];
        });
        $graph->Add($weather);

        $tempAndWind = new LinePlot(array_fill(0, $data->first()->count(), $max * 1.135));
        $tempAndWind->mark->SetType(MARK_IMG, resource_path('images/empty.png'), 1);
        $tempAndWind->setColor('#ffffff@0.0');
        $tempAndWind->SetBarCenter();
        $tempAndWind->SetWeight(0);

        if ($type === 'daily') {
            $tempAndWindSeries = $this->getDailyTempAndWindSeries(collect($labels));
        } elseif ($type === 'hourly') {
            $tempAndWindSeries = $this->getHourlyTempAndWindSeries(collect($labels));
        } else {
            throw new RuntimeException("unsupported type $type");
        }
        $tempAndWind->value->show();
        $tempAndWind->value->SetFont(FF_USERFONT2, FS_NORMAL, 12);
        $tempAndWind->value->SetColor($this->theme->fontColor);
        $tempAndWind->value->SetFormatCallback(function ($value) use ($tempAndWindSeries, $type) : string {
            $text = (string) $tempAndWindSeries->shift();

            if ($type === 'hourly' && $tempAndWindSeries->count() % 3 !== 0) {
                return '';
            }
            return $text;
        });

        $graph->Add($tempAndWind);
    }

    private function getLicenseIcon() : IconPlot
    {
        // Add CC icon
        $icon = new IconPlot(resource_path('images/cc-by-sa.png'));
        $icon->SetScale(0.45);
        $icon->SetMix(90);
        $icon->SetAnchor('right', 'top');
        $icon->SetPos($this->width - $this->theme->rightMargin, 35);

        return $icon;
    }

    private function getLogoIcon() : IconPlot
    {
        // Add CC icon
        $icon = new IconPlot(resource_path('images/uvindex/logo-dwi.png'));
        $icon->SetScale(0.12);
        $icon->SetMix(90);
        $icon->SetAnchor('left', 'top');
        $icon->SetPos(140, 35);

        return $icon;
    }

    private function setThemeAndSize(int $width, int $height) : void
    {
        $this->theme = new BiciTheme();
        $this->width = $width;
        $this->height = $height;
    }
}
