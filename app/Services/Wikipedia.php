<?php

namespace App\Services;

use Exception;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use RuntimeException;

class Wikipedia
{
    public readonly string $baseUrl;
    public readonly string $userAgent;

    private array $parameters = [];

    public function __construct(string $languageDomain = 'en')
    {
        $this->baseUrl = "http://$languageDomain.wikipedia.org/w/api.php";
        $this->userAgent = 'fedibots by @j3j5@hachyderm.io | https://gitlab.com/j3j5 | https://j3j5.uy';

        $this->parameters = [
            'action' => 'query',
            'redirects' => true,
            'format' => 'json',
            'formatversion' => 2,
            'utf8' => '',
            'maxlag' => 2,
        ];
    }

    public function parameter(string $key, mixed $value) : self
    {
        $this->parameters[$key] = $value;
        return $this;
    }

    public function parameters(array $parameters = null) : array|self
    {
        if ($parameters === null) {
            return $this->parameters;
        }
        $this->parameters = array_merge($this->parameters, $parameters);

        return $this;
    }

    public function get() : Response
    {
        return retry(
            5,
            $this->doCall(...),
            fn (int $attempt, Exception $e) => $attempt * 1000, // 1s per attempt tried
            fn (Exception $e) => $e->getMessage() !== 'No more pages' // Ignore pagination exceptions
        );
    }

    private function doCall() : Response
    {
        $response = Http::withHeaders([
            'User-Agent' => $this->userAgent,
            'Content-Encoding' => 'UTF-8',
            'Accept-Encoding' => 'gzip, deflate, br',
        ])->acceptJson()
            ->get($this->baseUrl, $this->parameters);

        // Handle maxlag waiting and retrying
        if ($response->json('error.code') === 'maxlag') {
            throw new RuntimeException($response->json('error.info'));
        }

        return $response;
    }

    public function nextPage(Response $response) : Response
    {
        if (!$response->offsetExists('continue')) {
            throw new RuntimeException('No more pages');
        }
        return $this->parameters($response->offsetGet('continue'))->get();
    }

}
