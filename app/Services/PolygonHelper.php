<?php

namespace App\Services;

use proj4php\Point;
use RuntimeException;

class PolygonHelper
{
    /**
     * Is the given point inside the given Polygon?
     *
     * See https://stackoverflow.com/a/18190354
     *
     * @param \proj4php\Point $p
     * @param \proj4php\Point[] $polygon
     * @return bool
     */
    public function pointInPolygon(Point $p, array $polygon) : bool
    {
        if (!isset($polygon[0])) {
            throw new RuntimeException('Polygon is empty');
        }

        $c = 0;
        $p1 = $polygon[0];
        $n = count($polygon);

        for ($i = 1; $i <= $n; $i++) {
            $p2 = $polygon[$i % $n];
            if ($p->x > min($p1->x, $p2->x)
                && $p->x <= max($p1->x, $p2->x)
                && $p->y <= max($p1->y, $p2->y)
                && $p1->x != $p2->x) {
                $xinters = ($p->x - $p1->x) * ($p2->y - $p1->y) / ($p2->x - $p1->x) + $p1->y;
                if ($p1->y == $p2->y || $p->y <= $xinters) {
                    $c++;
                }
            }
            $p1 = $p2;
        }
        // if the number of edges we passed through is even, then it's not in the poly.
        return $c % 2 != 0;
    }
}
