<?php

namespace App\Services\Meteobot;

use App\Services\UserAgent;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use RuntimeException;

/**
 * @phpstan-type Station array{id: int, name: string, location: array{lat: numeric-string, long: numeric-string}, department: string}
 */
class AmbienteUY
{
    private const BASE_URL = 'https://www.ambiente.gub.uy/consultas/index.php/consultas/mediciones_aire';

    // Latitud: -34.876107, Longitud: -56.038025
    //  Start: 29 de Septiembre de 2017
    private const BARRADAS = 137;

    // Latitud: -31.878337, Longitud: -54.159444
    private const ACEGUA = 112;

    // Latitud: -33.161635, Longitud: -58.353919
    private const LAS_CAÑAS = 113;

    // Latitud: -34.164019, Longitud: -58.033948
    // private const MONTES_PLATA_CONCHILLAS = 111;

    // Latitud: -34.2305, Longitud: -58.0502
    // Last data from 2021-06-30 12:00:00
    // private const MONTES_PLATA_PUERTO_INGLES = 103;

    // Latitud: -33.117877, Longitud: -58.290852
    // private const UPM = 102;

    /** @var array<int, Station> */
    private array $stations = [
        [
            'id' => self::ACEGUA,
            'name' => 'Estación Aceguá',
            'location' => [
                'lat' => '-31.878337',
                'long' => '-54.159444',
            ],
            'department' => 'Cerro Largo',
        ],
        [
            'id' => self::BARRADAS,
            'name' => 'Estación Barradas',
            'location' => [
                'lat' => '-34.876107',
                'long' => '-56.038025',
            ],
            'department' => 'Montevideo',
        ],
        [
            'id' => self::LAS_CAÑAS,
            'name' => 'Estación Las Cañas',
            'location' => [
                'lat' => '-33.161635',
                'long' => '-58.353919',
            ],
            'department' => 'Río Negro',
        ],
    ];

    public const HUMEDAD = 'Humedad (%)';
    public const LLUVIA = 'Precipitación (mm)';
    public const PRESION = 'Presión Atmosférica (hPa)';
    public const RADIACION = 'Radiación Solar (W/m²)';
    public const TEMP = 'Temperatura (ºC)';
    public const VIENTO_DIR = 'Viento - Dirección (º)';
    public const VIENTO_INT = 'Viento - Intensidad (nudos)';
    public const NITROGENO = 'Dióxido de Nitrógeno (µg/m³)';
    public const MATERIAL_AIRE = 'Material particulado en aire, tamaño menor a 10 um(µg/m³)';
    public const OZONO = 'Compuestos de Azufre Reducido Totales(µg/m³)';
    public const AZUFRE = 'Dióxido de Azúfre(µg/m3)';

    /** @return array<int, Station> */
    public function getStations() : array
    {
        return $this->stations;
    }

    /**
     *
     * @param int $station
     * @param \Illuminate\Support\Carbon $start
     * @param \Illuminate\Support\Carbon $end
     * @param int $limit
     * @param int $offset
     * @param string $order
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Exception
     * @throws \Illuminate\Http\Client\RequestException
     * @throws \RuntimeException
     * @return \Illuminate\Support\Collection<int, Collection<string, string>>
     */
    public function getData(int $station, Carbon $start, Carbon $end, int $limit = 100, int $offset = 0, string $order = 'asc') : Collection
    {
        $fields = collect([
            313 => self::VIENTO_INT,
            316 => self::PRESION,
            317 => self::LLUVIA,
            318 => self::RADIACION,
            2012 => self::HUMEDAD,
            2015 => self::MATERIAL_AIRE,
            2032 => self::TEMP,
            2033 => self::OZONO,
            2107 => self::AZUFRE,
            2229 => self::VIENTO_DIR,
            2230 => self::VIENTO_INT,
            2235 => self::NITROGENO,
        ]);

        $url = implode('/', [
            self::BASE_URL,
            $station,
            $fields->keys()->implode('_'),
            $start->format('d/m/Y/H_i'),
            $end->format('d/m/Y/H_i'),
        ]);

        $query = [
            'order' => $order,
            'offset' => $offset,
            'limit' => $limit,
        ];

        $response = Http::withOptions([
            'verify' => config('services.ambienteUy.certPath'),
        ])->withHeaders([
            'User-Agent' => resolve(UserAgent::class)->get(),
        ])->get($url, $query);

        $response->throw();

        $data = $response->json();
        if (!isset($data['rows'])) {
            throw new RuntimeException('Response did not contain rows');
        }
        /** @var array<int, array<string,string>> $rows */
        $rows = $data['rows'];
        return collect($rows)->map(
            fn (array $row) => collect($row)->filter()->mapWithKeys(
                fn (string $value, string $key) => $key === 'field_fecha'
                    ? ['Fecha' => $value]
                    : [$fields->get((int) str_replace('field_', '', $key)) => $value]
            )
        );
    }
}
