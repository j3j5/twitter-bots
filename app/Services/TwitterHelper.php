<?php

namespace App\Services;

use IntlBreakIterator;
use Normalizer;

use function Safe\mb_internal_encoding;
use function Safe\preg_replace;

class TwitterHelper
{
    public const URL_REGEX = '#https?://[-A-Za-z0-9+&@\#/%?=~_|!:,.;]+[-A-Za-z0-9+&@\#/%=~_|]#';

    /**
     * Replace chars that'll make twitter convert text to mentions, links or hashtags
     * @param string $text
     * @return string
     */
    public function safeText(string $text) : string
    {
        return str_replace(['.', '#', '@'], ['․', '♯', '﹫'], $text);
    }

    public function countChars(string $text) : int
    {
        if ($text === '') {
            return 0;
        }
        $text = preg_replace(self::URL_REGEX, str_pad('', 23, '*'), $text);
        // Get the normalized text in UTF-8
        $normalizedText = Normalizer::normalize($text, Normalizer::FORM_C);
        if (!is_string($normalizedText)) {
            return 0;
        }

        // Now we can calculate the number of codepoints in this normalized text
        $it = IntlBreakIterator::createCodePointInstance();
        $it->setText($normalizedText);
        $len = 0;
        foreach ($it as $codePoint) {
            $len++;
        }
        return $len;
    }

    public function mbStrrev(string $string, string $encoding = null) : string
    {
        if ($encoding === null) {
            $encoding = mb_detect_encoding($string);
            if (!$encoding) {
                // TODO: report error on phpstan, no parameter it returns string
                $encoding = mb_internal_encoding();
                if (!is_string($encoding)) {
                    $encoding = 'UTF-8';
                }
            }
        }

        $length = mb_strlen($string, $encoding);
        $reversed = '';
        while ($length-- > 0) {
            $reversed .= mb_substr($string, $length, 1, $encoding);
        }

        return $reversed;
    }

    public function splitTextIntoTweets(string $text, int $maxLength = 265) : array
    {
        $tweets = [];
        $tweet = '';

        $originalText = collect(mb_str_split($text))->reverse();
        while ($originalText->isNotEmpty()) {
            /** @phpstan-ignore-next-line */
            while ($this->countChars($tweet) <= $maxLength && $originalText->isNotEmpty()) { // Being on the safe side
                $tweet .= $originalText->pop();
            }
            // Another tweet is coming, cut the string on the previous word
            /** @phpstan-ignore-next-line */
            if ($originalText->isNotEmpty()) {
                $lastSpacePos = mb_strrpos($tweet, ' ');
                if ($lastSpacePos !== false) {
                    $remaining = trim(mb_substr($tweet, $lastSpacePos));
                    $tweet = mb_substr($tweet, 0, $lastSpacePos);
                }

                if (!empty($remaining)) {
                    $remainingArray = mb_str_split($this->mbStrrev($remaining));
                    foreach ($remainingArray as $char) {
                        $originalText->push($char);
                    }
                }
            }
            $tweets[] = $tweet;
            $tweet = '';
        }

        if (count($tweets) > 1) {
            foreach ($tweets as $i => &$tweet) {
                $tweet .= ' 🧵(' . ($i + 1) . '/' . count($tweets) . ')';
            }
        }

        return $tweets;
    }
}
