<?php

namespace App\Services\UTE;

use Amenadiel\JpGraph\Plot\IconPlot;
use Amenadiel\JpGraph\Plot\LinePlot;
use Amenadiel\JpGraph\Plot\PlotLine;
use Amenadiel\JpGraph\Text\Text;
use App\Graphs\Themes\UTETheme;
use App\Traits\Graphs\GeneratesGraphs;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Support\Collection;
use Imagick;
use ImagickDraw;
use ImagickPixel;
use Intervention\Image\Facades\Image;

class Graphs
{
    use GeneratesGraphs;

    public function getDistributionPie(array $data, string $midTitle = '', Carbon $lastUpdated = new Carbon()) : string
    {
        $this->theme = new UTETheme();
        $values = array_values($data);
        $labels = array_map(fn (string $label) => $label . "\n%.1f%%", array_keys($data));

        $title = '';
        $graph = $this->createPieGraph(400, 400);
        $graph = $this->createPieChartC($values, $labels, $title, $midTitle, $graph);

        $blob = $this->getGraphImage($graph);
        return $this->addLastUpdatedAndIcon($lastUpdated, $blob);
    }

    public function getHistoricalDistributionLine(Collection $data, int $width = 1500, int $height = 500, string $title = '', string $subtitle = '', string $subsubtitle = '', string $bottomMsg = '') : string
    {
        $this->theme = new UTETheme();
        // Get the labels
        $labels = $data->first()->keys()->values()->all();

        $lineData = [];
        if ($data->has('Demanda')) {
            $lineData = $data->get('Demanda')->values()->toArray();
            $data->forget('Demanda');
        }

        $data = $data->mapWithKeys(
            // Remove timestamps from the keys
            fn (Collection $series, string $key) => [$key => $series->values()]
        )->mapWithKeys(fn (Collection $series, string $key) => [
            // Make 0 small values for a cleaner graph
            $key => $series->map(
                fn ($value) => $value < 10 ? 0 : $value
            ),
        ])->toArray();

        $graph = $this->createGraph($width, $height);
        $graph = $this->createStackedLinePlot($data, $labels, $title, $subtitle, $subsubtitle, $graph);

        if (!empty($lineData)) {
            $demandLine = new LinePlot($lineData, $labels);
            $demandLine->SetLegend('Demanda');
            $graph->Add($demandLine);
        }

        $start = Carbon::createFromTimestamp($labels[0]);
        $end = Carbon::createFromTimestamp($labels[count($labels) - 1]);
        if ($start->diffInDays($end) < 2) {
            list($majTickPos, $minTickPos) = $this->getCustomIntervalTicks($labels, CarbonInterval::fromString('2 hours'), 'America/Montevideo');
            $graph->xaxis->SetLabelFormatCallback(
                fn ($ts) => Carbon::createFromTimestamp($ts, 'America/Montevideo')->format('H:i')
            );
        } else {
            list($majTickPos, $minTickPos) = $this->getCustomIntervalTicks($labels, CarbonInterval::fromString('24 hours'), 'America/Montevideo');

            $graph->xaxis->SetLabelFormatCallback(
                fn ($ts) => Carbon::createFromTimestamp($ts, 'America/Montevideo')->locale('es_UY')->isoFormat('D MMM')
            );
        }

        $graph->xaxis->SetTickPositions($majTickPos, $minTickPos);
        $graph->yaxis->SetTitle('MW', 'middle');

        $license = $this->getLicenseIcon($width);
        $graph->AddIcon($license);

        $logo = $this->getLogoIcon();
        $graph->AddIcon($logo);

        $updatedTxt = new Text($bottomMsg);
        $updatedTxt->SetPos($width - 80, $height - 20, 'right');
        $updatedTxt->SetFont(FF_USERFONT3, FS_NORMAL, 11);
        $graph->Add($updatedTxt);

        return $this->getGraphImage($graph);
    }

    public function getSpotLine(Collection $data, int $width = 1920, int $height = 1080, string $title = '', string $subtitle = '', string $subsubtitle = '') : string
    {
        $this->theme = new UTETheme();
        $labels = $data->first()->keys()->toArray();

        $series = $data->mapWithKeys(
            // Remove timestamps from the keys
            fn (Collection $series, string $key) => [$key => $series->values()]
        )->toArray();

        $graph = $this->createGraph($width, $height);
        // Add line plot
        $graph = $this->createLinePlot($series, $labels, $title, $subtitle, $subsubtitle, 'datlin', null, [1, 1], $graph);
        // $graph->SetTickDensity(TICKD_SPARSE);
        $graph->SetTickDensity(TICKD_VERYSPARSE);

        list($majTickPos, $minTickPos) = $this->getCustomIntervalTicks($labels, CarbonInterval::fromString('2 days'), 'America/Montevideo');
        $graph->xaxis->SetTickPositions($majTickPos, $minTickPos);
        $graph->xaxis->SetLabelFormatCallback(
            fn (int $ts) => Carbon::createFromTimestamp($ts, 'America/Montevideo')->locale('es_UY')->isoFormat('DD MMM')
        );

        $graph->yaxis->SetTitle('Precio en U$S/MWh', 'middle');
        $graph->yaxis->SetLabelFormatCallback(fn ($tick) => sprintf('%d', $tick));

        // Add average for the month
        $average = $data->first()->average();
        $avgLine = new PlotLine(HORIZONTAL, $average, '#998ec3', 5);
        $avgLine->SetLineStyle('dotted');
        $avgLine->SetLegend('Media (' . round($average, 2) . ' U$D/MWh)');
        $graph->Add($avgLine);

        // Add median for the month
        $median = $data->first()->median();
        $avgLine = new PlotLine(HORIZONTAL, $median, '#b35806', 5);
        $avgLine->SetLineStyle('dotted');
        $avgLine->SetLegend('Mediana (' . round($median, 2) . ' U$D/MWh)');
        $graph->Add($avgLine);

        $license = $this->getLicenseIcon($width);
        $graph->AddIcon($license);

        $logo = $this->getLogoIcon();
        $graph->AddIcon($logo);

        return $this->getGraphImage($graph);
    }

    public function getWeeklyChart(Collection $data, int $width, int $height, string $title = '', string $subtitle = '', string $subsubtitle = '') : string
    {
        $this->theme = new UTETheme();
        $labels = $data->first()->keys()->values()->map(
            fn ($ts) => Carbon::createFromTimestamp($ts, 'America/Montevideo')->locale('es_UY')->isoFormat('DD MMM')
        )->all();

        $data = $data->mapWithKeys(
            // Remove timestamps from the keys
            fn (Collection $series, string $key) => [$key => $series->values()]
        );

        $lineData = [];
        if ($data->has('Demanda')) {
            /** @phpstan-ignore-next-line */
            $lineData = $data->get('Demanda')->values()->toArray();
            $data->forget('Demanda');
        }

        $graph = $this->createGraph($width, $height);
        $graph = $this->createAccBarChart($data->toArray(), $labels, $title, $subtitle, $subsubtitle, '', '', 'textlin', $graph);

        if (!empty($lineData)) {
            // dd($lineData, $labels);
            $demandLine = new LinePlot($lineData);
            $demandLine->SetLegend('Demanda del día');

            $graph->Add($demandLine);
        }

        // $graph->SetTickDensity(TICKD_SPARSE);
        $graph->SetTickDensity(TICKD_VERYSPARSE);
        $graph->yaxis->SetLabelFormatCallback(fn ($tick) => sprintf('%d GW', $tick));
        $icon = $this->getLicenseIcon($width);
        $graph->AddIcon($icon);

        $logo = $this->getLogoIcon();
        $graph->AddIcon($logo);

        return $this->getGraphImage($graph);
    }

    private function getLicenseIcon(int $imgWidth) : IconPlot
    {
        // Add CC icon
        $icon = new IconPlot(resource_path('images/cc-by-sa.png'));
        $icon->SetScale(0.3);
        $icon->SetMix(90);
        $icon->SetAnchor('right', 'top');
        $icon->SetPos($imgWidth - 80, 60);

        return $icon;
    }

    private function getLogoIcon() : IconPlot
    {
        // Add logo icon
        $icon = new IconPlot(resource_path('images/ute/logo.png'));
        $icon->SetScale(0.9);
        $icon->SetMix(90);
        $icon->SetAnchor('left', 'top');
        $icon->SetPos(120, 20);

        return $icon;
    }

    private function addLastUpdatedAndIcon(Carbon $lastUpdated, string $binaryImage) : string
    {
        $img = Image::make($binaryImage);

        // Add flag watermark
        $img->insert(resource_path('images/ute/fist-uruguay-icon.png'), 'top-left', 146, 117);

        // Add last updated
        $lastUpdatedDate = $this->getDownwardsCurvedText($lastUpdated->format('Y-m-d'));
        $img->insert($lastUpdatedDate, 'top-left', 151, 85);
        $lastUpdatedTime = $this->getUpwardsCurvedText($lastUpdated->format('H:i'));
        $img->insert($lastUpdatedTime, 'top-left', 176, 300);

        return (string) $img->encode('png');
    }

    private function getDownwardsCurvedText(string $text) : Imagick
    {
        $draw = new ImagickDraw();

        $draw->setFont(config('graphs.font3-bold'));
        $draw->setFontSize(18);
        $draw->setStrokeAntialias(true);
        $draw->setTextAntialias(true);
        $draw->setFillColor(new ImagickPixel('#3f3f3f'));

        $textOnly = new Imagick();

        $textOnly->newImage(150, 35, 'transparent');
        $textOnly->setImageFormat('png');
        $x = 0;
        $y = 35;
        $textOnly->annotateImage($draw, $x, $y, 0, $text);
        $textOnly->trimImage(0);
        $textOnly->setImagePage($textOnly->getimageWidth(), $textOnly->getimageheight(), 0, 0);
        $distort = [45];
        $textOnly->setImageVirtualPixelMethod(Imagick::VIRTUALPIXELMETHOD_TRANSPARENT);
        $textOnly->setImageMatte(true);

        $textOnly->distortImage(Imagick::DISTORTION_ARC, $distort, true);
        $textOnly->setformat('png');

        return $textOnly;
    }

    private function getUpwardsCurvedText(string $text) : Imagick
    {
        $draw = new ImagickDraw();

        $draw->setFont(config('graphs.font3-bold'));
        $draw->setFontSize(18);
        $draw->setStrokeAntialias(true);
        $draw->setTextAntialias(true);
        $draw->setFillColor(new ImagickPixel('#3f3f3f'));

        $textOnly = new Imagick();
        $textOnly->newImage(110, 35, 'transparent');
        $textOnly->setImageFormat('png');
        $x = 0;
        $y = 35;
        $textOnly->annotateImage($draw, $x, $y, 0, $text);
        $textOnly->trimImage(0);
        $textOnly->setImagePage($textOnly->getimageWidth(), $textOnly->getimageheight(), 0, 0);

        $distort = [30, 180];
        $textOnly->setImageVirtualPixelMethod(Imagick::VIRTUALPIXELMETHOD_TRANSPARENT);
        $textOnly->setImageMatte(true);

        $textOnly->flipImage();
        $textOnly->distortImage(Imagick::DISTORTION_ARC, $distort, false);
        $textOnly->flopImage();

        return $textOnly;
    }
}
