<?php

namespace App\Services\UTE;

use App\Services\Google\GoogleSheets;
use App\Services\UserAgent;
use Carbon\Carbon;
use DiDom\Document;
use DiDom\Element;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use PhpOffice\PhpSpreadsheet\Reader\Exception as ReaderException;
use PhpOffice\PhpSpreadsheet\Reader\Ods;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use RuntimeException;
use Safe\Exceptions\FilesystemException;

use function Safe\unlink;

class ADME
{
    /** @var array<string, bool> */
    private array $filepaths;

    public function __construct(private UserAgent $userAgent, private Ods $reader)
    {
        $this->reader->setReadDataOnly(true);
    }

    public function __destruct()
    {
        if (!isset($this->filepaths)) {
            return;
        }

        foreach ($this->filepaths as $filepath => $val) {
            try {
                if (app()->isProduction()) {
                    unlink($filepath);
                }
            } catch(FilesystemException) {
            }
        }
    }

    /**
     * Download the file with the historical data for power generation on a 10 minute basis.
     *
     * @param string $filepath
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     * @throws \Exception
     * @throws \DiDom\Exceptions\InvalidSelectorException
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     * @throws \Illuminate\Http\Client\RequestException
     * @return void
     *
     */
    protected function downloadHistoricalTenMinutesFile(string $filepath, Carbon $start, Carbon $end) : void
    {
        $host = 'https://pronos.adme.com.uy';
        $baseUrl = $host . '/gpf.php';

        $params = [
            'fecha_ini' => $start->format('d/m/Y'),
            'fecha_fin' => $end->format('d/m/Y'),
            'send' => 'MOSTRAR',
        ];

        $response = Http::withHeaders([
            'User-Agent' => $this->userAgent->get(),
        ])->get($baseUrl, $params)->throw();

        $document = new Document($response->body());

        $anchor = $document->first('div > a');
        if (!$anchor instanceof Element) {
            throw new RuntimeException("Couldn't find the 10 min ODS file on the page");
        }
        $fileUrl = $host . $anchor->attr('href');
        $response = Http::withOptions([
            'sink' => $filepath,
        ])->get($fileUrl)->throw();
    }

    /**
     *
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Exception
     * @throws \DiDom\Exceptions\InvalidSelectorException
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     * @throws \Illuminate\Http\Client\RequestException
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Calculation\Exception
     * @throws \Safe\Exceptions\FilesystemException
     * @return \Illuminate\Support\Collection<string, \Illuminate\Support\Collection<int, float>>
     */
    public function getDataForInterval(Carbon $start, Carbon $end) : Collection
    {
        $spreadsheet = $this->readSpreadsheet($start, $end);

        $columns = [
            // "Fecha",
            // "Salto Grande",
            // "Bonete",
            // "Baygorria",
            // "Palmar",
            'Biomasa',
            'Solar',
            'Eólica',
            'Hidráulica',
            'Térmica',
            'Importación',
            'Demanda',
        ];

        $dams = [
            'Salto Grande',
            'Bonete',
            'Baygorria',
            'Palmar',
        ];

        $imports = [
            'Imp.Arg',
            'Imp.Br.Riv',
            'Imp.Br.Mel',
        ];

        $highestCol = $spreadsheet->getActiveSheet()->getHighestDataColumn();
        $highestRow = $spreadsheet->getActiveSheet()->getHighestDataRow();
        $range = "A3:{$highestCol}{$highestRow}";
        $spreadsheetData = $spreadsheet->getActiveSheet()
            ->rangeToArray(
                $range,     // The worksheet range that we want to retrieve
                0,        // Value that should be returned for empty cells
                false,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
                false,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
                true         // Should the array be indexed by cell row and cell column
            );
        $spreadsheetData = collect($spreadsheetData);
        /** @var array<string, string> $headers */
        $headers = $spreadsheetData->shift();
        /** @var \Illuminate\Support\Collection<int, array<string, float|int>> $spreadsheetData */
        $spreadsheetData = $spreadsheetData->map(fn ($row) => array_combine($headers, $row));

        $dates = $spreadsheetData->pluck('Fecha')
            ->map(fn (float $serialNumber) => GoogleSheets::getDateFromSerialNumber($serialNumber, 'America/Montevideo'))
            ->map(fn (Carbon $date) : int => (int) $date->timestamp)
            ->values();

        // Combine all dams
        $spreadsheetData->transform(function (array $row) use ($dams) {
            $row['Hidráulica'] = 0;
            foreach ($dams as $column) {
                $row['Hidráulica'] += $row[$column];
                unset($row[$column]);
            }
            return $row;
        });

        // Combine all imports
        $spreadsheetData->transform(function (array $row) use ($imports) {
            $row['Importación'] = 0;
            foreach ($imports as $column) {
                $row['Importación'] += $row[$column];
                unset($row[$column]);
            }
            return $row;
        });
        $data = new Collection();
        foreach ($columns as $column) {
            $data->put($column, $dates->combine($spreadsheetData->pluck($column)));
        }

        return $data;
    }

    public function getDemandForInterval(Carbon $start, Carbon $end) : Collection
    {
        $spreadsheet = $this->readSpreadsheet($start, $end);

        $highestCol = $spreadsheet->getActiveSheet()->getHighestDataColumn();
        $highestRow = $spreadsheet->getActiveSheet()->getHighestDataRow();
        $range = "{$highestCol}4:{$highestCol}{$highestRow}";
        $demandData = $spreadsheet->getActiveSheet()
            ->rangeToArray(
                $range,     // The worksheet range that we want to retrieve
                0,        // Value that should be returned for empty cells
                false,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
                false,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
                true         // Should the array be indexed by cell row and cell column
            );
        $dateRange = "A4:A{$highestRow}";
        $dates = collect($spreadsheet->getActiveSheet()->rangeToArray($dateRange, 0, false, false, true));
        $dates = $dates->flatten()
            ->map(fn (float $value) => GoogleSheets::getDateFromSerialNumber($value, 'America/Montevideo'))
            ->map(fn (Carbon $date) => $date->timestamp);

        $demandData = collect($demandData)->flatten();

        return collect(['Demanda' => $dates->combine($demandData)]);
    }

    /**
     *
     * @param \Carbon\Carbon $date
     * @throws \Exception
     * @throws \Illuminate\Http\Client\RequestException
     * @throws \RuntimeException
     * @return \Illuminate\Support\Collection<int, float>
     */
    public function getSpotPrice(Carbon $date) : Collection
    {
        $baseUrl = 'https://adme.com.uy/mmee/spot/spotSancionadoDetalle.php';
        $params = [
            'a' => $date->year,
            'm' => $date->format('m'),
            'remota' => 1,
        ];
        $url = $baseUrl . '?' . http_build_query($params);

        $postParams = [
            'descargar' => 'Descargar Datos',
        ];
        $response = Http::withHeaders([
            'User-Agent' => $this->userAgent->get(),
        ])->asForm()->post($url, $postParams)->throw();

        $rows = explode(";\r\n", $response->body());
        if (!is_array($rows)) {
            throw new RuntimeException('Download for spot data failed');
        }

        return collect($rows)
            ->filter()
            ->map(fn (string $row) => str_getcsv($row, ';'))
            ->map(function (array $row) use ($date) {
                $dateStr = $row[0];
                unset($row[0]);
                return collect($row)->mapWithKeys(fn (string $value, int $hour) => [
                    /** @phpstan-ignore-next-line */
                    (int) Carbon::createFromFormat('d/m/Y G:i', $dateStr . ' ' . $hour . ':00', 'America/Montevideo')->timestamp => floatval($value),
                ])->reject(
                    fn (float $value, int $ts) => Carbon::createFromTimestamp($ts, 'America/Montevideo')->month !== $date->month
                )->toArray();
            })->mapWithKeys(fn (array $dayData) => $dayData);
    }

    private function readSpreadsheet(Carbon $start, Carbon $end) : Spreadsheet
    {
        $filepath = $this->getFilepathForInterval($start, $end);
        try {
            $spreadsheet = $this->reader->load($filepath);
        } catch(ReaderException $e) {
            $this->downloadHistoricalTenMinutesFile($filepath, $start, $end);
            $this->filepaths[$filepath] = true;
            $spreadsheet = $this->reader->load($filepath);
        }
        return $spreadsheet;
    }

    private function getFilepathForInterval(Carbon $start, Carbon $end) : string
    {
        return storage_path('app/ute/' . $start->format('Y-m-d') . '-' . $end->format('Y-m-d') . '.ods');
    }
}
