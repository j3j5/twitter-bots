<?php

namespace App\Services\UTE;

use App\Services\UserAgent;
use Carbon\Carbon;
use DiDom\Document;
use DiDom\Element;
use Illuminate\Support\Facades\Http;
use RuntimeException;

class App
{
    public const POWER_BY_SOURCE = 'https://apps.ute.com.uy/SgePublico/ConsPotenciaGeneracionArbolXFuente.aspx';

    private array $powerData = [];
    /**
     *
     * @var array<string>
     */
    private array $powerDemand = [
        'Actual' => '',
        'Mínima del día' => '',
        'Máxima del día' => '',
    ];
    private Carbon $lastUpdated;

    public function __construct(private UserAgent $userAgent)
    {
        //
    }

    /**
     *
     * @throws \Exception
     * @throws \DiDom\Exceptions\InvalidSelectorException
     * @throws \InvalidArgumentException
     * @throws \Carbon\Exceptions\InvalidFormatException
     * @return array<string, string>
     */
    public function parseCurrentPowerGenerationBySource()
    {
        $response = Http::withHeaders([
            'User-Agent' => $this->userAgent->get(),
        ])->get(self::POWER_BY_SOURCE);

        $document = new Document($response->body());

        // Demand
        $rows = $document->find('#TABLE1 tr td table tr');
        foreach ($rows as $i => $row) {
            if ($i > 0 && $i < 6 && $row instanceof Element) {
                $tds = $row->find('td');
                if (isset($tds[0], $tds[1]) && $tds[0] instanceof Element && $tds[1] instanceof Element) {
                    $key = trim($tds[0]->text());
                    $value = trim($tds[1]->text());
                    $this->powerDemand[$key] = $value;
                }
            }
        }

        // Last update
        $lastUpdateNode = $document->first('#ctl00_ContentPlaceHolder1_lblUltFecScada');
        if ($lastUpdateNode instanceof Element) {
            $msg = 'Últimos datos aportados al SGE por el SCADA: ';
            $lastUpdateTxt = trim(str_replace($msg, '', $lastUpdateNode->text()));
            $format = 'd/m/Y H:i';
            $lastUpdated = Carbon::createFromFormat($format, trim($lastUpdateTxt), 'America/Montevideo');
            if ($lastUpdated instanceof Carbon) {
                $this->lastUpdated = $lastUpdated;
            }
        }

        // Get power data from the table
        $rows = $document->find('#ctl00_ContentPlaceHolder1_gridPotenciasNivel1 tr');
        $siteData = [];
        foreach ($rows as $i => $row) {
            if ($i > 0 && $i < 6 && $row instanceof Element) {
                /** @var array<int, string> $rowData */
                $rowData = collect($row->find('td'))
                    ->map(fn (Element $h) => $h->text()) /** @phpstan-ignore-line */
                    ->take(2)
                    ->toArray();
                $siteData[$rowData[0]] = $rowData[1];
            }
        }

        // Replace strings with floats
        foreach ($siteData as $key => $value) {
            // Change from "spanish" notation to float ('.' are removed and THEN ',' become '.')
            $power = floatval(str_replace(['.', ','], ['', '.'], $value));
            if ($power > 0) {
                $this->powerData[str_replace(['Fotovoltaica'], ['Solar'], $key)] = $power;
            }
        }

        return $this->powerData;
    }

    public function getLastUpdated() : Carbon
    {
        if (!isset($this->lastUpdated)) {
            throw new RuntimeException('Last updated is not available yet. Have you parse the page yet?');
        }
        return $this->lastUpdated;
    }

    public function getCurrentDemand() : array
    {
        if (empty($this->powerDemand)) {
            throw new RuntimeException('Power demand is not available yet. Have you parse the page yet?');
        }
        return $this->powerDemand;
    }

    public function getPowerData() : array
    {
        if (empty($this->powerData)) {
            throw new RuntimeException('Power data is not available yet. Have you parse the page yet?');
        }
        return $this->powerData;
    }
}
