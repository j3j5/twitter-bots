<?php

namespace App\Services;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use RuntimeException;

/**
 * @phpstan-type Estacion array{id: int, Estacion: string, NombreEstacion: string}
 * @phpstan-type Variable array{idInt: int, variable: string, nombre: string, unidad: string}
 * @phpstan-type Dato array<string, int|float|string|null>
 * @phpstan-type Observacion array{ifechas: array<int, string>, datos: array<int, Dato>}
 */
class Inumet
{
    public const ICONS_BASE_URL = 'https://www.inumet.gub.uy/themes/custom/inumet/css/assets/icons/clima/';
    public const CURRENT_WEATHER_URL = 'https://www.inumet.gub.uy/reportes/estadoActual/estadoActualDatosHorarios.mch';

    /**
     * List of urls of INUMET's weather icons keyed by their code
     * @var array<int|string, string>
     */
    public readonly array $icons;

    /**
     * List of weather emojis keyed by their INUMET code
     * @var array<int|string, string>
     */
    public readonly array $emojis;

    /**
     * List of variable names keyed by their slugs
     * @var array<string, string>
     */
    public readonly array $variables;

    /** @var \Illuminate\Support\Collection<string, array> */
    public Collection $data;

    /**
     * @var array{estaciones: array<int, Estacion>, variables: array<int, Variable>, observaciones: array<int, Observacion>, fechas: array<int, string>}
     */
    public array $rawData;

    public function __construct(private UserAgent $ua)
    {
        $this->icons = [
            1 => 'ico__claro.svg',
            2 => 'ico__algo-nuboso.svg',
            3 => 'ico__lluvias-aisladas.svg',
            4 => 'ico__cubierto.svg',
            5 => 'ico__llovizna.svg',
            6 => 'ico__lluvia.svg',
            7 => 'ico__chaparron.svg',
            8 => 'ico__niebla.svg',
            9 => 'ico__algo-cubierto.svg',
            10 => 'ico__relampagos.svg',
            11 => 'ico__tormenta.svg',
            12 => 'ico__ventisca.svg',
            13 => 'ico__nuboso.svg',
            14 => 'ico__humo.svg',
            15 => 'ico__neblina.svg',
            16 => 'ico__bruma.svg',
            20 => 'ico__noche-claro.svg',
            21 => 'ico__noche-algo-cubierto.svg',
            22 => 'ico__noche-algo-nuboso.svg',
            23 => 'ico__noche-lluvias-aisladas.svg',
            24 => 'ico__noche-nuboso.svg',
            '-1' => 'ico__estacion-automatica.svg',
        ];

        $this->emojis = [
            1 => '☀️',
            2 => '🌤️',
            3 => '🌦️',
            4 => '☁️',
            5 => '🌧️',
            6 => '🌧️',
            7 => '🌧️',
            8 => '🌫️',
            9 => '🌤️',
            10 => '🌩️',
            11 => '⛈️',
            12 => '🌬️',
            13 => '☁️',
            14 => '😶‍🌫️',
            15 => '🌫️',
            16 => '🌫️‍💧',
            20 => '🌜',
            21 => '🌜🌤️',
            22 => '🌜☁️',
            23 => '🌜🌦️',
            24 => '🌜☁️',
        ];

        $this->variables = [
            'Visibilidad' => 'Visibilidad horizontal',
            'DirViento' => 'Dirección del viento',
            'IntViento' => 'Intensidad de Viento',
            'IntRafaga' => 'Intensidad de ráfaga',
            'IntVientMaxHora' => 'Intensidad de Viento Máxima Horaria',
            'TempAire' => 'Temperatura del Aire',
            'HumRelativa' => 'Humedad relativa',
            'TempPtoRocio' => 'Temperatura de Punto de Rocío',
            'TensionVapAgua' => 'Tensión del Vapor de Agua',
            'PresAtmEst' => 'Presión Atmosférica a Nivel de Estación',
            'PresAtmMar' => 'Presión Atmosférica a Nivel del Mar',
            'IconoEstadoActual' => 'Estado Actual del Tiempo',
            'Nubes' => 'Nubes',
            'Cielo' => 'Nubosidad del cielo',
            'precipHoraria' => 'Precipitación Acumulada Horaria',
        ];
    }

    /**
     * @return \Illuminate\Support\Collection<string, array>
     */
    public function getCurrentWeather() : Collection
    {
        $response = Http::withHeaders([
            'User-Agent' => $this->ua->get(),
        ])->get(self::CURRENT_WEATHER_URL);

        $dataArray = $response->json();
        if (!is_array($dataArray) || empty($dataArray)) {
            Log::error('Inumet response body: ', ['body' => $response->body()]);
            throw new RuntimeException('Response is not a valid json');
        }

        /** @var array{estaciones: array<int, Estacion>, variables: array<int, Variable>, observaciones: array<int, Observacion>, fechas: array<int, string>} $dataArray */
        $this->rawData = $dataArray;
        /** @var \Illuminate\Support\Collection<int, string> $estaciones */
        $estaciones = collect($dataArray['estaciones'])->map(fn ($estacion) => $estacion['NombreEstacion']);
        /** @var \Illuminate\Support\Collection<int, string> $variables */
        $variables = collect($dataArray['variables'])->map(fn ($var) => $var['nombre']);
        /** @var \Illuminate\Support\Collection<int, Observacion> $observaciones */
        $observaciones = collect($dataArray['observaciones']);

        $this->data = $observaciones->mapWithKeys(
            fn (array $observacion, int $index) : array => [
                $variables->get($index) => $estaciones->combine(
                    collect($observacion['datos'])->flatten()
                )->toArray(),
            ]
        );

        return $this->data;
    }

    public function getCurrentWeatherIconForStation(string $station) : string
    {
        if (empty($this->data)) {
            $this->data = $this->getCurrentWeather();
        }
        $current = $this->data->get('Estado Actual del Tiempo');
        if (!isset($current[$station])) {
            throw new RuntimeException('Invalid station ' . $station);
        }
        return Arr::get($this->icons, (string) $current[$station], '');
    }

    public function getCurrentWeatherEmojiForStation(string $station) : string
    {
        if (empty($this->data)) {
            $this->data = $this->getCurrentWeather();
        }
        $current = $this->data->get('Estado Actual del Tiempo');
        if (!isset($current[$station])) {
            throw new RuntimeException('Invalid station ' . $station);
        }
        return Arr::get($this->emojis, (string) $current[$station], '');
    }

    public function getCurrentSkyStatusForStation(string $station) : string
    {
        if (empty($this->data)) {
            $this->data = $this->getCurrentWeather();
        }
        $current = $this->data->get('Nubosidad del cielo');
        if (!isset($current[$station])) {
            throw new RuntimeException('Invalid station ' . $station);
        }
        return $this->translateSkyCode((string) $current[$station]);
    }

    public function getCurrentTemperatureForStation(string $station) : float
    {
        if (empty($this->data)) {
            $this->data = $this->getCurrentWeather();
        }
        $current = $this->data->get('Temperatura del Aire');
        if (!isset($current[$station])) {
            throw new RuntimeException('Invalid station ' . $station);
        }
        return (float) $current[$station];
    }

    public function getCurrentWindDirectionForStation(string $station) : string
    {
        if (empty($this->data)) {
            $this->data = $this->getCurrentWeather();
        }
        $current = $this->data->get('Dirección del viento');
        if (!isset($current[$station])) {
            throw new RuntimeException('Invalid station ' . $station);
        }
        return $this->translateWindDirection((float) $current[$station]);
    }

    public function getCurrentWindDirectionEmojiForStation(string $station) : string
    {
        $direction = $this->getCurrentWindDirectionForStation($station);

        return $this->translateWindDirectionToEmoji($direction);
    }

    /**
     * @return int (km/h)
     */
    public function getCurrentWindSpeed(string $station) : int
    {
        if (empty($this->data)) {
            $this->data = $this->getCurrentWeather();
        }
        $current = $this->data->get('Intensidad de Viento');
        if (!isset($current[$station])) {
            throw new RuntimeException('Invalid station ' . $station);
        }
        return (int) $current[$station];
    }

    public function setData(Collection $data) : self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * As seen in https://www.inumet.gub.uy/sites/default/files/js/js_Z87e31Ge-JslWjRSsWEYMhgiddI64RU-J1Mb2f1NCMA.js
     * @param string $code
     * @return string Readable sky status
     */
    private function translateSkyCode(string $code) : string
    {
        switch ($code) {
            case 'cl':
                return 'Cielo despejado';
            case 'an':
                return 'Algo nuboso';
            case 'nub':
                return 'Nuboso';
            case 'cub':
                return 'Cielo cubierto';
            case 'inv':
                return '-';
            default:
                return $code;
        }
    }

    private function translateWindDirection(float $value) : string
    {
        if ($value === -1) {
            return 'variable';
        }

        if ($value >= 348.75) {
            $dirViento = $value + 11.25 - 360;
        } else {
            $dirViento = $value + 11.25;
        }

        return match(true) {
            $dirViento <= 22.5 => 'N',
            $dirViento <= 45 => 'NNE',
            $dirViento <= 67.5 => 'NE',
            $dirViento <= 90 => 'ENE',
            $dirViento <= 112.5 => 'E',
            $dirViento <= 135 => 'ESE',
            $dirViento <= 157.5 => 'SE',
            $dirViento <= 180 => 'SSE',
            $dirViento <= 202.5 => 'S',
            $dirViento <= 225 => 'SSW',
            $dirViento <= 247.5 => 'SW',
            $dirViento <= 270 => 'WSW',
            $dirViento <= 292.5 => 'W',
            $dirViento <= 315 => 'WNW',
            $dirViento <= 337.5 => 'NW',
            $dirViento <= 360 => 'NNW',
        };
    }

    private function translateWindDirectionToEmoji(string $direction)
    {
        return match ($direction) {
            'variable' => '🔄',
            'N', => '⬇️',
            'NNE', 'NE', 'ENE' => '↘️',
            'E' => '➡️',
            'ESE', 'SE', 'SSE', => '↗️',
            'S' => '⬆️',
            'SSW', 'SW', 'WSW', => '↖️',
            'W' => '⬅️',
            'WNW', 'NW', 'NNW', => '↙️',
            default => '',
        };
    }

    /*
        private function getStrViento(indexEstacion, indexFecha, idDirViento, idIntViento, idIntRafaga, datos) {
            $indexIntViento = -1;
            $indexIntRafaga = -1;
            $indexDirViento = -1;
            for (i = 0; i < datos.variables.length; i++) {
                if (datos.variables[i].idInt === idDirViento)
                    $indexDirViento = i;
                if (datos.variables[i].idInt === idIntViento)
                    $indexIntViento = i;
                if (datos.variables[i].idInt === idIntRafaga)
                    $indexIntRafaga = i;
                if (($indexDirViento >= 0) && ($indexIntViento >= 0) && ((idIntRafaga <= 0) || ($indexIntRafaga >= 0)))
                    break;
            }
            if (($indexIntViento < 0) || ($indexDirViento < 0) || ((idIntRafaga > 0) && ($indexIntRafaga < 0))) {
                return "-";
            }

            $intViento = datos.observaciones[$indexIntViento].datos[indexEstacion][indexFecha];
            $dirViento = datos.observaciones[$indexDirViento].datos[indexEstacion][indexFecha];
            $intRafaga = null;

            if ($indexIntRafaga >= 0)
                $intRafaga = datos.observaciones[$indexIntRafaga].datos[indexEstacion][indexFecha];

            if (existeValor($intViento)) {
                if ($intViento === 0)
                    return "Calmo";
            }

            strViento = "-";
            if (existeValor($dirViento)) {
                $dirViento = Number($dirViento);
                if ($dirViento === -1) {
                    strViento = "variable";
                } else {
                    dirVientoCorr = $dirViento;
                    if ($dirViento >= 348.75) {
                        dirVientoCorr = $dirViento + 11.25 - 360;
                    } else {
                        dirVientoCorr = $dirViento + 11.25;
                    }

                    if (dirVientoCorr <= 22.5) {
                        strViento = 'N';
                    } else if (dirVientoCorr <= 45) {
                        strViento = 'NNE';
                    } else if (dirVientoCorr <= 67.5) {
                        strViento = 'NE';
                    } else if (dirVientoCorr <= 90) {
                        strViento = 'ENE';
                    } else if (dirVientoCorr <= 112.5) {
                        strViento = 'E';
                    } else if (dirVientoCorr <= 135) {
                        strViento = 'ESE';
                    } else if (dirVientoCorr <= 157.5) {
                        strViento = "SE";
                    } else if (dirVientoCorr <= 180) {
                        strViento = 'SSE';
                    } else if (dirVientoCorr <= 202.5) {
                        strViento = 'S';
                    } else if (dirVientoCorr <= 225) {
                        strViento = 'SSW';
                    } else if (dirVientoCorr <= 247.5) {
                        strViento = 'SW';
                    } else if (dirVientoCorr <= 270) {
                        strViento = 'WSW';
                    } else if (dirVientoCorr <= 292.5) {
                        strViento = "W";
                    } else if (dirVientoCorr <= 315) {
                        strViento = 'WNW';
                    } else if (dirVientoCorr <= 337.5) {
                        strViento = 'NW';
                    } else if (dirVientoCorr <= 360) {
                        strViento = 'NNW';
                    }
                }

                return concatStrIntVientoYRafaga(strViento, $intViento, $intRafaga);
            }

            return strViento;
        }*/
}
