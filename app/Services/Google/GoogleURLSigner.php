<?php

namespace App\Services\Google;

use Exception;
use Psr\Http\Message\RequestInterface;

use function Safe\base64_decode;
use function Safe\parse_url;

class GoogleURLSigner
{
    private string $secret;

    public function __construct(string $secret)
    {
        $this->secret = $secret;
    }

    public function signBeforeSend(RequestInterface $request) : RequestInterface
    {
        $newUrl = $this->signUrl((string) $request->getUri());
        $newQuery = parse_url($newUrl, PHP_URL_QUERY);
        if (!is_string(($newQuery))) {
            $newQuery = '';
        }
        return $request->withUri($request->getUri()->withQuery($newQuery));
    }

    // Sign a URL with a given crypto key
    // Note that this URL must be properly URL-encoded
    public function signUrl(string $myUrlToSign) : string
    {
        // parse the url
        $url = parse_url($myUrlToSign);
        if (!is_array($url)) {
            throw new Exception('Malformed URL to sign');
        }

        $urlPartToSign = $url['path'] . '?' . $url['query'];

        // Decode the private key into its binary format
        $decodedKey = $this->decodeBase64UrlSafe($this->secret);

        // Create a signature using the private key and the URL-encoded
        // string using HMAC SHA1. This signature will be binary.
        $signature = hash_hmac('sha1', $urlPartToSign, $decodedKey, true);

        $encodedSignature = $this->encodeBase64UrlSafe($signature);

        return $myUrlToSign . '&signature=' . $encodedSignature;
    }

    // Encode a string to URL-safe base64
    private function encodeBase64UrlSafe(string $value) : string
    {
        return str_replace(
            ['+', '/'],
            ['-', '_'],
            base64_encode($value)
        );
    }

    // Decode a string from URL-safe base64
    private function decodeBase64UrlSafe(string $value) : string
    {
        return base64_decode(str_replace(
            ['-', '_'],
            ['+', '/'],
            $value
        ));
    }
}
