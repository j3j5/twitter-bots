<?php

namespace App\Services;

use Amenadiel\JpGraph\Plot\IconPlot;
use App\Graphs\Themes\IpapTheme;
use App\Services\Google\GoogleSheets;
use App\Traits\Graphs\GeneratesGraphs;
use Google\Service\Sheets;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use RuntimeException;

use function Safe\array_combine;
use function Safe\preg_match;

class IPAP
{
    use GeneratesGraphs;

    private Sheets $sheetsApi;

    private string $spreadsheetId;
    private string $sheet;

    /** First date on the spreadsheet */
    private Carbon $startDate;
    /** First date with IPAP data */
    private Carbon $firstMonth;
    /** First row with data */
    private int $startRow;
    /** */
    private int $lastRowWithData;
    /** The last month with data to use */
    private Carbon $latestMonth;

    private array $prices;
    private array $inflationRates;
    private array $incidencia;

    public function __construct(Sheets $sheetsApi)
    {
        $this->sheetsApi = $sheetsApi;

        $this->spreadsheetId = '1f8_95ZXiB_Cz1ZVznkpd34O3O0zO8Huvs5O7bhUXZ2U';
        $this->sheet = 'IPAP';

        $this->startRow = 6;
        $this->startDate = Carbon::parse('2010-12-01'); // First date on the spreadsheet
        $this->firstMonth = Carbon::parse('2011-12-01'); // First date with IPAP data
    }

    public function init(?string $month = null) : Carbon
    {
        // Calculate the highest column currently to get the InsertRange (next column)
        $datesRange = "{$this->sheet}!A:E";
        $params = ['valueRenderOption' => 'UNFORMATTED_VALUE'];
        $response = $this->sheetsApi->spreadsheets_values->get($this->spreadsheetId, $datesRange, $params);
        /** @var array<int, array<int, string|int|float>> */
        $values = $response->getValues();
        $allColumns = collect($values);
        $columnsWithData = $allColumns->filter(
            fn (array $row) => !empty($row) && $row[1] !== 0 && (!is_string($row[4]) || preg_match('/^#N\/A \(Did not find value/', $row[4]) === 0)
        );

        if ($month !== null) {
            // A month was provided, no need to look for the latest
            $this->latestMonth = Carbon::parse($month);
            $dateNumber = GoogleSheets::getSerialNumberFromDate($this->latestMonth);
            $row = $columnsWithData->firstWhere('0', $dateNumber);
            if ($row === null) {
                throw new RuntimeException('Month not found');
            }
            $this->lastRowWithData = (int) $columnsWithData->search($row) + 1;
        } else {
            $this->lastRowWithData = (int) $columnsWithData->keys()->last() + 1; // Add 1 because the array is zero-indexed
            $lastColumn = $columnsWithData->last();
            if (!isset($lastColumn[0])) {
                throw new RuntimeException('No months found');
            }
            $this->latestMonth = GoogleSheets::getDateFromSerialNumber((float) $lastColumn[0]);
        }

        return $this->latestMonth;
    }

    public function generateCharts() : array
    {
        $charts = [];
        $charts['precio-por-rubro-año'] = $this->precioAPporRubroComparada($this->latestMonth->copy()->subYear());
        $charts['precio-por-rubro-mes'] = $this->precioAPporRubroComparada($this->latestMonth->copy()->subMonth());
        $charts['evolucion-precios'] = $this->evolucionPreciosAP();
        $charts['incidencia'] = $this->incidenciaPorRubro();
        $charts['ipap-x-rubro'] = $this->ipapPorRubro();
        $charts['ipap-vs-inflacion'] = $this->ipapVsInflacion();
        return $charts;
    }

    /**
     * Group Barchart
     */
    public function precioAPporRubroComparada(Carbon $firstDate, ?Carbon $secondDate = null) : string
    {
        $this->theme = new IpapTheme();
        // Last Year
        $prices = $this->getPrices($firstDate);
        $data[] = array_values($prices);

        $secondDate = $secondDate ?? $this->latestMonth;
        $prices = $this->getPrices($secondDate);

        $labels = array_keys($prices);
        $data[] = array_values($prices);

        $title = 'Precio Asado con Picadita por Rubro';
        /** @phpstan-ignore-next-line */
        $subtitle = $firstDate->locale('es_UY')->isoFormat('MMM YYYY');
        $subtitle .= ' vs ';
        /** @phpstan-ignore-next-line */
        $subtitle .= $secondDate->locale('es_UY')->isoFormat('MMM YYYY');
        /** @phpstan-ignore-next-line */
        $subsubtitle = 'Comparación ' . $firstDate->locale('es_UY')->diffForHumans($secondDate);
        $xLabel = '';
        $yLabel = 'Precio';
        $graph = $this->createBarChart($data, $labels, $title, $subtitle, $subsubtitle, $xLabel, $yLabel);
        // Apply custom format to labels
        /** @var \Amenadiel\JpGraph\Plot\GroupBarPlot $plot */
        foreach (Arr::wrap($graph->plots) as $plot) {
            /** @var \Amenadiel\JpGraph\Plot\BarPlot $p */
            foreach (Arr::wrap($plot->plots) as $i => $p) {
                $p->value->SetFormatCallback(function ($value) use ($data, $i) : string {
                    $format = '$%d';
                    $price = $value;
                    // Value in the past, format the price only
                    if ($i === 0) {
                        return sprintf($format, $price);
                    }
                    // For the current value, calculate the % difference and
                    // add it on a new line
                    $pos = array_search($value, $data[1]);
                    $prevPrice = $data[0][$pos];
                    $perc = ($price - $prevPrice) / $prevPrice * 100;

                    // Center the price on top of the perc based on how many letters
                    // the percentage string takes
                    $chars = strlen(sprintf('(%+01.2f%%)', $perc)) - 1;
                    $format = str_repeat(' ', (int) floor($chars / 2)) . '$%d' . PHP_EOL . '(%+01.2f%%)';

                    return sprintf($format, $price, $perc);
                });
            }
        }

        $graph->SetTickDensity(TICKD_SPARSE);
        $graph->yaxis->SetLabelFormatCallback(fn ($tick) => sprintf('$%d', $tick));

        $icon = $this->getLicenseIcon();
        $graph->AddIcon($icon);

        return $this->getGraphImage($graph);
    }

    /**
     * Barchart
     */
    public function ipapPorRubro() : string
    {
        $this->theme = new IpapTheme();
        $rates = $this->getInflationRates();
        $labels = array_keys($rates);
        array_walk($rates, function (&$val) {
            $val *= 100;
        });
        $data[] = array_values($rates);
        $title = 'Inflación e IPAP por Rubro';
        $subtitle = 'Variación interanual, último mes (' . $this->latestMonth->locale('es_UY')->isoFormat('MMM YYYY') . ')';
        $subsubtitle = '';
        $xLabel = '';
        $yLabel = '';
        $graph = $this->createBarChart($data, $labels, $title, $subtitle, $subsubtitle, $xLabel, $yLabel);
        // Apply custom format to labels
        foreach (Arr::wrap($graph->plots) as $plot) {
            /** @var \Amenadiel\JpGraph\Plot\BarPlot $plot */
            $plot->value->SetFormatCallback(fn ($value) => sprintf('%01.2f%%', $value));
        }
        $graph->SetTickDensity(TICKD_VERYSPARSE);
        list($min, $max) = $graph->yscale->scale;
        if (($max - $min) > 0.15) {
            $graph->SetTickDensity(TICKD_SPARSE);
            foreach (Arr::wrap($graph->plots) as $plot) {
                /** @var \Amenadiel\JpGraph\Plot\BarPlot $plot */
                $plot->SetValuePos('max');
                $plot->value->SetFont(FF_USERFONT3, FS_BOLD, 14);
                $plot->value->SetColor($this->theme->fontColorLight);
            }
        }
        $graph->yaxis->SetLabelFormatCallback(fn ($tick) => sprintf('%d%%', $tick));

        $icon = $this->getLicenseIcon();
        $graph->AddIcon($icon);

        return $this->getGraphImage($graph);
    }

    /**
     * Piechart
     */
    public function incidenciaPorRubro() : string
    {
        $this->theme = new IpapTheme();
        // Get the labels for all the parts
        $labelsRange = "{$this->sheet}!BC5:BG5";
        $params = ['valueRenderOption' => 'UNFORMATTED_VALUE'];
        $response = $this->sheetsApi->spreadsheets_values->get($this->spreadsheetId, $labelsRange, $params);
        /** @var array<int, array<int, string|int|float>> */
        $values = $response->getValues();
        $labels = Arr::first($values);

        // Get the values for "incidencia" on the latest row with data
        $dataRange = "{$this->sheet}!BC{$this->lastRowWithData}:BG{$this->lastRowWithData}";
        $params = ['valueRenderOption' => 'UNFORMATTED_VALUE'];
        $response = $this->sheetsApi->spreadsheets_values->get($this->spreadsheetId, $dataRange, $params);
        /** @var array<int, array<int, string|int|float>> */
        $values = $response->getValues();
        $this->incidencia = array_combine($labels, Arr::first($values));
        uksort($this->incidencia, [$this, 'sorter']);
        $labels = array_keys($this->incidencia);
        $data = array_values($this->incidencia);

        array_walk($labels, function (&$label, $i) use ($data) {
            $label .= ' (' . number_format($data[$i] * 100, 2) . '%%)';
        });

        $title = 'Incidencia por rubro del Asado con Picadita';
        $subtitle = 'Mes actual (' . $this->latestMonth->locale('es_UY')->isoFormat('MMM YYYY') . ')';
        $subsubtitle = '';
        $graph = $this->createPieChart($data, $labels, $title, $subtitle, $subsubtitle);

        $icon = $this->getLicenseIcon();
        $graph->AddIcon($icon);

        return $this->getGraphImage($graph);
    }

    /** 2 lines linechart */
    public function ipapVsInflacion() : string
    {
        $this->theme = new IpapTheme();
        $firstRow = $this->getRowFromMonth($this->firstMonth);
        // Get the values for IPAP and Inflacion on the latest row with data
        $inflacionDataRange = "{$this->sheet}!AT{$firstRow}:AT{$this->lastRowWithData}";
        $ipapDataRange = "{$this->sheet}!AU{$firstRow}:AU{$this->lastRowWithData}";
        $params = ['valueRenderOption' => 'UNFORMATTED_VALUE'];
        $inflacionResponse = $this->sheetsApi->spreadsheets_values->get($this->spreadsheetId, $inflacionDataRange, $params);
        $ipapResponse = $this->sheetsApi->spreadsheets_values->get($this->spreadsheetId, $ipapDataRange, $params);
        /** @var array<int, array<int, string|int|float>> $inflaValues */
        $inflaValues = $inflacionResponse->getValues();
        $inflacion = collect($inflaValues)->flatten()->map(fn ($value) => round($value * 100, 2))->toArray();
        /** @var array<int, array<int, string|int|float>> $ipapValues */
        $ipapValues = $ipapResponse->getValues();
        $ipap = collect($ipapValues)->flatten()->map(fn ($value) => round($value * 100, 2))->toArray();
        $data = [
            'Inflación' => $inflacion,
            'IPAP' => $ipap,
        ];
        $labels = [];
        $month = $this->firstMonth->copy();
        while ($month->isBefore($this->latestMonth)) {
            $labels[] = $month->timestamp;
            $month->addMonth();
        }
        $labels[] = $month->timestamp;

        // Now get labels at the start of each month
        $title = 'IPAP vs Inflación';
        $subtitle = 'Variaciones interanuales, serie histórica';
        $subsubtitle = $this->firstMonth->locale('es_UY')->isoFormat('MMM YYYY') . ' - ' . $this->latestMonth->locale('es_UY')->isoFormat('MMM YYYY');
        $graph = $this->createLinePlot($data, $labels, $title, $subtitle, $subsubtitle);

        $graph->yscale->SetGrace(5);

        // Get manual tick every second year
        list($majTickPos, $minTickPos) = $this->getYearlyTicks($labels);
        $graph->xaxis->SetTickPositions($majTickPos, $minTickPos);
        $graph->xaxis->SetLabelFormatCallback(fn ($ts) => Carbon::createFromTimestamp($ts)->locale('es_UY')->isoFormat('MMM YYYY'));

        $graph->SetTickDensity(TICKD_SPARSE);
        $graph->yaxis->SetLabelFormatCallback(fn ($tick) => sprintf('%d%%', $tick));

        $icon = $this->getLicenseIcon();
        $graph->AddIcon($icon);

        return $this->getGraphImage($graph);
    }

    public function evolucionPreciosAP() : string
    {
        $this->theme = new IpapTheme();
        $firstRow = $this->getRowFromMonth($this->startDate);
        // Get the labels for all the parts of AP
        $labelsRange = "{$this->sheet}!AB5:AF5";
        $params = ['valueRenderOption' => 'UNFORMATTED_VALUE'];
        $response = $this->sheetsApi->spreadsheets_values->get($this->spreadsheetId, $labelsRange, $params);
        /** @var array<int, array<int, string|int|float>> */
        $values = $response->getValues();
        $seriesLabels = Arr::first($values);

        // Get the values for all different parts of AP
        $pricesRange = "{$this->sheet}!AB{$firstRow}:AF{$this->lastRowWithData}";
        $params = ['valueRenderOption' => 'UNFORMATTED_VALUE'];
        $pricesResponse = $this->sheetsApi->spreadsheets_values->get($this->spreadsheetId, $pricesRange, $params);

        $labels = [];
        $month = $this->startDate->copy();
        while ($month->isBefore($this->latestMonth)) {
            $labels[] = $month->timestamp;
            $month->addMonth();
        }
        $labels[] = $month->timestamp;
        /** @var array<int, array<int, string|int|float>> $pricesValues */
        $pricesValues = $pricesResponse->getValues();
        $values = collect($pricesValues)->map(fn ($series) => array_combine($seriesLabels, $series));
        $data = [];
        foreach ($seriesLabels as $l) {
            $data[$l] = $values->pluck($l);
        }
        uksort($data, [$this, 'sorter']);

        // Now get labels at the start of each month
        $title = 'Evolución Precio de Asado con Picadita';
        $subtitle = 'Serie histórica';
        $subsubtitle = $this->firstMonth->locale('es_UY')->isoFormat('MMM YYYY') . ' - ' . $this->latestMonth->locale('es_UY')->isoFormat('MMM YYYY');
        $graph = $this->createStackedLinePlot($data, $labels, $title, $subtitle, $subsubtitle);

        // Get manual tick every second year
        list($majTickPos, $minTickPos) = $this->getYearlyTicks($labels);
        $graph->xaxis->SetTickPositions($majTickPos, $minTickPos);
        $graph->xaxis->SetLabelFormatCallback(
            fn ($ts) => Carbon::createFromTimestamp($ts)->locale('es_UY')->isoFormat('MMM YYYY')
        );

        $graph->SetTickDensity(TICKD_SPARSE);
        $graph->yaxis->SetLabelFormatCallback(fn ($tick) => sprintf('$%d', $tick));

        $icon = $this->getLicenseIcon();
        $graph->AddIcon($icon);

        return $this->getGraphImage($graph);
    }

    /**
     * Get the prices of each IPAP component for the given month
     *
     * @param Carbon $month
     * @return array
     * @-return array{Bebida: string, Picadita: string, Asado: string, Ensalada: string, Postre: string, 'Asado con Picadita': string}
     *
     */
    public function getPrices(Carbon $month = null) : array
    {
        $month = $month ?? $this->latestMonth;
        $row = $this->getRowFromMonth($month);
        $params = ['valueRenderOption' => 'UNFORMATTED_VALUE'];
        $pricesRange = "IPAP!AB{$row}:AG{$row}";
        $pricesKeys = [
            'Bebida' => 0,
            'Picadita' => 1,
            'Asado' => 2,
            'Ensalada' => 3,
            'Postre' => 4,
            'Asado con Picadita' => 5,
        ];
        $response = $this->sheetsApi->spreadsheets_values->get($this->spreadsheetId, $pricesRange, $params);
        $this->prices[$month->format('Y-m')] = $this->getFormattedArrayFromResults($pricesKeys, $response->getValues());

        return $this->prices[$month->format('Y-m')];
    }

    /**
     * Get the inflation rate of each IPAP component for the given month
     *
     * @param Carbon $month
     * @return array
     * @-return array{Bebida: string, Picadita: string, Asado: string, Ensalada: string, Postre: string, IPAP: string, INFLACION: string}
     */
    public function getInflationRates(Carbon $month = null) : array
    {
        $month = $month ?? $this->latestMonth;
        $row = $this->getRowFromMonth($this->latestMonth);
        $inflationRange = "IPAP!AT{$row}:BA{$row}";
        $inflationKeys = [
            'Inflación' => 0,
            'IPAP' => 1,
            'Bebida' => 3,
            'Picadita' => 4,
            'Asado' => 5,
            'Ensalada' => 6,
            'Postre' => 7,
        ];

        $params = ['valueRenderOption' => 'UNFORMATTED_VALUE'];
        $response = $this->sheetsApi->spreadsheets_values->get($this->spreadsheetId, $inflationRange, $params);
        $this->inflationRates[$month->format('Y-m')] = $this->getFormattedArrayFromResults($inflationKeys, $response->getValues());
        return $this->inflationRates[$month->format('Y-m')];
    }

    public function getPrice(Carbon $month, string $key) : int
    {
        if (!isset($this->prices[$month->format('Y-m')])) {
            throw new RuntimeException($month->format('Y-m') . ' is not available on prices array');
        }

        if (!isset($this->prices[$month->format('Y-m')][$key])) {
            throw new RuntimeException($key . ' is not available on prices array for ' . $month->format('Y-m'));
        }

        return (int) round($this->prices[$month->format('Y-m')][$key]);
    }

    public function getInflationRate(Carbon $month, string $key) : float
    {
        if (!isset($this->inflationRates[$month->format('Y-m')])) {
            throw new RuntimeException($month->format('Y-m') . ' is not available on inflation rates array');
        }

        if (!isset($this->inflationRates[$month->format('Y-m')][$key])) {
            throw new RuntimeException($key . ' is not available on inflation rates array for ' . $month->format('Y-m'));
        }

        return round($this->inflationRates[$month->format('Y-m')][$key] * 100, 2);
    }

    public function getIncidencia(string $key) : string
    {
        if (!isset($this->incidencia[$key])) {
            throw new RuntimeException("$key is not available on the incidencia array");
        }
        return number_format($this->incidencia[$key] * 100, 2);
    }

    public function getRowFromMonth(Carbon $month) : int
    {
        return $month->diffInMonths($this->startDate) + $this->startRow;
    }

    private function getLicenseIcon() : IconPlot
    {
        // Add CC icon
        $icon = new IconPlot(resource_path('images/ipap-cc-by-sa.png'));
        $icon->SetScale(0.35);
        $icon->SetMix(90);
        $icon->SetAnchor('right', 'top');
        $icon->SetPos(1200, 40);

        return $icon;
    }

    private function getFormattedArrayFromResults(array $keys, array $values) : array
    {
        $result = [];
        foreach ($keys as $key => $index) {
            $result[$key] = $values[0][$index];
        }
        uksort($result, [$this, 'sorter']);
        return $result;
    }

    protected function sorter(string $key1, string $key2) : int
    {
        $preferredOrder = [
            'Asado',
            'Bebida',
            'Picadita',
            'Bebida',
            'Postre',
            'Ensalada',
            'IPAP',
            'Inflación',
            'Asado con Picadita',
        ];

        return array_search($key1, $preferredOrder) - array_search($key2, $preferredOrder);
    }
}
