<?php

namespace App\Services\MIEM;

class Helper
{
    /**
     *
     * @param int $class
     * @return array{title: string, icon: string, desc: string}
     */
    public function getNiceClassInfo(int $class) : array
    {
        return match ($class) {
            1 => [
                'title' => 'Productos' . PHP_EOL . 'químicos',
                'icon' => 'flask-vial',
                'desc' => 'productos químicos que se utilizan en la industria, la ciencia y la agricultura',
            ],
            2 => [
                'title' => 'Pinturas' . PHP_EOL . 'y resinas',
                'icon' => 'paint-roller',
                'desc' => 'pinturas, colorantes y productos anticorrosivos',
            ],
            3 => [
                'title' => 'Cosméticos' . PHP_EOL . 'y Limpieza',
                'icon' => 'pump-soap',
                'desc' => 'preparaciones de tocador no medicinales, así como las preparaciones de limpieza utilizadas en el hogar o en otros ámbitos',
            ],
            4 => [
                'title' => 'Combustibles' . PHP_EOL . 'y Aceites',
                'icon' => 'gas-pump',
                'desc' => 'aceites y las grasas para uso industrial, los combustibles y los materiales de alumbrado',
            ],
            5 => [
                'title' => 'Medicinas',
                'icon' => 'pills',
                'desc' => 'productos farmacéuticos y otras preparaciones para uso médico o veterinario',
            ],
            6 => [
                'title' => 'Metales y' . PHP_EOL . 'aleaciones',
                'icon' => 'magnet',
                'desc' => 'metales comunes en bruto y semielaborados, incluidas las menas, así como ciertos productos fabricados con metales comunes',
            ],
            7 => [
                'title' => 'Maquinaria',
                'icon' => 'gears',
                'desc' => 'máquinas y las máquinas herramientas, así como los motores',
            ],
            8 => [
                'title' => 'Herramientas' . PHP_EOL . 'de mano',
                'icon' => 'screwdriver-wrench',
                'desc' => 'herramientas e instrumentos de mano que funcionan manualmente para la realización de tareas tales como el taladrado, el modelado, el corte y la perforación',
            ],
            9 => [
                'title' => 'Aparatos eléctr.' . PHP_EOL . 'y científicos',
                'icon' => 'computer',
                'desc' => 'aparatos e instrumentos científicos o de investigación, los equipos audiovisuales y de tecnología de la información y los equipos de seguridad y salvamento',
            ],
            10 => [
                'title' => 'Instrumentos' . PHP_EOL . 'médicos',
                'icon' => 'stethoscope',
                'desc' => 'aparatos, instrumentos y artículos quirúrgicos, médicos, dentales y veterinarios',
            ],
            11 => [
                'title' => 'Control' . PHP_EOL . 'ambiental',
                'icon' => 'snowflake',
                'desc' => 'aparatos e instalaciones de alumbrado, calefacción, enfriamiento, producción de vapor, cocción, secado, ventilación y distribución de agua, así como instalaciones sanitarias',
            ],
            12 => [
                'title' => 'Vehículos',
                'icon' => 'truck-plane',
                'desc' => 'vehículos y aparatos para el transporte terrestre, aéreo o acuático de personas o de mercancías',
            ],
            13 => [
                'title' => 'Armas' . PHP_EOL . 'de fuego',
                'icon' => 'gun',
                'desc' => 'armas de fuego, municiones y proyectiles, explosivos y fuegos artificiales',
            ],
            14 => [
                'title' => 'Joyería',
                'icon' => 'gem',
                'desc' => 'metales preciosos y ciertos productos de metales preciosos o chapados, así como los artículos de joyería y relojería',
            ],
            15 => [
                'title' => 'Instrumentos' . PHP_EOL . 'musicales',
                'icon' => 'guitar',
                'desc' => 'instrumentos musicales, sus partes y accesorios',
            ],
            16 => [
                'title' => 'Material' . PHP_EOL . 'de oficina',
                'icon' => 'file-signature',
                'desc' => 'el papel, el cartón y ciertos productos de estos materiales, así como los artículos de oficina',
            ],
            17 => [
                'title' => 'Materiales' . PHP_EOL . 'de goma',
                'icon' => 'eraser',
                'desc' => 'materiales aislantes eléctricos, térmicos o acústicos y las materias plásticas utilizadas en procesos de fabricación, en forma de hojas, placas o varillas, así como ciertos productos de caucho, gutapercha, goma, amianto, mica y sus sucedáneos',
            ],
            18 => [
                'title' => 'Cuero',
                'icon' => 'cow',
                'desc' => 'el cuero, el cuero de imitación y ciertos productos de estos materiales',
            ],
            19 => [
                'title' => 'Materiales' . PHP_EOL . 'de construcción',
                'icon' => 'trowel-bricks',
                'desc' => 'materiales de construcción no metálicos',
            ],
            20 => [
                'title' => 'Muebles',
                'icon' => 'couch',
                'desc' => 'muebles y sus partes, así como ciertos productos de madera',
            ],
            21 => [
                'title' => 'Artículos' . PHP_EOL . 'del hogar',
                'icon' => 'wine-glass-empty',
                'desc' => 'pequeños aparatos y utensilios manuales para uso doméstico y culinario, así como utensilios de tocador y cosméticos, los artículos de cristalería y ciertos productos de porcelana, cerámica, loza, barro cocido o vidrio',
            ],
            22 => [
                'title' => 'Cuerdas' . PHP_EOL . 'y velas',
                'icon' => 'sailboat',
                'desc' => 'lonas y otros materiales para hacer velas de navegación, la cordelería, los materiales de acolchado y relleno, así como las materias textiles fibrosas en bruto',
            ],
            23 => [
                'title' => 'Hilos',
                'icon' => 'i-cursor',   // TODO: improve
                'desc' => 'hilos e hilados naturales o sintéticos para uso textil',
            ],
            24 => [
                'title' => 'Tejidos',
                'icon' => 'mattress-pillow',
                'desc' => 'tejidos y sus sucedáneos, ropa de hogar, cortinas de materias textiles o de materias plásticas',
            ],
            25 => [
                'title' => 'Ropa y' . PHP_EOL . 'calzado',
                'icon' => 'shirt',
                'desc' => 'prendas de vestir, el calzado y los artículos de sombrerería para personas',
            ],
            26 => [
                'title' => 'Adornos',
                'icon' => 'ring', // TODO: improve
                'desc' => 'artículos de mercería y pasamanería, los cabellos naturales o sintéticos y los adornos para el cabello, así como los artículos destinados a la decoración de objetos diversos',
            ],
            27 => [
                'title' => 'Alfombras',
                'icon' => 'rug',
                'desc' => 'productos destinados a recubrir o revestir, con el fin de acondicionar los suelos o paredes ya construidos',
            ],
            28 => [
                'title' => 'Juguetes' . PHP_EOL . 'y deportes',
                'icon' => 'volleyball',
                'desc' => 'los juguetes, los aparatos de juegos, los equipos de deportes, los artículos de entretenimiento y de broma, así como ciertos artículos para árboles de Navidad',
            ],
            29 => [
                'title' => 'Carnes, verduras' . PHP_EOL . 'y procesados',
                'icon' => 'drumstick-bite',
                'desc' => 'productos alimenticios de origen animal, las verduras, hortalizas y legumbres, así como comestibles preparados o en conserva para su consumo',
            ],
            30 => [
                'title' => 'Café, arroz' . PHP_EOL . 'y harinas',
                'icon' => 'bowl-rice',
                'desc' => 'productos alimenticios de origen vegetal, excepto las frutas, verduras, hortalizas y legumbres preparados o en conserva para su consumo, así como los aditivos para realzar el sabor de los alimentos',
            ],
            31 => [
                'title' => 'Productos' . PHP_EOL . 'agrícolas',
                'icon' => 'apple-whole',
                'desc' => 'productos de la tierra y del mar que no hayan sido procesados para su consumo, los animales vivos y las plantas vivas, así como los alimentos para animales',
            ],
            32 => [
                'title' => 'Bebidas s/ alcohol' . PHP_EOL . 'y cervezas',
                'icon' => 'bottle-droplet',
                'desc' => 'bebidas sin alcohol, siropes y otras preparaciones sin alcohol para elaborar bebidas, así como las cervezas',
            ],
            33 => [
                'title' => 'Vinos y' . PHP_EOL . 'licores',
                'icon' => 'wine-bottle',
                'desc' => 'bebidas alcohólicas, excepto cervezas, preparaciones alcohólicas para elaborar bebidas',
            ],
            34 => [
                'title' => 'Productos' . PHP_EOL . 'para fumadores',
                'icon' => 'smoking',
                'desc' => 'tabaco y los artículos para fumar, así como ciertos accesorios y recipientes relacionados con su uso',
            ],
            35 => [
                'title' => 'Publicidad',
                'icon' => 'rectangle-ad',
                'desc' => 'publicidad, gestión, organización y administración de negocios comerciales, trabajos de oficina',
            ],
            36 => [
                'title' => 'Servicios' . PHP_EOL . 'financieros',
                'icon' => 'piggy-bank',
                'desc' => 'servicios relativos a los servicios financieros, los servicios de valoración, así como las actividades de seguros y negocios inmobiliarios',
            ],
            37 => [
                'title' => 'Construcción' . PHP_EOL . 'y reparación',
                'icon' => 'trowel',
                'desc' => 'servicios de construcción, servicios de instalación y reparación, extracción minera, perforación de gas y de petróleo',
            ],
            38 => [
                'title' => 'Telecomunicaciones',
                'icon' => 'tower-cell',
                'desc' => 'servicios de telecomunicaciones, así como servicios de difusión y de transmisión de datos',
            ],
            39 => [
                'title' => 'Transporte' . PHP_EOL . 'y almacenamiento',
                'icon' => 'truck',
                'desc' => 'servicios de transporte, embalaje y almacenamiento de mercancías así como organización de viajes',
            ],
            40 => [
                'title' => 'Tratamiento' . PHP_EOL . 'de materiales',
                'icon' => 'recycle',
                'desc' => 'tratamiento de materiales, reciclaje de residuos y desechos, purificación del aire y tratamiento del agua, servicios de impresión, conservación de alimentos y bebidas',
            ],
            41 => [
                'title' => 'Educación y' . PHP_EOL . 'entretenimiento',
                'icon' => 'graduation-cap',
                'desc' => 'servicios de educación, formación así como servicios de entretenimiento y actividades deportivas y culturales',
            ],
            42 => [
                'title' => 'Ciencia y' . PHP_EOL . 'tecnología',
                'icon' => 'dna',
                'desc' => 'servicios prestados por personas en relación con sectores de actividades de alta complejidad (científicos, tecnológicos, investigación, diseño industrial, software y hardware)',
            ],
            43 => [
                'title' => 'Restauración',
                'icon' => 'utensils',
                'desc' => 'servicios de restauración (alimentación) y de hospedaje temporal',
            ],
            44 => [
                'title' => 'Medicina' . PHP_EOL . 'y veterinaria',
                'icon' => 'house-chimney-medical',
                'desc' => 'servicios médicos, veterinarios, tratamientos de higiene y de belleza para personas o animales así como servicios de agricultura, acuicultura, horticultura y silvicultura',
            ],
            45 => [
                'title' => 'Jurídicos' . PHP_EOL . 'y seguridad',
                'icon' => 'user-shield',
                'desc' => 'servicios jurídicos y de seguridad para la protección física de bienes materiales y persona',
            ],
            default => [
                'title' => '',
                'icon' => '',
                'desc' => '',
            ],
        };
    }

    public function getCountryInfo(string $short) : string
    {
        return match ($short) {
            'AD' => 'Andorra',
            'AE' => 'Emiratos Árabes Unidos',
            'AF' => 'Afganistán',
            'AG' => 'Antigua y Barbuda',
            'AI' => 'Anguilla',
            'AL' => 'Albania',
            'AM' => 'Armenia',
            'AN' => 'Antillas Neerlandesas',
            'AO' => 'Angola',
            'AR' => 'Argentina',
            'AS' => 'Samoa Oriental',
            'AT' => 'Austria',
            'AU' => 'Australia',
            'AW' => 'Aruba',
            'AZ' => 'Azerbaiyán',
            'BA' => 'Bosnia y Herzegovina',
            'BB' => 'Barbados',
            'BD' => 'Blangladesh',
            'BE' => 'Bélgica',
            'BF' => 'Burkina Faso',
            'BG' => 'Bulgaria',
            'BH' => 'Bahrein',
            'BI' => 'Burundi',
            'BJ' => 'Benin',
            'BM' => 'Bermuda',
            'BN' => 'Brunei Darussalam',
            'BO' => 'Bolivia',
            'BR' => 'Brasil',
            'BS' => 'Bahamas',
            'BT' => 'Bhutan',
            'BU' => 'Birmania',
            'BV' => 'Islas Bouvet',
            'BW' => 'Botswana',
            'BX' => 'Benelux',
            'BY' => 'Belarus',
            'BZ' => 'Belice',
            'CA' => 'Canadá',
            'CC' => 'Islas Coco',
            'CE' => 'Checoslovaquia',
            'CF' => 'República Centroafricana',
            'CG' => 'Congo',
            'CH' => 'Suiza',
            'CI' => 'Costa De Marfil',
            'CK' => 'Islas Cook',
            'CL' => 'Chile',
            'CM' => 'Camerún',
            'CN' => 'China',
            'CO' => 'Colombia',
            'CR' => 'Costa Rica',
            'CS' => 'Serbia y Montenegro',
            'CT' => 'Islas Caton y Endurbury',
            'CU' => 'Cuba',
            'CV' => 'Cabo Verde',
            'CW' => 'Curaçao',
            'CX' => 'Islas Christmas',
            'CY' => 'Chipre',
            'CZ' => 'República Checa',
            'DD' => 'Rep. Democrática Alemania',
            'DE' => 'Alemania',
            'DJ' => 'Djibouti',
            'DK' => 'Dinamarca',
            'DM' => 'Dominica',
            'DO' => 'República Dominicana',
            'DZ' => 'Argelia',
            'EC' => 'Ecuador',
            'EE' => 'Estonia',
            'EG' => 'Egipto',
            'EH' => 'República Árabe Saharaui Democrática',
            'EM' => 'Oficina De Armonización Del Mercado Interior',
            'EP' => 'Europa',
            'ES' => 'España',
            'ET' => 'Etiopia',
            'FI' => 'Finlandia',
            'FJ' => 'Fidji',
            'FK' => 'Islas Falkland (Malvinas)',
            'FO' => 'Islas Faroe',
            'FR' => 'Francia',
            'GA' => 'Gabón',
            'GB' => 'Reino Unido',
            'GD' => 'Granada',
            'GE' => 'Georgia',
            'GF' => 'Guyana Francesa',
            'GG' => 'Guernsey',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GL' => 'Groelandia',
            'GM' => 'Gambia',
            'GN' => 'Guinea',
            'GP' => 'Guadalupe',
            'GQ' => 'Guinea Ecuatorial',
            'GR' => 'Grecia',
            'GT' => 'Guatemala',
            'GU' => 'Guam',
            'GW' => 'Guinea-Bissau',
            'GY' => 'Guyana',
            'HK' => 'Hong Kong',
            'HM' => 'Islas Heard y Mac Donald',
            'HN' => 'Honduras',
            'HR' => 'Croacia',
            'HT' => 'Haití',
            'HU' => 'Hungria',
            'HV' => 'Alto Volta',
            'IB' => 'Iib',
            'ID' => 'Indonesia',
            'IE' => 'Irlanda',
            'IL' => 'Israel',
            'IM' => 'Isla De Man',
            'IN' => 'India',
            'IQ' => 'Irak',
            'IR' => 'República Islamica De Irán',
            'IS' => 'Islandia',
            'IT' => 'Italia',
            'JM' => 'Jamaica',
            'JO' => 'Jordania',
            'JP' => 'Japón',
            'JT' => 'Atolón Johnston',
            'KE' => 'Kenya',
            'KG' => 'Kirguistán',
            'KH' => 'Camboya',
            'KI' => 'Kiribati',
            'KM' => 'Comoras',
            'KN' => 'St. Kitts y Nevis',
            'KP' => 'Corea Del Norte',
            'KR' => 'Corea Del Sur',
            'KW' => 'Kuwait',
            'KY' => 'Islas Caimán',
            'KZ' => 'Kazajstan',
            'LA' => 'Laos',
            'LB' => 'Líbano',
            'LC' => 'Santa Lucía',
            'LI' => 'Liechtenstein',
            'LK' => 'Sri Lanka',
            'LR' => 'Liberia',
            'LS' => 'Lesotho',
            'LT' => 'Lituania',
            'LU' => 'Luxemburgo',
            'LV' => 'Letonia',
            'LY' => 'Libia',
            'MA' => 'Marruecos',
            'MC' => 'Mónaco',
            'MD' => 'Moldova',
            'ME' => 'Montenegro',
            'MG' => 'Madagascar',
            'MI' => 'Islas Midway',
            'MK' => 'Ex Rep. Yugoslava De Macedonia',
            'ML' => 'Mali',
            'MM' => 'Myanmar',
            'MN' => 'Mongolia',
            'MO' => 'Macao',
            'MQ' => 'Martinica',
            'MR' => 'Mauritania',
            'MS' => 'Monserrat',
            'MT' => 'Malta',
            'MU' => 'Mauricio',
            'MV' => 'Maldivas',
            'MW' => 'Malawi',
            'MX' => 'México',
            'MY' => 'Malasia',
            'MZ' => 'Mozambique',
            'NA' => 'Namibia',
            'NC' => 'Nueva Caledonia',
            'NE' => 'Níger',
            'NF' => 'Norfolk',
            'NG' => 'Nigeria',
            'NI' => 'Nicaragua',
            'NL' => 'Paises Bajos',
            'NO' => 'Noruega',
            'NP' => 'Nepal',
            'NQ' => 'Tierra De La Reina Maud',
            'NR' => 'Nauru',
            'NT' => 'Zona Neutral',
            'NU' => 'Niue',
            'NZ' => 'Nueva Zelandia',
            'OA' => 'Oapi',
            'OM' => 'Omán',
            'PA' => 'Panamá',
            'PE' => 'Perú',
            'PF' => 'Polinesia Francesa',
            'PG' => 'Papua Nueva Guinea',
            'PH' => 'Filipinas',
            'PK' => 'Pakistán',
            'PL' => 'Polonia',
            'PM' => 'San Pedro y Miguelon',
            'PN' => 'Pitcairn',
            'PR' => 'Puerto Rico',
            'PT' => 'Portugal',
            'PW' => 'Palau',
            'PY' => 'Paraguay',
            'QA' => 'Qatar',
            'RE' => 'Reunión',
            'RH' => 'Rhodesia Del Sur',
            'RO' => 'Rumania',
            'RS' => 'Serbia',
            'RU' => 'Rusia',
            'RW' => 'Rwanda',
            'SA' => 'Arabia Saudita',
            'SB' => 'Islas Salomón',
            'SC' => 'Seychelles',
            'SD' => 'Sudán',
            'SE' => 'Suecia',
            'SG' => 'Singapur',
            'SH' => 'Santa Elena',
            'SI' => 'Eslovenia',
            'SJ' => 'Islas Svalbard y Juan Mayen',
            'SK' => 'Eslovaquia',
            'SL' => 'Sierra Leona',
            'SM' => 'San Marino',
            'SN' => 'Senegal',
            'SO' => 'Somalia',
            'SR' => 'Surinam',
            'ST' => 'Santo Tomé y Principe',
            'SU' => 'Union Soviética',
            'SV' => 'El Salvador',
            'SY' => 'Siria',
            'SZ' => 'Swazilandia',
            'TC' => 'Islas Turks y Caicos',
            'TD' => 'Chad',
            'TG' => 'Togo',
            'TH' => 'Tailandia',
            'TJ' => 'Tayikistán',
            'TK' => 'Tokelau',
            'TM' => 'Turkmenistán',
            'TN' => 'Tunez',
            'TO' => 'Tonga',
            'TR' => 'Turquia',
            'TT' => 'Trinidad y Tobago',
            'TV' => 'Tuvalu',
            'TW' => 'Taiwán',
            'TZ' => 'República Unida de Tanzania',
            'UA' => 'Ucrania',
            'UG' => 'Uganda',
            'UK' => 'Reino Unido',
            'US' => 'EEUU',
            'UY' => 'Uruguay',
            'UZ' => 'Uzbekistán',
            'VA' => 'Ciudad del Vaticano',
            'VC' => 'San Vicente y Granadinas',
            'VE' => 'Venezuela',
            'VG' => 'Islas Vírgenes Británicas',
            'VI' => 'Islas Vírgenes Americanas',
            'VN' => 'Vietnam',
            'VU' => 'Vanuatu',
            'WB' => 'Berlín Oriental',
            'WF' => 'Islas Wallis y Futuna',
            'WK' => 'Islas Wake',
            'WO' => 'Wipo (Ompi)',
            'WS' => 'Samoa',
            'YD' => 'Yemen Democrática',
            'YE' => 'Yemen',
            'YU' => 'Yugoslavia',
            'ZA' => 'Sudáfrica',
            'ZC' => 'Antillas Neozelandesas',
            'ZE' => 'Ceilan',
            'ZF' => 'Nyasa',
            'ZH' => 'Sarre',
            'ZI' => 'Siam',
            'ZJ' => 'Tanganika',
            'ZK' => 'Zanzibar',
            'ZM' => 'Zambia',
            'ZR' => 'Zaire',
            'ZW' => 'Zimbabwe',
            default => '',
        };
    }
}
