<?php

namespace App\Listeners\EveryFlight;

use App\Events\EveryFlight\FlightAdded;
use App\Events\EveryFlight\FlightStatusChanged;
use App\Events\EveryFlight\FlightTimeChanged;
use App\Models\EveryFlight\FlightStatus;
use Illuminate\Support\Facades\Log;

class FlightsEventSubscriber
{
    public function handleFlightAdded(FlightAdded $event) : void
    {
        $flightStatus = FlightStatus::create([
            'flight_id' => $event->flight->id,
            'new_status' => FlightStatus::getFromFlightStatus($event->flight->status),
        ]);
        Log::debug("flight added: {$event->flight->flight_number} ({$event->flight->status})");
    }

    public function handleFlightStatusChanged(FlightStatusChanged $event) : void
    {
        $flightStatus = FlightStatus::create([
            'flight_id' => $event->flight->id,
            'old_status' => FlightStatus::getFromFlightStatus($event->oldStatus),
            'new_status' => FlightStatus::getFromFlightStatus($event->flight->status),
        ]);
        Log::debug("flight status changed: {$event->flight->flight_number} ({$event->flight->status})");
    }

    public function handleFlightTimeChanged(FlightTimeChanged $event) : void
    {
        $flightStatus = FlightStatus::create([
            'flight_id' => $event->flight->id,
            'old_status' => FlightStatus::getFromFlightStatus($event->flight->status),
            'new_status' => FlightStatus::STATUS_TIMECHANGED,
        ]);
        Log::debug("flight time changed: {$event->flight->flight_number} ({$event->flight->status})");
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     * @return array
     */
    public function subscribe($events)
    {
        return [
            FlightAdded::class => 'handleFlightAdded',
            FlightStatusChanged::class => 'handleFlightStatusChanged',
            FlightTimeChanged::class => 'handleFlightTimeChanged',
        ];
    }
}
