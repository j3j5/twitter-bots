<?php

namespace App\Models\EveryPurchase;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\EveryPurchase\Provider
 *
 * @property int $id
 * @property string $name
 * @property int $rut
 * @property string|null $country
 * @property string|null $address
 * @property string|null $status
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\EveryPurchase\Item[] $items
 * @property-read int|null $items_count
 * @method static \Illuminate\Database\Eloquent\Builder|Provider newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Provider newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Provider query()
 * @method static \Illuminate\Database\Eloquent\Builder|Provider whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Provider whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Provider whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Provider whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Provider whereRut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Provider whereStatus($value)
 * @property int $doc
 * @method static \Illuminate\Database\Eloquent\Builder|Provider whereDoc($value)
 * @mixin \Eloquent
 */
class Provider extends Model
{
    use HasFactory;

    protected $connection = 'everypurchase';
    protected $guarded = [];
    public $timestamps = false;

    public function items() : HasMany
    {
        return $this->hasMany(Item::class);
    }

    public function getNameAttribute(string $value) : string
    {
        // Replace dots with a ONE-DOT-LEADER (U+2024) to avoid links on description
        return str_replace('.', '․', $value);
    }
}
