<?php

namespace App\Models\EveryPurchase;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PurchaseSource
 *
 * @property int $id
 * @property string $slug
 * @property string $name
 * @property string $rss
 * @property string $twitter_config
 * @property bool $active
 * @method static Builder|PurchaseSource active()
 * @method static Builder|PurchaseSource newModelQuery()
 * @method static Builder|PurchaseSource newQuery()
 * @method static Builder|PurchaseSource query()
 * @method static Builder|PurchaseSource whereActive($value)
 * @method static Builder|PurchaseSource whereId($value)
 * @method static Builder|PurchaseSource whereName($value)
 * @method static Builder|PurchaseSource whereRss($value)
 * @method static Builder|PurchaseSource whereSlug($value)
 * @method static Builder|PurchaseSource whereTwitterConfig($value)
 * @mixin \Eloquent
 */
class PurchaseSource extends Model
{
    use HasFactory;

    protected $connection = 'everypurchase';
    protected $guarded = [];
    protected $casts = [
        'active' => 'bool',
    ];
    public $timestamps = false;

    public function getTwitterConfigAttribute(string $value) : array
    {
        return config('services.twitter.' . $value, []);
    }

    public function scopeActive(Builder $query) : Builder
    {
        return $query->where('active', 1);
    }
}
