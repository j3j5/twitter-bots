<?php

namespace App\Models\EveryUruguayan;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use RuntimeException;

trait HasEducation
{
    public function getEstudiosTextAttribute() : string
    {
        if ($this->estudia) {
            $text = 'Actualmente estudio';
            if (!empty($this->tipo_centro)) {
                $text .= ' en un centro ' . strtolower($this->tipo_centro);
                if ($this->beca) {
                    $text .= ' con una beca ' . strtolower($this->tipo_beca);
                }
            }
            return $text;
        }
        return '';
    }

    public function getAlfabetizacionTextAttribute() : string
    {
        if ($this->edad <= 8) {
            return '';
        }
        switch ($this->alfabetizacion) {
            case 'No':
                return 'No sé leer ni escribir.';
            case 'Sí':
            case 'Sin dato':
            default:
                return '';
        }
    }

    public function getMaxEducationLevelAttribute() : string
    {
        if ($this->universidad === 'Sí, asistió') {
            return 'universidad';
        }

        if (in_array($this->asistio_media, ['Sí, asistió', 'Asiste actualmente'], true)) {
            return 'media';
        }

        if (in_array($this->asistio_primaria, ['Sí, asistió', 'Asiste actualmente'], true)) {
            return 'primaria';
        }
        return '';
    }

    public function getMaxEducationTextAttribute() : string
    {
        switch ($this->max_education_level) {
            case 'media':
                if ($this->años_aprobados_media_basico === '99') {
                    return '';
                }
                // With no data for bachillerato, we count it as 0 (not correct but ok)
                if ($this->años_aprobados_bachillerato === '99') {
                    $this->años_aprobados_bachillerato = '0';
                }

                $añosAcabados = (int) $this->años_aprobados_media_basico + (int) $this->años_aprobados_bachillerato;
                $text = Arr::random(['Terminé ', 'Acabé ', 'Completé ', 'Hice ']);
                if ($añosAcabados === 0) {
                    $text = 'No llegué a terminar 1º de liceo';
                    if (!empty($this->razon_no_termino_media)) {
                        $text .= ' ' . $this->razon_no_termino_media;
                    }
                    return $text;
                } elseif ($añosAcabados === 6) {
                    $text .= 'los 6 años';
                } else {
                    $text .= 'hasta ' . ($añosAcabados) . 'º';
                }
                $text .= ' de liceo';
                // Add centro
                if ($this->tipo_liceo === 'Público') {
                    $text .= ' en un centro público';
                } elseif ($this->tipo_liceo === 'Privado') {
                    $text .= ' en un centro privado';
                }

                if (!empty($this->razon_no_termino_media)) {
                    $text .= ', lo dejé ' . $this->razon_no_termino_media;
                }

                return $text;
            case 'primaria':
                $text = Arr::random(['Terminé ', 'Acabé ', 'Completé ']);
                if ($this->finalizo_primaria) {
                    return $text . 'la escuela primaria pero nunca fui al liceo';
                }
                $añosAcabados = ((int) $this->años_aprobados_primaria + (int) $this->años_aprobados_primaria_especial);
                return 'Solo ' . strtolower($text) . $añosAcabados . ' ' . Str::plural('año', $añosAcabados) . ' de primaria';
            case 'universidad': // Already on universidad_textdefault:
            default:
                return '';
        }
    }

    public function getEducacionTextAttribute() : string
    {
        if (!empty($this->universidad_text)) {
            return $this->universidad_text . '.';
        }

        if ($this->asiste_preescolar) {
            return $this->preescolar_text . '.';
        }

        if ($this->asiste_primaria) {
            return $this->primaria_text . '.';
        }

        if ($this->asiste_liceo) {
            return $this->educacion_media_text . '.';
        }

        // Include older than 12 who didn't finish primaria, or older than 18 who didn't finish secundaria
        if ($this->edad > 3 && $this->max_education_text !== '') {
            // TODO: Finish UTU level
            return $this->max_education_text . '.';
        }

        return '';
    }

    public function getPreescolarTextAttribute() : string
    {
        $text = '';
        if ($this->asiste_preescolar) {
            $text = "Voy a {$this->centro_preescolar}";
            if ($this->recibe_comida_preescolar) {
                $text .= ", {$this->comidas_preescolar_text}";
            }
        }
        return $text;
    }

    public function getCentroPreescolarAttribute() : string
    {
        switch ($this->tipo_centro_preescolar) {
            case 'Privado':
            case 'Público':
                /** @phpstan-ignore-next-line */ // https://github.com/phpstan/phpstan/issues/4413
                return 'preescolar en un centro ' . strtolower($this->tipo_centro_preescolar);
            case 'CAIF / CAPI / Nuestros Niños':
                return 'un CAIF (o similar)';
            default:
                return 'preescolar';
        }
    }

    public function getComidasPreescolarTextAttribute() : string
    {
        return $this->getComidasText('preescolar');
    }

    public function getAsistePreescolarAttribute() : bool
    {
        return $this->asistio_preescolar === 'Asiste actualmente';
    }

    public function getAsistePrimariaAttribute() : bool
    {
        return $this->asistio_primaria === 'Asiste actualmente';
    }

    public function getPrimariaTextAttribute() : string
    {
        $text = '';
        if (!$this->asiste_primaria) {
            return $text;
        }
        $text = Arr::random(['Voy a ', 'Estoy cursando ', 'Estoy haciendo ', 'Hago '])
            . (intval($this->años_aprobados_primaria) + intval($this->años_aprobados_primaria_especial) + 1)
            . 'º ';
        if ($this->tipo_centro_primaria === 'Privado') {
            $text .= 'de escuela ';
        }
        $text .= "en {$this->centro_primaria}";
        if ($this->recibe_comida_primaria) {
            $text .= ", {$this->comidas_primaria_text}";
        }
        return $text;
    }

    public function getComidasPrimariaTextAttribute() : string
    {
        return $this->getComidasText('primaria');
    }

    public function getCentroPrimariaAttribute() : string
    {
        switch ($this->tipo_centro_primaria) {
            case 'Privado':
                return 'un colegio';
            case 'Público':
                return 'una escuela pública';
            default:
                return '';
        }
    }

    public function getComidasText(string $nivel) : string
    {
        $variable = 'cantidad_comidas_semanales';
        if ($nivel === 'preescolar') {
            $level = 'pre';
        } elseif ($nivel === 'primaria') {
            $level = 'pri';
        } else {
            throw new RuntimeException('Invalid level of school ' . $nivel);
        }

        $text = '';
        $tiposComida = ['desayuno' => 0, 'almuerzo' => 0, 'merienda' => 0];
        foreach ($tiposComida as $tipo => $n) {
            $name = "{$variable}_{$tipo}_{$level}";
            $number = $this->$name;
            if ($tipo === 'merienda') {
                $tipo = 'meriendo';
            }
            $tiposComida[$tipo] = intval($number);
        }
        $comidas = array_filter($tiposComida);
        if (empty($comidas)) {
            return '';
        }

        $nrComidas = Arr::first($comidas);
        if ($nrComidas === 5) {
            $text = 'todos los días ' . collect($comidas)->keys()->join(', ', ' y ');
        } else {
            $text = $nrComidas . ' ' . Str::plural('día', $nrComidas) . ' a la semana ' . collect($comidas)->keys()->join(', ', ' y ');
        }

        if ($nivel === 'preescolar') {
            $text .= ' en el ';
            if ($this->tipo_centro_preescolar === 'CAIF / CAPI / Nuestros Niños') {
                $text .= 'CAIF';
            } else {
                $text .= 'jardín';
            }
        } else {
            $tipoCentro = "tipo_centro_{$nivel}";
            $text .= ($this->$tipoCentro === 'Público') ? ' en la escuela' : ' en el colegio';
        }

        return $text;
    }

    public function getAsisteLiceoAttribute() : bool
    {
        return $this->asistio_media === 'Asiste actualmente';
    }

    public function getCentroMediaAttribute() : string
    {
        switch ($this->tipo_liceo) {
            case 'Privado':
                return 'en un colegio';
            case 'Público':
                return 'en un liceo público';
            default:
                return 'de liceo';
                // return '';
        }
    }

    public function getEducacionMediaTextAttribute() : string
    {
        $text = '';
        if (!$this->asiste_liceo) {
            return $text;
        }
        // $text = Arr::random(['Voy a ', 'Estoy cursando ', 'Estoy haciendo ', 'Estoy en '])
        $text = Arr::random(['Voy a ', 'Estoy cursando ', 'Estoy haciendo ', 'Curso ', 'Hago '])
            . (intval($this->años_aprobados_media_basico) + intval($this->años_aprobados_bachillerato) + 1)
            . 'º ';
        if ($this->tipo_liceo === 'Privado') {
            $text .= 'de liceo ';
        }
        $text .= $this->centro_media;

        return $text;
    }

    public function getUniversidadTextAttribute() : string
    {
        switch ($this->universidad) {
            case 'Sí, asistió':
                // Fue y terminó
                if ($this->termino_universidad) {
                    return 'Fui a ' . ($this->universidad_nombre ?: 'la universidad') . ", terminé los {$this->años_aprobados_uni} años de {$this->carrera}";
                }
                // Fue pero no hizo ni un año
                if (intval($this->años_aprobados_uni) < 1) {
                    return '';
                }
                // Fue pero nunca terminó
                $text = "Terminé {$this->años_aprobados_uni} "
                    . Str::plural('año', intval($this->años_aprobados_uni)) . ' de ';
                $text .= "{$this->carrera} ";
                if (!empty($this->universidad_nombre)) {
                    $text .= "en {$this->universidad_nombre}";
                } else {
                    $text .= 'universitaria';
                }
                $text .= ' pero lo dejé';
                return $text;
            case 'Asiste actualmente':
                if ($this->años_aprobados_uni === '0') {
                    $text = 'Este año empecé ';
                } elseif (intval($this->años_aprobados_uni) > 0) {
                    $text = "Estoy cursando ({$this->años_aprobados_uni} " . Str::plural('año', intval($this->años_aprobados_uni)) . ' terminados) ';
                } else {
                    $text = 'Estoy cursando ';
                }

                $text .= "{$this->carrera} ";
                if (!empty($this->universidad_nombre)) {
                    $text .= "en {$this->universidad_nombre}";
                } else {
                    $text .= 'universitaria';
                }
                // no break
            case 'Sin dato':
            case 'No asistió':
            default:
                return '';
        }
    }

    public function getUniversidadNombreAttribute() : string
    {
        switch ($this->tipo_universidad) {
            case 'Público':
                return 'una univ. pública';
            case 'Privado':
                return 'una univ. privada';
            case 'Sin dato':
                return 'la universidad';
            default:
                return '';
        }
    }

    public function getAñosAprobadosUniAttribute(?string $value) : string
    {
        if (is_null($value) || $value === '99') {
            return '';
        }

        if ($value === '.1') {
            return '0';
        }

        return (string) $value;
    }

    public function getCarreraAttribute(?string $value) : string
    {
        switch ($value) {
            case 'Programas Terciarios de Ciencias Sociales':
                return 'una carrera de CCSS';
            case 'Programas Terciarios de Enseñanza Comercial y Administración':
                return 'una carrera de Comercio y Admón';
            case 'Programas Terciarios de Arquitectura y Construcción':
                return 'una carrera de Arquitectura';
            case 'Programas Terciarios de Periodismo e Información':
                return 'una carrera de Periodismo';
            case 'Programas Terciarios de Matemáticas y Estadística':
                return 'una carrera de Matemáticas';
            case 'Programas Terciarios de Agricultura, Silvicultura y Pesca':
                return 'una carrera de Agricultura';
            case 'Programas Terciarios de Servicios de Transporte':
                return 'una carrera de Transporte';
            case 'Programas Terciarios de Servicios de Seguridad':
                return 'una carrera de Seguridad';
            case 'Programas Terciarios de Informática':
            case 'Programas Terciarios de Medicina':
            case 'Programas Terciarios de Veterinaria':
            case 'Programas Terciarios de Servicios Sociales':
            case 'Programas Terciarios de Derecho':
            case 'Programas Terciarios de Gestión Ambiental':
            case 'Programas Terciarios de Humanidades':
            case 'Programas Terciarios de Ciencias de la Vida':
            case 'Programas Terciarios de Artes':
            case 'Programas Terciarios de Formación de Personal Docente':

            case 'Programas Terciarios de Industria y Producción':
            case 'Programas Terciarios de Servicios Personales':
                /** @phpstan-ignore-next-line */ // https://github.com/phpstan/phpstan/issues/4413
                return str_replace('Programas Terciarios ', 'una carrera ', $value);
            case 'Programas Terciarios de Ingeniería y Profesiones Afines':
                return 'una carrera de Ingeniería';
            case 'Programas Terciarios de Ciencias Físicas, Químicas y Biológi':
                return 'una carrera de Ciencias Físicas, Químicas y Biológicas';
            default:
                return 'una carrera';
        }
    }

    public function getRazonNoTerminoMediaAttribute(?string $razon) : string
    {
        switch ($razon) {
            case 'Le interesaba aprender otras cosas':
                return 'porque me interesaba aprender otras cosas';
            case 'Comenzó a trabajar':
                return 'porque comencé a trabajar';
            case 'Le resultaban difíciles las materias':
                return 'porque las materias se me hacían difíciles';
            case 'Quedó usted o su pareja embarazada':
                return $this->sexo === 'Mujer' ? 'porque quedé embarazada' : 'porque mi pareja quedó embarazada';
            case 'No tenía interés':
                return 'porque perdí el interés';
            case 'Otras razones':
                return 'por motivos personales';
            case 'Porque tuvo que atender asuntos familiares':
                return 'porque tuve que atender asuntos familiares';
            case 'Por dificultades económicas':
                /** @phpstan-ignore-next-line */ // https://github.com/phpstan/phpstan/issues/4413
                return strtolower($razon);
            case 'Sin dato':
            default:
                return '';
        }
    }

    public function getTipoBecaAttribute(?string $value) : string
    {
        if ($value === 'beca MEC') {
            return 'MEC';
        }
        return $this->forceString($value);
    }
}
