<?php

namespace App\Models\EveryUruguayan;

use Illuminate\Support\Str;
use NumberFormatter;

trait HasJob
{
    public function getActividadIngresosTextAttribute() : string
    {
        $text = '';
        if (!empty($this->actividad_text)) {
            $text = $this->actividad_text;
        }

        if (empty($text)) {
            $text .= $this->ingresos_text;
        } elseif (!empty($this->ingresos_text)) {
            $text .= ' y ' . strtolower($this->ingresos_text);
        } else {
            $text .= '.';
        }

        return $text;
    }

    public function getAportesTextAttribute() : string
    {
        if (!$this->aporta_caja) {
            return '';
        }
        $text = 'Aporto a';
        switch ($this->caja_aportes) {
            case 'BPS y AFAP':
            case 'BPS':
                return "$text {$this->caja_aportes}.";
            case 'Militar':
            case 'Bancaria':
            case 'Profesional':
            case 'Policial':
            case 'Notarial':
                return "$text la Caja {$this->caja_aportes}.";
            case 'En el exterior':
                return "$text en el exterior.";
            default:
                return '';
        }
    }

    public function getActividadTextAttribute() : string
    {
        switch ($this->condicion_actividad) {
            case 'Ocupados':
                if (is_null($this->jornada_laboral)) {
                    return '';
                }
                $text = "Trabajo {$this->jornada_laboral} " . Str::plural('hora', $this->jornada_laboral) . ' a la semana';
                if ($this->trabajo_antes) {
                    $text .= ' en mi primer trabajo';
                }
                return $text;
            case 'Desocupados BT1V':
                return 'Estoy buscando trabajo por primera vez';
            case 'Desocupados propiamente dichos':
                $text = 'Estoy desocupad';
                break;
            case 'Desocupados en seguro de desempleo':
                $text = 'Estoy en el seguro de paro';
                if (!empty($this->tiempo_dejo_empleo)) {
                    $text .= ' desde hace ' . $this->tiempo_dejo_empleo;
                }
                // TODO: Make sure this is correct
                // if (!$this->trabajo_antes) {
                //     $text .= ' (era mi primer laburo)';
                // }
                if (!empty($this->razon_dejar_trabajo_text)) {
                    $text .= " $this->razon_dejar_trabajo_text";
                }

                if (!empty($this->categoria_anterior_trabajo)) {
                    $text .= ', ' . $this->categoria_anterior_trabajo_text;
                }

                return $text;
            case 'Inactivo, realiza los quehaceres del hogar':
                return 'Me dedico a los quehaceres del hogar';
            case 'Inactivo, rentista':
                return 'Soy rentista';
            case 'Inactivo, pensionista':
                return 'Recibo una pensión';
            case 'Inactivo, jubilado':
                $text = 'Estoy jubilad';
                break;
            case 'Inactivo, estudiante':
            case 'Inactivo, otro':
            case 'Menor 14 años':
            default:
                return '';
        }
        $text .= $this->genero_gramatical === Uruguayan::FEMENINO ? 'a' : 'o';
        return $text;
    }

    public function getIngresosTextAttribute() : string
    {
        if ($this->ingresos > 0) {
            $fmt = new NumberFormatter('es_UY', NumberFormatter::DECIMAL);
            $fmt->setAttribute(NumberFormatter::FRACTION_DIGITS, 0);
            $verbo = $this->condicion_actividad === 'Ocupado' ? 'Gano' : 'Cobro';
            return $verbo . ' $' . $fmt->formatCurrency($this->ingresos, 'UYU') . ' al mes.';
        }
        return '';
    }

    public function getCategoriaAnteriorTrabajoTextAttribute() : string
    {
        switch ($this->categoria_anterior_trabajo) {
            case Str::startsWith($this->categoria_anterior_trabajo, 'asalariad'):
            case 'miembro de una cooperativa':
            case 'patrón':
            case 'patrona':
                return "era {$this->categoria_anterior_trabajo}";
            case Str::startsWith($this->categoria_anterior_trabajo, 'cuenta propia'):
                return "trabajaba por {$this->categoria_anterior_trabajo}";
            case 'miembro del hogar':
                return 'trabajaba para un miembro de mi hogar';
            default:
                return '';
        }
    }

    public function getRazonDejarTrabajoTextAttribute() : string
    {
        switch ($this->razon_dejar_trabajo) {
            case 'Acabó la zafra':
                return 'porque acabó la zafra';
            case 'Cierre del establecimiento':
                return 'porque la empresa cerró';
            case 'Despido':
                return 'porque me despidieron';
            case 'Finalización del contrato':
                return 'porque se me acabó el contrato';
            case 'Mal pago':
                return 'porque era mal pago';
            case 'Razones de estudio':
                return 'por razones de estudio';
            case 'Razones familiares':
                return 'por motivos familiares';
            case 'Se jubiló':
                return 'porque me jubilé';
            case 'Otras razones':
            default:
                return '';
        }
    }

    public function getCategoriaAnteriorTrabajoAttribute(?string $value) : string
    {
        switch ($value) {
            case 'Asalariado/a privado/a':
                return 'asalariad' . ($this->genero_gramatical === Uruguayan::FEMENINO ? 'a' : 'o') . ' privad' . ($this->genero_gramatical === Uruguayan::FEMENINO ? 'a' : 'o');
            case 'Asalariado/a público/a':
                return 'asalariad' . ($this->genero_gramatical === Uruguayan::FEMENINO ? 'a' : 'o') . ' públic' . ($this->genero_gramatical === Uruguayan::FEMENINO ? 'a' : 'o');
            case 'Miembro de cooperativa de producción o trabajo':
                return 'miembro de una cooperativa';
            case 'Patrón/a':
                return 'patr' . ($this->genero_gramatical === Uruguayan::FEMENINO ? 'ona' : 'ón');
            case 'Cuenta propia sin local ni inversión':
            case 'Cuenta propia con local o inversión':
                /** @phpstan-ignore-next-line */ // https://github.com/phpstan/phpstan/issues/4413
                return strtolower($value);
            case 'Miembro del hogar no remunerado':
                return 'miembro del hogar';
            case 'Trabajador/a de un programa social de empleo':
                return 'trabajad' . ($this->genero_gramatical === Uruguayan::FEMENINO ? 'ora' : 'or') . ' de un programa social de empleo';
            case 'Sin dato':
            default:
                return '';
        }
    }
}
