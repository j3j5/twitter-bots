<?php

namespace App\Models\PoliticosAlert;

use App\DTO\Twitter\User;
use App\Jobs\PoliticosAlert\CheckFollowerGrowth;
use App\Jobs\PoliticosAlert\DownloadAvatar;
use App\Services\TwitterHelper;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;

use function Safe\json_decode;
use function Safe\json_encode;

/**
 * App\Models\PoliticosAlert\TwitterUser
 *
 * @property int $id
 * @property string $twitter_id
 * @property string $username
 * @property string $description
 * @property string $location
 * @property string $url
 * @property int $protected
 * @property int $verified
 * @property int $followers_count
 * @property int $friends_count
 * @property int $listed_count
 * @property \Illuminate\Support\Carbon $created_at
 * @property int $favourites_count
 * @property int $statuses_count
 * @property string $avatar
 * @property string $banner
 * @property string $withheld_in_countries
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterUser whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterUser whereBanner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterUser whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterUser whereFavouritesCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterUser whereFollowersCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterUser whereFriendsCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterUser whereListedCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterUser whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterUser whereProtected($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterUser whereStatusesCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterUser whereTwitterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterUser whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterUser whereUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterUser whereVerified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterUser whereWithheldInCountries($value)
 * @property string $name
 * @property-read \App\Models\PoliticosAlert\FriendsList|null $friends
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterUser whereName($value)
 * @property-read string $avatar_filepath
 * @property-read int $pronoun
 * @mixin \Eloquent
 */
class TwitterUser extends Model
{
    use HasFactory;

    public const FOLDER = 'polalert/';

    public const HOURLY_THRESHOLD_PERC = 5;
    public const DAILY_THRESHOLD_PERC = 10;
    public const HOURLY_THRESHOLD_COUNT = 200;
    public const DAILY_THRESHOLD_COUNT = 1000;

    public const THEIR = 0;
    public const HER = 1;
    public const HIS = 2;

    protected $connection = 'pol-alert';

    protected $guarded = ['id'];

    protected $casts = [
        'protected' => 'bool',
        'verified' => 'bool',
        'withheld_in_countries' => 'array',
    ];
    public $timestamps = false;

    protected string $hourlyFollowersFilename = 'hourly_followers.json';
    protected string $dailyFollowersFilename = 'daily_followers.json';

    public static function createOrUpdateFromTweet(array $twitterUser) : self
    {
        $user = new User($twitterUser);

        $unique = [
            'twitter_id' => $user->getId(),
        ];
        $data = [
            'username' => $user->getUsername(),
            'name' => $user->getName(),
            'description' => $user->description,
            'location' => (string) $user->getLocation(),
            'url' => $user->getUrl(),
            'protected' => (bool) $user->protected,
            'verified' => (bool) $user->verified,
            'followers_count' => $user->followers_count,
            'friends_count' => $user->friends_count,
            'listed_count' => $user->listed_count,
            'created_at' => Carbon::parse($user->getCreatedAt()),
            'favourites_count' => $user->favourites_count,
            'statuses_count' => $user->statuses_count,
            'avatar' => $user->getAvatar(),
            'banner' => $user->getBanner(),
            'withheld_in_countries' => $user->withheld_in_countries,
        ];

        $twitterUser = self::updateOrCreate($unique, $data);
        CheckFollowerGrowth::dispatchSync($twitterUser);

        // Download the first time
        DownloadAvatar::dispatch($twitterUser);

        return $twitterUser;
    }

    public function friends() : HasOne
    {
        return $this->hasOne(FriendsList::class, 'user_id');
    }

    /**
     * Get the user's name.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function name(): Attribute
    {
        return Attribute::make(
            // Replace dots with a ONE-DOT-LEADER (U+2024) to avoid links on tweets
            // Replace hashtag with MUSIC SHARP SIGN (U+266F) to avoid tweeting hashtags
            get: [app(TwitterHelper::class), 'safeText']
        );
    }

    public function avatar() : Attribute
    {
        return Attribute::make(
            get: fn ($value) => str_replace('_normal', '_400x400', $value)
        );
    }

    public function getPronounAttribute() : int
    {
        // TODO: implement, look at the bio and try to regex with the classic expression
        //  if the locale is set to english
        return self::THEIR;
    }

    public function getAvatarFilepathAttribute() : string
    {
        return self::FOLDER . $this->twitter_id . '/avatar.jpg';
    }

    public function getAvatarFile() : string
    {
        if (!Storage::fileExists($this->avatar_filepath)) {
            DownloadAvatar::dispatchSync($this);
        }

        return (string) Storage::get($this->avatar_filepath);
    }

    /**
     * @return array{0: string, 1: int}
     */
    public function getHourlyFollowersData() : array
    {
        $file = self::FOLDER . $this->twitter_id . '/' . $this->hourlyFollowersFilename;
        if (!Storage::fileExists($file)) {
            return $this->updateHourlyFollowersData();
        }

        return json_decode((string) Storage::get($file), true);
    }

    /**
     * @return array{0: string, 1: int}
     */
    public function updateHourlyFollowersData() : array
    {
        $file = self::FOLDER . $this->twitter_id . '/' . $this->hourlyFollowersFilename;
        $data = [now()->toIso8601String(), $this->followers_count];
        Storage::put($file, json_encode($data));
        return $data;
    }

    /**
     * @return array{0: string, 1: int}
     */
    public function getDailyFollowersData() : array
    {
        $file = self::FOLDER . $this->twitter_id . '/' . $this->dailyFollowersFilename;
        if (!Storage::fileExists($file)) {
            return $this->updateDailyFollowersData();
        }

        return json_decode((string) Storage::get($file), true);
    }

    /**
     * @return array{0: string, 1: int}
     */
    public function updateDailyFollowersData() : array
    {
        $file = self::FOLDER . $this->twitter_id . '/' . $this->dailyFollowersFilename;
        $data = [now()->toIso8601String(), $this->followers_count];
        Storage::put($file, json_encode($data));
        return $data;
    }
}
