<?php

namespace App\Models\PoliticosAlert;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PoliticosAlert\TwitterList
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $name
 * @property string $list_id
 * @property string $owner_id
 * @property string $owner_username
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterList newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterList newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterList query()
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterList whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterList whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterList whereListId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterList whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterList whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterList whereOwnerUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TwitterList whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TwitterList extends Model
{
    use HasFactory;

    protected $connection = 'pol-alert';
    protected $table = 'lists';
    protected $guarded = ['id'];
}
