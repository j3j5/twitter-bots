<?php

namespace App\Models\EveryFlight;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use RuntimeException;

/**
 * App\Models\EveryFlight\FlightStatus
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $flight_id
 * @property string|null $old_status
 * @property string $new_status
 * @property string|null $tweet_id
 * @property string|null $tweeted_at
 * @property-read \App\Models\EveryFlight\Flight $flight
 * @property-read \Illuminate\Database\Eloquent\Collection|FlightStatus[] $siblings
 * @property-read int|null $siblings_count
 * @method static \Illuminate\Database\Eloquent\Builder|FlightStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FlightStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FlightStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|FlightStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FlightStatus whereFlightId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FlightStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FlightStatus whereNewStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FlightStatus whereOldStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FlightStatus whereTweetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FlightStatus whereTweetedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FlightStatus whereUpdatedAt($value)
 * @method static Builder|FlightStatus toBeTweeted()
 * @mixin \Eloquent
 */
class FlightStatus extends Model
{
    use HasFactory;

    public const STATUS_ONTIME = 'en horario';
    public const STATUS_LANDED = 'aterrizado';
    public const STATUS_DEPARTED = 'despegado';
    public const STATUS_LASTCALL = 'ultimo aviso';
    public const STATUS_DELAYED = 'demorado';
    public const STATUS_RESCHEDULED = 'reprogramado';
    public const STATUS_CHECKING_IN = 'chequeando';
    public const STATUS_CANCELLED = 'cancelado';
    public const STATUS_ESTIMATED = 'estima';
    public const STATUS_SURROUNDINGS = 'en zona';
    public const STATUS_PREBOARDING = 'pre embarque';
    public const STATUS_BOARDING = 'embarcando';
    public const STATUS_CLOSED = 'cerrado';
    public const STATUS_DIVERTED = 'desviado';
    public const STATUS_DOORCHANGED = 'cambio puerta';
    public const STATUS_TIMECHANGED = 'cambio de horario';
    public const STATUS_DELAYED_METEO = 'demorado meteo';

    public static array $flightStatuses = [
        self::STATUS_ONTIME,
        self::STATUS_LANDED,
        self::STATUS_DEPARTED,
        self::STATUS_LASTCALL,
        self::STATUS_DELAYED,
        self::STATUS_DELAYED_METEO,
        self::STATUS_RESCHEDULED,
        self::STATUS_CHECKING_IN,
        self::STATUS_CANCELLED,
        self::STATUS_ESTIMATED,
        self::STATUS_SURROUNDINGS,
        self::STATUS_PREBOARDING,
        self::STATUS_CLOSED,
        self::STATUS_BOARDING,
        self::STATUS_DIVERTED,
        self::STATUS_DOORCHANGED,
    ];

    protected $guarded = ['id'];
    protected $connection = 'everyflight';

    public function flight() : BelongsTo
    {
        return $this->belongsTo(Flight::class);
    }

    public function siblings() : HasMany
    {
        return $this->hasMany(FlightStatus::class, 'flight_id', 'flight_id')->where('id', '!=', $this->id);
    }

    public function getTweet() : string
    {
        switch ($this->new_status) {
            case self::STATUS_ONTIME:
                $text = "{$this->flight->emoji} El vuelo {$this->flight->description} está programado para {$this->flight->time}.";
                break;
            case self::STATUS_LANDED:
                $text = "{$this->flight->emoji} El vuelo {$this->flight->description} ha aterrizado {$this->flight->actual_time}.";
                break;
            case self::STATUS_DEPARTED:
                $text = "{$this->flight->emoji} El vuelo {$this->flight->description} ha despegado {$this->flight->actual_time}.";
                break;
            case self::STATUS_DELAYED:
            case self::STATUS_DELAYED_METEO:
                $text = "{$this->flight->emoji} El vuelo {$this->flight->description} se encuentra demorado, hora estimada de ";
                $text .= $this->flight->isArrival() ? 'llegada' : 'despegue';
                $text .= " {$this->flight->most_accurate_time}.";
                break;
            case self::STATUS_LASTCALL:
                $text = "{$this->flight->emoji} Último aviso para los pasajeros del vuelo {$this->flight->description}, despegará {$this->flight->most_accurate_time}.";
                break;
            case self::STATUS_CHECKING_IN:
                $text = "{$this->flight->emoji} Check-in abierto para el vuelo {$this->flight->description}, saldrá de Montevideo {$this->flight->most_accurate_time}.";
                break;
            case self::STATUS_SURROUNDINGS:
                $text = "{$this->flight->emoji} El vuelo {$this->flight->description} previsto para {$this->flight->time} ha entrado la zona aérea de Montevideo.";
                break;
            case self::STATUS_CANCELLED:
                return "{$this->flight->emoji} El vuelo {$this->flight->description} programado para {$this->flight->time} ha sido cancelado.";
            case self::STATUS_RESCHEDULED:
                $text = "{$this->flight->emoji} El vuelo {$this->flight->description} previsto para {$this->flight->time} ha sido reprogramado. ";
                $text .= 'Se recomienda revisar la página para futuras actualizaciones https://www.aeropuertodecarrasco.com.uy/vuelos/';
                return $text;
            case self::STATUS_SURROUNDINGS:
                $text = "{$this->flight->emoji} El vuelo {$this->flight->description} previsto para {$this->flight->time} ha entrado la zona aérea de Montevideo.";
                break;
            case self::STATUS_PREBOARDING:
                $text = "{$this->flight->emoji} El vuelo {$this->flight->description} empezó los preparativos para el embarque, el despegue está previsto para {$this->flight->most_accurate_time}.";
                break;
            case self::STATUS_BOARDING:
                $text = "{$this->flight->emoji} El vuelo {$this->flight->description} comenzó el embarque, despegue previsto para {$this->flight->most_accurate_time}.";
                break;
            case self::STATUS_CLOSED:
                $text = "{$this->flight->emoji} El embarque para el vuelo {$this->flight->description} está cerrado, despegue {$this->flight->most_accurate_time}.";
                break;
            case self::STATUS_TIMECHANGED:
            case self::STATUS_ESTIMATED:
                $text = "{$this->flight->emoji} Se estima que el vuelo {$this->flight->description}";
                if ($this->flight->type === Flight::ARRIVAL) {
                    $text .= " llegue {$this->flight->most_accurate_time}.";
                } else {
                    $text .= " salga de Montevideo {$this->flight->most_accurate_time}.";
                }
                return $text;
            case self::STATUS_CANCELLED:
                return "{$this->flight->emoji} El vuelo {$this->flight->description} programado para {$this->flight->time} ha sido cancelado.";
            case self::STATUS_DIVERTED:
                $text = "{$this->flight->emoji} El vuelo {$this->flight->description} programado para {$this->flight->time} ha sido desviado.";
                break;
            case self::STATUS_DOORCHANGED:
            case self::STATUS_PREBOARDING:
                // return "{$this->flight->emoji} El vuelo {$this->flight->description} empezó los preparativos para el embarque, el despegue está previsto para {$this->flight->most_accurate_time}.";
                return '';
            default:
                throw new RuntimeException("El vuelo {$this->flight->description} está en estado desconocido.");
        }

        if ($this->flight->shouldIncludeRadarLink()) {
            $text .= PHP_EOL . PHP_EOL . '🗺️ ' . $this->flight->radar_url;
        }

        return $text;
    }

    public function markAsTweeted(string $tweetId) : void
    {
        $this->tweeted_at = now();
        if ($tweetId !== '') {
            $this->tweet_id = $tweetId;
        }

        $this->save();
    }

    public static function getFromFlightStatus(string $status) : string
    {
        $status = mb_strtolower($status);
        if (!in_array($status, self::$flightStatuses)) {
            throw new RuntimeException('Unknown flight status: ' . $status);
        }
        return $status;
    }

    public function scopeToBeTweeted(Builder $query) : Builder
    {
        return $query->whereNull('tweeted_at');
    }
}
