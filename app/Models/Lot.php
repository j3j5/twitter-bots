<?php

namespace App\Models;

use App\Services\Google\Geocode;
use App\Services\Google\MapsShortener;
use App\Services\PolygonHelper;
use Exception;
use Http\Client\Exception\RequestException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Client\HttpClientException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use proj4php\Point;
use proj4php\Proj;
use proj4php\Proj4php;
use function Safe\json_encode;
use Shapefile\ShapefileReader;

/**
 * App\Models\Lot
 *
 * @property int $id
 * @property string $lat
 * @property string $long
 * @property string $punto_wkb
 * @property string $codigo_postal
 * @property string $nombre_via
 * @property string $num_puerta
 * @property string $letra_puerta
 * @property string $km
 * @property string $manzana
 * @property string $solar
 * @property string $nombre_inmueble
 * @property string $localidad
 * @property string $departamento
 * @property bool $maps_tried
 * @property string|null $geocode_lat
 * @property string|null $geocode_long
 * @property string|null $user_id
 * @property string|null $username
 * @property string|null $tweet_id
 * @property string|null $tweeted_at
 * @property-read string $address
 * @property-read string $filename
 * @property-read string $human_adddress
 * @method static \Illuminate\Database\Eloquent\Builder|Lot googleVirgin()
 * @method static \Illuminate\Database\Eloquent\Builder|Lot newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Lot newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Lot notTweetedYet()
 * @method static \Illuminate\Database\Eloquent\Builder|Lot query()
 * @method static \Illuminate\Database\Eloquent\Builder|Lot whereCodigoPostal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lot whereDepartamento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lot whereGeocodeLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lot whereGeocodeLong($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lot whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lot whereKm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lot whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lot whereLetraPuerta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lot whereLocalidad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lot whereLong($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lot whereManzana($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lot whereMapsTried($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lot whereNombreInmueble($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lot whereNombreVia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lot whereNumPuerta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lot wherePuntoWkb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lot whereSolar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lot whereTweetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lot whereTweetedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lot whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lot whereUsername($value)
 * @property string|null $google_lat
 * @property string|null $google_long
 * @property-read string $instant_streetview_url
 * @method static Builder|Lot whereGoogleLat($value)
 * @method static Builder|Lot whereGoogleLong($value)
 * @property-read string $hood_hashtag
 * @property-read string $neighborhood
 * @property-read string $twitter_address
 * @property-read string $human_address
 * @property-read string $streetview_url
 * @property array|null $metadata
 * @method static Builder|Lot whereMetadata($value)
 * @property string|null $gshort_url
 * @method static Builder|Lot whereGshortUrl($value)
 * @property-read string $short_street_view_url
 * @method static Builder|Lot alreadyTweeted()
 * @property int|null $via
 * @method static Builder|Lot whereVia($value)
 * @mixin \Eloquent
 */
class Lot extends Model
{
    use HasFactory;

    protected $connection = 'everylot';
    protected $guarded = ['id'];
    protected $casts = [
        'metadata' => 'array',
    ];
    public $timestamps = false;

    private Proj4php $projector;
    private ShapefileReader $hoodsShape;
    private Proj $hoodsProjection;
    private Proj $googleProjection;
    private string $hood = '';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        // Open Shapefile
        $this->projector = app(Proj4php::class);
        $this->hoodsShape = app()->makeWith(ShapefileReader::class, [
            'files' => resource_path('shapes/ine_barrios_mvd/ine_barrios_mvd.shp'),
        ])->setCharset('UTF-8');

        // Your initial projection information from your .prj file
        $this->hoodsProjection = new Proj($this->hoodsShape->getPRJ(), $this->projector);
        // WGS84 (for google map)
        $this->googleProjection = new Proj('EPSG:4326', $this->projector);
    }

    public function getFloors() : int
    {
        // TODO
        return 0;
    }

    public function getLocation() : string
    {
        if (empty($this->google_lat) || empty($this->google_long)) {
            try {
                $coordinates = app(Geocode::class)->getCoordinatesFromAddress($this->address);
                $this->google_lat = Arr::get($coordinates, 'lat');
                $this->google_long = Arr::get($coordinates, 'lng');
                Log::debug($this->address . ' got the following coordinates on Google: ' . $this->google_lat . ', ' . $this->google_long);
                $this->save();
            } catch (HttpClientException $e) {
                return $this->lat . ',' . $this->long;
            }
        }

        if ($this->areGoogleCoordsCloseEnough()) {
            Log::debug("Googles coords are close enough, using Google's coordinates for street view");
            return $this->google_lat . ',' . $this->google_long;
        }

        return $this->lat . ',' . $this->long;
    }

    public function areGoogleCoordsCloseEnough() : bool
    {
        // Magic number taken from @everylot
        // https://github.com/fitnr/everylotbot/blob/529826f01820b925803d13aa458baf3cc778f9c8/everylot/everylot.py#L132
        $d = 0.007;
        $min = [floatval($this->lat) - $d, floatval($this->long) - $d];
        $max = [floatval($this->lat) + $d, floatval($this->long) + $d];

        if ((float) $this->google_lat > $max[0] || (float) $this->google_lat < $min[0]) {
            return false;
        }
        if ((float) $this->google_long > $max[1] || (float) $this->google_long < $min[1]) {
            return false;
        }

        return true;
    }

    public function getNeighborhoodAttribute() : string
    {
        if ($this->hood !== '') {
            return $this->hood;
        }
        // Read all the records
        while ($hoodPolygonShape = $this->hoodsShape->fetchRecord()) {
            // Skip the record if marked as "deleted"
            if ($hoodPolygonShape->isDeleted()) {
                continue;
            }

            $data = $hoodPolygonShape->getDataArray();
            $geometry = $hoodPolygonShape->getArray();
            if (isset($geometry['parts'])) {
                foreach ($geometry['parts'] as $part) {
                    foreach ($part['rings'] as $ring) {
                        if ($this->isOnPolygon($ring['points'])) {
                            $this->hood = $this->beautifyName(Arr::get($data, 'NOMBBARR', ''));
                            return $this->beautifyName(Arr::get($data, 'NOMBBARR', ''));
                        }
                    }
                }
            } elseif (isset($geometry['rings'])) {
                foreach ($geometry['rings'] as $ring) {
                    if ($this->isOnPolygon($ring['points'])) {
                        $this->hood = $this->beautifyName(Arr::get($data, 'NOMBBARR', ''));
                        return $this->beautifyName(Arr::get($data, 'NOMBBARR', ''));
                    }
                }
            } else {
                throw new Exception('Unknown structure on array for hoodPolygonShape: ' . json_encode($geometry));
            }
        }
    }

    private function isOnPolygon(array $polygon) : bool
    {
        $polygonHood = collect($polygon)
            ->map(function (array $points) {
                // Your original point
                $srcPoint = new Point($points['x'], $points['y'], $this->hoodsProjection);
                // New point in WGS84 projection for Google Maps
                return $this->projector->transform($this->googleProjection, $srcPoint);
            })->filter();

        $currentPosition = new Point($this->long, $this->lat, $this->googleProjection);
        if (app(PolygonHelper::class)->pointInPolygon($currentPosition, $polygonHood->toArray())) {
            return true;
        }
        return false;
    }

    private function beautifyName(string $name) : string
    {
        $wrong = [
            'PQUE. BATLLE, V. DOLORES' => 'PARQUE BATLLE',
            'MAROÑAS, PARQUE GUARANI' => 'MAROÑAS',
            'FLOR DE MAROÑAS' => 'FLOR DE MAROÑAS',
            'PTA. RIELES, BELLA ITALIA' => 'PUNTA DE RIELES',
            'VILLA ESPAÑOLA' => 'VILLA ESPAÑOLA',
            'MERCADO MODELO, BOLIVAR' => 'MERCADO MODELO',
            'CASTRO, P. CASTELLANOS' => 'PEREZ CASTELLANOS',
            'MANGA, TOLEDO CHICO' => 'TOLEDO CHICO',
            'PEÑAROL, LAVALLEJA' => 'PEÑAROL',
            'CASABO, PAJAS BLANCAS' => 'CASABO',
            'LA PALOMA, TOMKINSON' => 'LA PALOMA',
            'PRADO, NUEVA SAVONA' => 'PRADO',
            'CAPURRO, BELLA VISTA' => 'CAPURRO',
            'LARRAÑAGA' => 'LARRAÑAGA',
            'VILLA MUÑOZ, RETIRO' => 'VILLA MUÑOZ',
            'TRES OMBUES, VICTORIA' => 'TRES OMBUES',
            'COLON SURESTE, ABAYUBA' => 'COLON',
            'COLON CENTRO Y NOROESTE' => 'COLON',
            'LEZICA, MELILLA' => 'LEZICA',
            'VILLA GARCIA, MANGA RUR.' => 'VILLA GARCIA',
        ];
        $name = Arr::get($wrong, $name, $name);
        return mb_convert_case($name, MB_CASE_TITLE);
    }

    public function getHoodHashtagAttribute() : string
    {
        if (!empty($this->neighborhood)) {
            return '#' . Str::studly($this->neighborhood);
        }
        return '';
    }

    public function getAddressAttribute() : string
    {
        return $this->nombre_via . ' '
            . $this->num_puerta . '-' . $this->letra_puerta . ', '
            . $this->localidad . ', ' . $this->departamento;
    }

    public function getHumanAddressAttribute() : string
    {
        $calle = mb_convert_case($this->nombre_via, MB_CASE_TITLE);
        $address = empty($this->nombre_inmueble)
            ? $calle . ' '
            : $this->nombre_inmueble . ' - ' . $calle . ' ';
        if (!empty($this->num_puerta)) {
            $address .= $this->num_puerta;
        }
        if (!empty($this->letra_puerta)) {
            $address .= '-' . $this->letra_puerta;
        }
        if (!empty($this->neighborhood)) {
            $address .= ' ' . $this->neighborhood;
        }

        return trim($address);
    }

    public function getTwitterAddressAttribute() : string
    {
        $calle = mb_convert_case($this->nombre_via, MB_CASE_TITLE);
        $address = empty($this->nombre_inmueble)
            ? $calle . ' '
            : $this->nombre_inmueble . ' - ' . $calle . ' ';
        if (!empty($this->num_puerta)) {
            $address .= $this->num_puerta;
        }
        if (!empty($this->letra_puerta)) {
            $address .= '-' . $this->letra_puerta;
        }
        if (!empty($this->hood_hashtag)) {
            $address .= ' ' . $this->hood_hashtag;
        }

        return trim($address);
    }

    public function getFilenameAttribute() : string
    {
        return storage_path('app/everylot/' . $this->id . '.jpg');
    }

    public function getStreetviewUrlAttribute() : string
    {
        $url = 'https://www.google.com/maps/@';
        $query = [
            'api' => 1,
            'map_action' => 'pano',
        ];
        if ($panoId = data_get($this->metadata, 'pano_id')) {
            $query['pano'] = $panoId;
        }
        if ($this->areGoogleCoordsCloseEnough()) {
            $query['viewpoint'] = "$this->google_lat,$this->google_long";
        } else {
            $query['viewpoint'] = "$this->lat,$this->long";
        }

        return $url . '?' . http_build_query($query);
    }

    public function getShortStreetViewUrlAttribute() : string
    {
        if (!is_string($this->gshort_url)) {
            try {
                $this->gshort_url = app(MapsShortener::class)->shorten($this->streetview_url);
                $this->save();
            } catch (RequestException $e) {
                return $this->streetview_url;
            }
        }

        return $this->gshort_url;
    }

    public function scopeAlreadyTweeted(Builder $query) : Builder
    {
        return $query->whereNotNull('tweeted_at');
    }

    public function scopeNotTweetedYet(Builder $query) : Builder
    {
        return $query->whereNull('tweeted_at');
    }

    public function scopeGoogleVirgin(Builder $query) : Builder
    {
        return $query->where('maps_tried', 0);
    }
}
