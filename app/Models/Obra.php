<?php

namespace App\Models;

use App\DTO\Twitter\Tweet;
use DiDom\Document;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

/**
 * App\Models\Obra
 *
 * @property int $id
 * @property string $path
 * @property string $titulo
 * @property string|null $subtitulo
 * @property string|null $autores
 * @property string|null $fecha_publicacion
 * @property string $tipo_obra
 * @property string|null $estado_derechos_obra
 * @property string|null $ubicacion
 * @property string|null $materiales
 * @property string|null $alto
 * @property string|null $ancho
 * @property string|null $calidad
 * @property string|null $asset
 * @property int $scraped
 * @property string|null $user_id
 * @property string|null $username
 * @property string|null $tweet_id
 * @property string|null $tweeted_at
 * @property-read string $filename
 * @property-read string $readable_type
 * @method static Builder|Obra alreadyTweeted()
 * @method static Builder|Obra newModelQuery()
 * @method static Builder|Obra newQuery()
 * @method static Builder|Obra notTweetedYet()
 * @method static Builder|Obra query()
 * @method static Builder|Obra whereAlto($value)
 * @method static Builder|Obra whereAncho($value)
 * @method static Builder|Obra whereAsset($value)
 * @method static Builder|Obra whereAutores($value)
 * @method static Builder|Obra whereCalidad($value)
 * @method static Builder|Obra whereEstadoDerechosObra($value)
 * @method static Builder|Obra whereFechaPublicacion($value)
 * @method static Builder|Obra whereId($value)
 * @method static Builder|Obra whereMateriales($value)
 * @method static Builder|Obra wherePath($value)
 * @method static Builder|Obra whereScraped($value)
 * @method static Builder|Obra whereSubtitulo($value)
 * @method static Builder|Obra whereTipoObra($value)
 * @method static Builder|Obra whereTitulo($value)
 * @method static Builder|Obra whereTweetId($value)
 * @method static Builder|Obra whereTweetedAt($value)
 * @method static Builder|Obra whereUbicacion($value)
 * @method static Builder|Obra whereUserId($value)
 * @method static Builder|Obra whereUsername($value)
 * @mixin \Eloquent
 */
class Obra extends Model
{
    use HasFactory;

    protected $connection = 'autoresuy';
    protected $guarded = [];
    public $timestamps = false;

    // Tipos
    public const VISUAL = 'Obra visual';
    public const TEXTO = 'Obra de texto';
    // Licencias
    public const PUBLIC_DOMAIN = 'Dominio público';
    public const BY_NC_4 = 'Creative Commons BY-NC 4.0 Internacional';
    public const BY_NC_SA_4 = 'Creative Commons BY NC SA 4.0 Internacional';
    public const BY_NC_ND_4 = 'Creative Commons BY-NC-ND 4.0 Internacional';
    public const BY_ND_4 = 'Creative Commons BY-ND 4.0 Internacional';
    public const BY_SA = 'Creative Commons BY-SA 4.0 Internacional';
    public const NO_LICENSE = 'Sin licencia';

    public function getExtraFromSite() : void
    {
        if ($this->tipo_obra === self::VISUAL) {
            $this->scrapeVisualWork();
        } else {
            // NOT IMPLEMENTED YET
        }
        $this->save();
    }

    public function getTweet() : string
    {
        $tweet = "por {$this->autores}";
        if ($this->fecha_publicacion) {
            $tweet .= " ({$this->fecha_publicacion})";
        }

        $tweet .= PHP_EOL;
        if ($this->materiales || ($this->alto && $this->ancho)) {
            $tweet .= PHP_EOL . "{$this->readable_type}";
            if ($this->materiales) {
                $tweet .= " {$this->materiales}";
            }
            if ($this->alto && $this->ancho) {
                $tweet .= " {$this->alto} x {$this->ancho}";
            }
        }

        if ($this->ubicacion) {
            $tweet .= PHP_EOL . "🏠 {$this->ubicacion}";
        }

        $tweet .= PHP_EOL . PHP_EOL . "🔗 {$this->path}";

        $originalText = collect(mb_str_split($this->titulo))->reverse();
        $title = '';
        while ((Tweet::countChars($title) + Tweet::countChars($tweet)) <= 263 && $originalText->isNotEmpty()) {
            $title .= $originalText->pop();
        }
        if ($originalText->isNotEmpty()) {
            $lastSpacePos = mb_strrpos($title, ' ');
            if ($lastSpacePos !== false) {
                $title = mb_substr($title, 0, $lastSpacePos);
            }
            $title .= '…';
        }

        return "«{$title}» $tweet";
    }

    public function downloadAsset() : bool
    {
        if (!is_string($this->asset)) {
            return false;
        }

        $response = Http::withOptions([
            'sink' => $this->filename,
        ])->get($this->asset);

        return $response->successful();
    }

    public function getFilenameAttribute() : string
    {
        return storage_path('app/autoresuy/' . $this->id . '.jpg');
    }

    public function getReadableTypeAttribute() : string
    {
        if ($this->tipo_obra === self::VISUAL) {
            return '🖼️';
        } elseif ($this->tipo_obra === self::TEXTO) {
            return '📖';
        }
        return '👽';
    }

    private function scrapeVisualWork() : void
    {
        $document = new Document($this->path, true);

        $ubicacion = $document->first('.field--name-field-ubicacion .field__item');
        if ($ubicacion !== null) {
            /** @var \DiDom\Element $ubicacion */
            $this->ubicacion = $ubicacion->text();
        }

        $alto = $document->first('.field--name-field-tamano-alto .field__item');
        if ($alto !== null) {
            /** @var \DiDom\Element $alto */
            $this->alto = $alto->text();
        }
        $ancho = $document->first('.field--name-field-tamano-ancho .field__item');
        if ($ancho !== null) {
            /** @var \DiDom\Element $ancho */
            $this->ancho = $ancho->text();
        }
        $calidad = $document->first('.field--name-field-calidad-de-digitalizacion .field__item');
        if ($calidad !== null) {
            /** @var \DiDom\Element $calidad */
            $this->calidad = $calidad->text();
        }

        $materiales = $document->first('.field--name-field-materiales .field__item');
        if ($materiales !== null) {
            /** @var \DiDom\Element $materiales */
            $this->materiales = $materiales->text();
        }

        $image = $document->first('.field--name-field-portada img');
        if ($image !== null) {
            $this->asset = 'https://autores.uy' . $image->getAttribute('src');
        }
    }

    public function scopeAlreadyTweeted(Builder $query) : Builder
    {
        return $query->whereNotNull('tweeted_at');
    }

    public function scopeNotTweetedYet(Builder $query) : Builder
    {
        return $query->whereNull('tweeted_at');
    }
}
