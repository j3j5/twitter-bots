<?php

namespace App\Processors;

use App\Jobs\Defancify\ProcessTweet;
use Illuminate\Support\Facades\Log;
use OauthPhirehose;

class DefancifyStream extends OauthPhirehose
{
    /**
     * Enqueue each status
     *
     * @param string $status
     */
    public function enqueueStatus($status) : void
    {
        Log::debug('Tweet received: ' . $status);
        ProcessTweet::dispatch($status);
    }
}
