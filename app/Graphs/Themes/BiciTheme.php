<?php

namespace App\Graphs\Themes;

use Amenadiel\JpGraph\Plot\AccBarPlot;
use Amenadiel\JpGraph\Plot\AccLinePlot;
use Amenadiel\JpGraph\Plot\BarPlot;
use Amenadiel\JpGraph\Plot\GroupBarPlot;
use Amenadiel\JpGraph\Plot\LinePlot;
use OzdemirBurak\Iris\Color\Hex;

/**
 * JPGraph v4.0.3 compaible
 * @author j3j5
 */
class BiciTheme extends BaseTheme
{
    protected const LEGEND_FONTSIZE = 18;

    protected int $barPlots = 0;
    protected int $linePlots = 0;

    public int $leftMargin = 120;
    public int $rightMargin = 80;
    public int $topMargin = 160;
    public int $bottomMargin = 80;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     * @return array<int, string>
     */
    public function GetColorList() : array
    {
        return [
            '#4090db',
            '#46cc71',  // IM
            '#e6d800',
            '#e60049',
            '#ffa300',
            '#9b19f5',
            '#dc0ab4',
            '#b3d4ff',

        ];
    }

    /**
     *
     * @param \Amenadiel\JpGraph\Graph\Graph $graph
     * @throws \Amenadiel\JpGraph\Util\JpGraphExceptionL
     * @return void
     */
    public function SetupGraph($graph)
    {
        parent::SetupGraph($graph);
        $graph->subtitle->SetMargin(6);
        $graph->subsubtitle->SetMargin(8);

        // legend
        $posX = 0.5;
        $posY = 0.96;
        if ($graph->img->original_width / $graph->img->original_height > 2) {
            $posY = 0.95;
        }

        $graph->legend->Pos($posX, $posY, 'center', 'bottom');
        $graph->legend->SetFont(FF_USERFONT3, FS_NORMAL, self::LEGEND_FONTSIZE);
    }

    /**
     *
     * @param \Amenadiel\JpGraph\Graph\Graph $graph
     * @throws \Amenadiel\JpGraph\Util\JpGraphExceptionL
     * @return void
     */
    public function PreStrokeApply($graph)
    {
        if ($graph->legend->HasItems()) {
            // Make some room for the legend at the bottom
            $graph->SetMargin(
                $graph->img->raw_left_margin,
                $graph->img->raw_right_margin,
                $graph->img->raw_top_margin,
                $graph->img->raw_bottom_margin + 40
            );
        }

        if (reset($graph->plots) instanceof LinePlot || reset($graph->plots) instanceof AccLinePlot) {
            $graph->xaxis->SetWeight(2);
            $graph->xgrid->SetColor($this->gridColor);
            $graph->xgrid->SetFill(true, 'white@0.99', 'white@0.99');
            $graph->xgrid->Show(true, true);

            $graph->xaxis->SetLabelMargin(20);
            $graph->xaxis->SetTitleMargin(30);
        }

        // Center the mark on lines with the grouped bars
        foreach($graph->plots as $i => $plot) {
            if ($plot instanceof LinePlot && $plot->mark->type === MARK_FILLEDCIRCLE) {
                $offset = $i % 2 === 0 ? 0.633 : 0.233;
                for ($i = 0; $i < count($plot->coords[0]); $i++) {
                    $plot->coords[1][$i] = $i + $offset;
                }
            }
        }
    }

    /**
     *
     * @param \Amenadiel\JpGraph\Plot\Plot $plot
     * @throws \Amenadiel\JpGraph\Util\JpGraphExceptionL
     * @return void
     */
    public function ApplyPlot($plot)
    {
        switch (get_class($plot)) {
            case AccBarPlot::class:
                /** @var \Amenadiel\JpGraph\Plot\AccBarPlot $plot */
                /** @var \Amenadiel\JpGraph\Plot\BarPlot $barplot */
                foreach ($plot->plots as $barplot) {
                    $this->ApplyPlot($barplot);
                }
                break;
            case GroupBarPlot::class:
                /** @var \Amenadiel\JpGraph\Plot\GroupBarPlot $plot */
                foreach ($plot->plots as $i => $plot) {
                    /** @var \Amenadiel\JpGraph\Plot\BarPlot $plot */
                    $this->ApplyPlot($plot);
                }
                break;
            case BarPlot::class:
                /** @var \Amenadiel\JpGraph\Plot\BarPlot $plot */
                $plot->Clear();
                $plot->SetColor('#FFFFFF@0.99');
                $color = $this->GetNextColor();
                $hexColor = new Hex($color);
                $plot->SetFillGradient(
                    (string) $hexColor->saturate(40),
                    (string) $hexColor->desaturate(20),
                    GRAD_VER,
                );
                $plot->value->SetColor($this->fontColor);
                // $plot->SetWidth(2);
                // $plot->value->show();
                $plot->SetWidth(0.7);
                $this->barPlots++;
                break;
            case LinePlot::class:
                /** @var \Amenadiel\JpGraph\Plot\LinePlot $plot */
                if ($plot->mark->type !== -1) {
                    // Some style was already applied outside the theme
                    return;
                }

                $color = $this->GetColorList()[$this->linePlots];
                $hexColor = new Hex($color);
                $plot->SetColor((string) $hexColor->darken(10));
                $plot->SetWeight(4);
                // leave commented, centered in preAdjust
                // $plot->SetBarCenter();

                $plot->mark->SetType(MARK_FILLEDCIRCLE);
                $plot->mark->SetFillColor((string) $hexColor->lighten(15));

                $markWidth = 4;
                $plot->mark->SetWidth($markWidth);

                $this->linePlots++;
                break;
            default:
                break;
        }
    }
}
