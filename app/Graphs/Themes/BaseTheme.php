<?php

namespace App\Graphs\Themes;

use Amenadiel\JpGraph\Graph\Graph;
use Amenadiel\JpGraph\Graph\PieGraph;
use Amenadiel\JpGraph\Themes\Theme;

abstract class BaseTheme extends Theme
{
    public string $fontColor = '#16161d';      // Eigengrau
    public string $fontColorLight = '#e9e9e2';      // Alabaster
    public string $bgColor = '#fffdfa';        // floral white
    public string $axisColor = '#16161d';      // Eigengrau
    public string $gridColor = '#e9e9e2@0.4';  // Alabaster with transparency

    public int $leftMargin = 120;
    public int $rightMargin = 80;
    public int $topMargin = 115;
    public int $bottomMargin = 80;

    protected const AXIS_FONTSIZE = 16;
    protected const AXIS_TITLE_FONTSIZE = 18;
    protected const H1_FONTSIZE = 24;
    protected const H2_FONTSIZE = 20;
    protected const H3_FONTSIZE = 16;
    protected const LEGEND_FONTSIZE = 16;

    /** @return array<int, string> */
    abstract public function GetColorList();

    /**
     *
     * @param \Amenadiel\JpGraph\Plot\Plot $plot
     * @throws \Amenadiel\JpGraph\Util\JpGraphExceptionL
     * @return void
     */
    abstract public function ApplyPlot($plot);

    /**
     *
     * @param \Amenadiel\JpGraph\Graph\Graph $graph
     * @throws \Amenadiel\JpGraph\Util\JpGraphExceptionL
     * @return void
     */
    public function SetupGraph($graph)
    {
        $graph->SetFrame(true, $this->bgColor);
        $graph->SetBox(true, $this->bgColor);
        $graph->SetMarginColor($this->bgColor);

        $graph->SetMargin(
            $this->leftMargin,
            $this->rightMargin,
            $this->topMargin,
            $this->bottomMargin
        );

        // Fonts
        $this->setUserFonts($graph);
        $this->setTitleStyles($graph, FF_USERFONT1);

        // legend
        $graph->legend->SetFrameWeight(0);
        $graph->legend->Pos(0.5, 0.95, 'center', 'bottom');
        $graph->legend->SetFillColor($this->bgColor);
        $graph->legend->SetColor($this->fontColor);
        $graph->legend->SetFont(FF_USERFONT1, FS_NORMAL, self::LEGEND_FONTSIZE);
        $graph->legend->SetLayout(LEGEND_HOR);
        $graph->legend->SetShadow(false);
        $graph->legend->SetMarkAbsSize(10);
        $graph->legend->SetLineWeight(10);

        $this->setupDefaultAxisStyles($graph);

        $graph->img->SetAntiAliasing();
    }

    protected function setupDefaultAxisStyles(Graph $graph) : void
    {
        // xaxis
        $graph->xaxis->title->SetColor($this->fontColor);
        $graph->xaxis->title->SetFont(FF_USERFONT3, FS_NORMAL, self::AXIS_TITLE_FONTSIZE);
        $graph->xaxis->SetColor($this->axisColor, $this->fontColor);
        $graph->xaxis->SetFont(FF_USERFONT3, FS_NORMAL, self::AXIS_FONTSIZE);
        $graph->xaxis->SetTickSide(SIDE_BOTTOM);
        $graph->xaxis->SetLabelMargin(10);
        $graph->xaxis->SetTitleMargin(20);

        // yaxis
        $graph->yaxis->title->SetColor($this->fontColor);
        $graph->yaxis->title->SetFont(FF_USERFONT3, FS_NORMAL, self::AXIS_TITLE_FONTSIZE);
        $graph->yaxis->SetTitleMargin(70);
        $graph->yaxis->SetColor($this->axisColor, $this->fontColor);
        $graph->yaxis->SetFont(FF_USERFONT3, FS_NORMAL, self::AXIS_FONTSIZE);
        $graph->yaxis->SetTickSide(SIDE_LEFT);
        $graph->yaxis->SetLabelMargin(10);
        $graph->yaxis->SetWeight(2);

        // grid
        $graph->ygrid->SetColor($this->gridColor);
        $graph->ygrid->SetFill(true, $this->bgColor . '@0.99', $this->bgColor . '@0.99');
        $graph->ygrid->Show(true, true);
    }

    protected function setTitleStyles(Graph $graph, int $font = FF_USERFONT3) : void
    {
        // title
        $graph->title->SetColor($this->fontColor);
        $graph->title->SetFont($font, FS_BOLD, self::H1_FONTSIZE);
        $graph->title->SetMargin(40);

        $graph->subtitle->SetColor($this->fontColor);
        $graph->subtitle->SetFont($font, FS_NORMAL, self::H2_FONTSIZE);

        $graph->subsubtitle->SetColor($this->fontColor);
        $graph->subsubtitle->SetFont($font, FS_NORMAL, self::H3_FONTSIZE);

        if ($graph instanceof PieGraph) {
            $graph->SetAntiAliasing();
        }
    }

    protected function setUserFonts(Graph $graph) : void
    {
        $graph->SetUserFont1(
            config('graphs.font1-regular'),
            config('graphs.font1-bold'),
            config('graphs.font1-italic'),
        );

        $graph->SetUserFont2(
            config('graphs.font2-regular'),
            config('graphs.font2-bold'),
            config('graphs.font2-italic'),
        );

        $graph->SetUserFont3(
            config('graphs.font3-regular'),
            config('graphs.font3-bold'),
            config('graphs.font3-italic'),
        );
    }
}
