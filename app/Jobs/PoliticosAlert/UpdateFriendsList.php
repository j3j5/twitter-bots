<?php

namespace App\Jobs\PoliticosAlert;

use App\Models\PoliticosAlert\Event;
use App\Models\PoliticosAlert\FriendsList;
use App\Models\PoliticosAlert\TwitterUser;
use Atymic\Twitter\Exception\ClientException;
use Atymic\Twitter\Service\Querier;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Psr\Http\Message\ResponseInterface;

use function Safe\json_decode;
use function Safe\json_encode;

class UpdateFriendsList implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected TwitterUser $user;
    protected string $basePath;
    protected Querier $api;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(TwitterUser $user)
    {
        $this->user = $user;
        $this->delay = mt_rand(1, 300);
        $this->onQueue('bigbo');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Querier $api) : void
    {
        $this->api = $api;
        Log::debug('token: ' . $api->getConfiguration()->getAccessToken());
        $newFriends = [];

        $paginationToken = null;

        $this->basePath = TwitterUser::FOLDER . $this->user->twitter_id;
        if (Storage::exists($this->basePath . '/paginationToken') && Storage::exists($this->basePath . '/friends')) {
            $paginationToken = Storage::get($this->basePath . '/paginationToken');
            $friendsContents = (string) Storage::get($this->basePath . '/friends');
            $newFriends = Arr::wrap(json_decode($friendsContents, true));
        }

        do {
            $params = [
                'max_results' => '1000',
                'format' => 'array',
            ];
            if ($paginationToken) {
                $params['pagination_token'] = $paginationToken;
                Storage::put($this->basePath . '/paginationToken', $paginationToken);
            }
            try {
                $result = $this->api->get(
                    "users/{$this->user->twitter_id}/following",
                    $params
                );
            } catch (ClientException $e) {
                if ($e->getCode() !== 429) {
                    throw $e;
                }
                $response = $this->api->getSyncClient()->getLastResponse();
                if (!$response instanceof ResponseInterface) {
                    Log::warning('No response on client while rate-limited');
                    throw $e;
                }
                $resetHeader = $response->getHeader('x-rate-limit-reset');
                $reset = Arr::first($resetHeader, null, 300);
                $resetRateLimit = Carbon::createFromTimestamp($reset);
                $seconds = $resetRateLimit->diffInSeconds(now());
                Log::debug("Releasing job with a delay of $seconds s.  " . now()->setTimezone('America/Montevideo')->addSeconds($seconds)->toTimeString());
                $this->release($seconds);
                return;
            }
            if (!isset($result['data'])) {
                Log::warning('NO data on response', Arr::wrap($result));
                return;
            }
            $newFriends = array_merge($newFriends, $result['data']);
            Storage::put($this->basePath . '/friends', json_encode($newFriends));

            $paginationToken = data_get($result, 'meta.next_token');
        } while ($paginationToken !== null);

        if ($this->user->friends) {
            $originalFriends = $this->user->friends->list;

            $originalIds = Arr::pluck($originalFriends, 'id');
            $newIds = Arr::pluck($newFriends, 'id');
            $newFollows = array_diff($newIds, $originalIds);
            $newUnfollows = array_diff($originalIds, $newIds);

            $this->processFollows($newFollows, $newFriends);
            $this->processUnfollows($newUnfollows, $originalFriends);
        }

        FriendsList::updateOrCreate([
            'user_id' => $this->user->id,
        ], [
            'list' => $newFriends,
        ]);

        Storage::deleteDirectory($this->basePath);
    }

    private function processFollows(array $newFollows, array $newFriends) : void
    {
        foreach ($newFollows as $id) {
            $follow = Arr::first($newFriends, fn ($item) => $item['id'] === $id);
            RecordEvent::dispatch($this->user, Event::FOLLOW, '', json_encode($follow));
        }
    }

    private function processUnfollows(array $newUnfollows, array $originalFriends) : void
    {
        $unfollows = $newUnfollows;
        // TODO: implement pagination so we can retrieve more than 100 at a time
        if (count($newUnfollows) > 100) {
            $unfollows = array_slice($newUnfollows, 0, 100);
        }

        $params = [
            'ids' => implode(',', $unfollows),
            'format' => 'array',
        ];
        try {
            $result = $this->api->get(
                'users',
                $params
            );
        } catch (ClientException $e) {
            $result = null;
        }
        /** @var array<string, mixed> $errorsArray */
        $errorsArray = data_get($result, 'errors', []);
        $errors = collect($errorsArray);
        foreach ($newUnfollows as $id) {
            $unfollow = Arr::first($originalFriends, fn ($item) => $item['id'] === $id);
            if ($error = $errors->firstWhere('value', $id)) {
                $unfollow['error_title'] = Arr::get($error, 'title');
                $unfollow['error_detail'] = Arr::get($error, 'detail');
            }
            RecordEvent::dispatch($this->user, Event::UNFOLLOW, '', json_encode($unfollow));
        }
    }
}
