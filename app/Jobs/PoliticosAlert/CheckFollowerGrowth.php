<?php

namespace App\Jobs\PoliticosAlert;

use App\Models\PoliticosAlert\Event;
use App\Models\PoliticosAlert\TwitterUser;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class CheckFollowerGrowth
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(private TwitterUser $user)
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() : void
    {
        // Check hourly followers
        $this->checkHourlyData();

        // Check daily followers
        $this->checkDailyData();
    }

    private function checkHourlyData() : void
    {
        $hourlyFollowData = $this->user->getHourlyFollowersData();
        $dataTime = Carbon::parse($hourlyFollowData[0]);
        if ($dataTime->diffInHours() < 1) {
            // Too soon
            return;
        }

        $followersLastHour = $hourlyFollowData[1];
        if ($followersLastHour === 0) {
            // Newly created account, don't break 🥹
            return;
        }

        $difference = $this->user->followers_count - $followersLastHour;
        $percentage = $difference / $followersLastHour * 100;

        Log::debug("$percentage% of difference for @{$this->user->username} in the last hour");

        // On the last hour, it increased by more than HOURLY_THRESHOLD_PERC% with
        // a minimum of half HOURLY_THRESHOLD_COUNT or it increased in absolute
        // numbers by more than HOURLY_THRESHOLD_COUNT
        if (
            (abs($percentage) >= TwitterUser::HOURLY_THRESHOLD_PERC && $difference > TwitterUser::HOURLY_THRESHOLD_COUNT / 2) ||
            $difference >= TwitterUser::HOURLY_THRESHOLD_COUNT
        ) {
            RecordEvent::dispatch($this->user, Event::GROWTH, Event::HOURLY, (string) $followersLastHour);
        }
        // Update hourly record
        $this->user->updateHourlyFollowersData();
    }

    private function checkDailyData() : void
    {
        $dailyFollowData = $this->user->getDailyFollowersData();
        $dataTime = Carbon::parse($dailyFollowData[0]);

        if ($dataTime->diffInDays() < 1) {
            // Too soon
            return;
        }

        $followersLastDay = $dailyFollowData[1];
        $difference = $this->user->followers_count - $followersLastDay;
        $percentage = $difference / $followersLastDay * 100;
        Log::debug("$percentage% of difference for @{$this->user->username} in the last day");

        // On the last day, it increased by more than DAILY_THRESHOLD_PERC% with
        // a minimum of half DAILY_THRESHOLD_COUNT or it increased in absolute
        // numbers by more than DAILY_THRESHOLD_COUNT
        if (
            (abs($percentage) >= TwitterUser::DAILY_THRESHOLD_PERC && $difference > TwitterUser::DAILY_THRESHOLD_COUNT / 2) ||
            $difference >= TwitterUser::DAILY_THRESHOLD_COUNT) {
            RecordEvent::dispatch($this->user, Event::GROWTH, Event::DAILY, (string) $followersLastDay);
        }
        // Update daily record
        $this->user->updateDailyFollowersData();
    }
}
