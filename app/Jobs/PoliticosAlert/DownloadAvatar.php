<?php

namespace App\Jobs\PoliticosAlert;

use App\Models\PoliticosAlert\TwitterUser;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class DownloadAvatar implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private TwitterUser $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(TwitterUser $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() : void
    {
        $filepath = $this->user->avatar_filepath;

        if (Storage::exists($filepath)) {
            Log::debug("File $filepath already exists");
            return;
        }

        // Download pic
        $response = Http::retry(3, mt_rand(1, 5000))->get($this->user->avatar)->throw();

        Storage::put($filepath, $response->body());
    }
}
