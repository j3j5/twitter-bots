<?php

namespace App\Jobs\RandomBakeries;

use GrahamCampbell\GitHub\Facades\GitHub;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use function Safe\fclose;
use function Safe\file_get_contents;
use function Safe\file_put_contents;
use function Safe\fopen;
use function Safe\fputcsv;
use function Safe\json_decode;
use function Safe\json_encode;

class StoreData
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected string $csvPath;
    protected string $geojsonPath;

    public function __construct(protected array $bakery)
    {
        $this->csvPath = storage_path('app/randomBakeries/bakeries.csv');
        $this->geojsonPath = storage_path('app/randomBakeries/bakeries.geojson');
    }

    public function handle() : void
    {
        $this->appendToCSV();
        $this->updateGeoJson();
        $this->updateGist();
    }

    private function appendToCSV() : void
    {
        $data = [
            'name' => Arr::get($this->bakery, 'result.name', ''),
            'address' => Arr::get($this->bakery, 'result.formatted_address', ''),
            'web' => Arr::get($this->bakery, 'result.website', ''),
            'lat' => Arr::get($this->bakery, 'result.geometry.location.lat', ''),
            'long' => Arr::get($this->bakery, 'result.geometry.location.lng', ''),
            'rating' => Arr::get($this->bakery, 'result.rating', ''),
            'total_ratings' => Arr::get($this->bakery, 'result.user_ratings_total', ''),
        ];

        $h = null;
        if (!file_exists($this->csvPath)) {
            $h = fopen($this->csvPath, 'a');
            fputcsv($h, array_keys($data));
        }
        $h = $h ?? fopen($this->csvPath, 'a');
        fputcsv($h, $data);
        fclose($h);
    }

    private function updateGeoJson() : void
    {
        $data = [
            'name' => Arr::get($this->bakery, 'result.name', ''),
            'address' => Arr::get($this->bakery, 'result.formatted_address', ''),
            'web' => Arr::get($this->bakery, 'result.website', ''),
            'lat' => Arr::get($this->bakery, 'result.geometry.location.lat', ''),
            'long' => Arr::get($this->bakery, 'result.geometry.location.lng', ''),
            'rating' => Arr::get($this->bakery, 'result.rating', ''),
            'total_ratings' => Arr::get($this->bakery, 'result.user_ratings_total', ''),
        ];

        if (file_exists($this->geojsonPath)) {
            $geojson = json_decode(file_get_contents($this->geojsonPath), true);
        } else {
            $geojson = [
                'type' => 'FeatureCollection',
                'crs' => [
                    'type' => 'name',
                    'properties' => [
                        'name' => 'urn:ogc:def:crs:OGC:1.3:CRS84',
                    ],
                ],
                'features' => [],
            ];
        }

        $geo = [
            'type' => 'Point',
            'coordinates' => [
                $data['long'],
                $data['lat'],
            ],
        ];
        unset($data['lat']);
        unset($data['long']);
        $feature = [
            'type' => 'Feature',
            'properties' => $data,
            'geometry' => $geo,
        ];
        $geojson['features'][] = $feature;
        file_put_contents($this->geojsonPath, json_encode($geojson));
    }

    private function updateGist() : void
    {
        $upload = [
            'files' => [
                'panaderias.csv' => ['content' => file_get_contents($this->csvPath)],
                'panaderias.geojson' => ['content' => file_get_contents($this->geojsonPath)],
            ],
        ];

        /** @phpstan-ignore-next-line */
        GitHub::gists()->update(config('github.bakeries-gist'), $upload);
    }
}
