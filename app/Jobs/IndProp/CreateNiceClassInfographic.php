<?php

namespace App\Jobs\IndProp;

use App\Models\IndProp\Brand;
use App\Services\MIEM\Helper;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Intervention\Image\Facades\Image;
use Intervention\Image\Image as Img;

class CreateNiceClassInfographic
{
    use Dispatchable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected Brand $brand)
    {
        //
    }

    /**
     * Generate and image including all the Nice classes for the given brand.
     *
     * @return \Intervention\Image\Image
     */
    public function handle(Helper $nice) : Img
    {
        $width = 1600;
        $rowHeight = 220;
        $initialOffset = 170;
        $bgColor = '#fffdfa';
        $height = count($this->brand->niceClasses) * $rowHeight + $initialOffset;
        $img = Image::canvas($width, $height, $bgColor);

        // Heading
        $x = (int) $width / 2;
        $y = 40;
        $img->text('Clasificación Niza', $x, $y, function ($font) {
            $font->file(config('graphs.font1-bold'));
            $font->size(36);
            $font->color('#16161d');
            $font->align('center');
            $font->valign('middle');
        });

        // Brand Name
        if ($this->brand->brand) {
            $brandName = wordwrap($this->brand->brand, 30, PHP_EOL);
            $lines = explode(PHP_EOL, $brandName);
            foreach ($lines as $lineNr => $text) {
                $y += 40;
                $img->text($text, $x, $y, function ($font) {
                    $font->file(config('graphs.font1-bold'));
                    $font->size(30);
                    $font->color('#16161d');
                    $font->align('center');
                    $font->valign('middle');
                });
                $initialOffset += 35;
                $img->resizeCanvas(0, 35, 'top', true, $bgColor);
            }
        }

        // Add brand logo
        if ($this->brand->logo) {
            $xLogo = 20;
            $yLogo = 30;

            $logo = Image::make($this->brand->logo);
            $logo->trim('top-left', ['top', 'bottom']);
            $maxLogoHeight = 100;
            $maxLogoWidth = (int) round($width - round($width * 2 / 3)) - $xLogo;
            $logo->resize(null, $maxLogoHeight, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            if ($logo->width() > $maxLogoWidth) {
                $logo->resize($maxLogoWidth, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            $img->insert($logo, 'top-right', $xLogo, $yLogo);

            $starOffset = 3;
            $img->text('*', $img->width() - $xLogo + $starOffset, $yLogo + $starOffset, function ($font) {
                $font->file(config('graphs.font1-regular'));
                $font->size(28);
                $font->color('#16161d');
                $font->align('left');
                $font->valign('top');
            });

            // Add disclaimers
            $logoDisc = '*Logo perteneciente a su respectivo dueño';
            $x = 20;
            $y = $img->height() - 15;
            $img->text($logoDisc, $x, $y, function ($font) {
                $font->file(config('graphs.font1-regular'));
                $font->size(14);
                $font->color('#16161d');
                $font->align('left');
                $font->valign('bottom');
            });
        }

        $i = 0;
        foreach ($this->brand->niceClasses as $classNumber => $desc) {
            $diameter = (int) round($width / 15);
            $x = 120;
            $rowOffset = 105;
            $y = ($diameter + $rowOffset) * $i + $initialOffset;
            $img->circle($diameter, $x, $y, function ($draw) {
                $draw->border(4, '16161d');
            });
            $info = $nice->getNiceClassInfo($classNumber);

            // Add number
            $xClass = $x;
            $yClass = $y - (int) round($diameter / 2) - 6;
            $img->text("Clase $classNumber", $xClass, $yClass, function ($font) {
                $font->file(config('graphs.font1-bold'));
                $font->size(26);
                $font->color('#16161d');
                $font->align('center');
                $font->valign('bottom');
            });

            // Add title, use the explode to help cutomize the line-spacing on the text
            $lines = explode(PHP_EOL, $info['title']);
            foreach ($lines as $lineNr => $title) {
                $size = 24;
                $rowOffset = (int) round($diameter / 2) + $lineNr * $size + 2;
                $img->text($title, $x, $y + $rowOffset, function ($font) use ($size) {
                    $font->file(config('graphs.font1-regular'));
                    $font->size($size);
                    $font->color('#16161d');
                    $font->align('center');
                    $font->valign('top');
                });
            }

            // Add icon
            $iconPath = base_path('node_modules/@fortawesome/fontawesome-free/svgs/solid/' . $info['icon'] . '.svg');
            $icon = Image::make($iconPath);
            $iconWidth = (int) round($diameter / 2);
            $icon->resize($iconWidth, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            if ($icon->height() > $iconWidth) {
                $icon->resize(null, $iconWidth, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }

            $xIcon = $x - (int) round($icon->width() / 2) + 1;
            $yIcon = $y - (int) round($icon->height() / 2) + 1;
            $img->insert($icon, 'top-left', $xIcon, $yIcon);

            // Add description, try to use custom
            if (empty($desc)) {
                $desc = $info['desc'];
            }
            $wrappedDesc = wordwrap(ucfirst($desc), 122, PHP_EOL);
            $lines = explode(PHP_EOL, $wrappedDesc);
            $maxLines = 7;
            if (count($lines) > $maxLines) {
                $lines = array_slice($lines, 0, $maxLines);
                $lines[$maxLines - 1] = $lines[$maxLines - 1] . '…';
            }

            foreach ($lines as $lineNr => $line) {
                $xOffset = $diameter;
                $rowOffset = $lineNr * 24 - (count($lines) - 1) * 10;
                $img->text($line, $x + $xOffset, $y + $rowOffset, function ($font) {
                    $font->file(config('graphs.font1-regular'));
                    $font->size(22);
                    $font->color('#16161d');
                    $font->align('left');
                    $font->valign('middle');
                });
            }
            $i++;
        }

        // Add license
        $license = Image::make(resource_path('images/cc-by-sa.png'));
        $licenseX = 20;
        $licenseY = 40;
        $license->resize(null, 40, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->insert($license, 'bottom-right', $licenseX, $licenseY);

        // Add disclaimer
        $iconsDisc = 'Iconos creados por Font Awesome';
        $x = $img->width() - 20;
        $y = $img->height() - 15;
        $img->text($iconsDisc, $x, $y, function ($font) {
            $font->file(config('graphs.font1-regular'));
            $font->size(14);
            $font->color('#16161d');
            $font->align('right');
            $font->valign('bottom');
        });

        return $img->encode('png');
    }
}
