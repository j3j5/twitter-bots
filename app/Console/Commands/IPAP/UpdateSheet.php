<?php

namespace App\Console\Commands\IPAP;

use App\Services\Google\GoogleSheets;
use Exception;
use Google\Service\Sheets;
use Google\Service\Sheets\AppendDimensionRequest;
use Google\Service\Sheets\BatchUpdateSpreadsheetRequest;
use Google\Service\Sheets\Request;
use Google\Service\Sheets\ValueRange;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RuntimeException;

use function Safe\preg_match;
use function Safe\preg_replace;

class UpdateSheet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ipap:update-sheet';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update sources';

    private Sheets $service;
    private string $spreadsheetId;
    // private string $ipapId;
    // private string $chartsId;
    // private string $preciosUYId;
    private string $preciosMediosId;
    private string $indicePreciosId;
    // private string $papasChipId;
    private string $ipapFilesBasePath;

    private const AVG_PRICES = 'Precios Medios';
    private const INDEXES = 'Índices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->ipapFilesBasePath = storage_path('app/ipap/');
        $this->spreadsheetId = '1f8_95ZXiB_Cz1ZVznkpd34O3O0zO8Huvs5O7bhUXZ2U';
        // $this->ipapId = '1277472891';
        // $this->chartsId = '143654460';
        // $this->preciosUYId = '1582206519';
        $this->preciosMediosId = '34493036';
        $this->indicePreciosId = '1446640599';
        // $this->papasChipId = '1755359182';
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Sheets $service)
    {
        /** @phpstan-ignore-next-line */
        app()->log->pushProcessor(static function (array $record) {
            $record['message'] = "[IPAP][UPDATE-SHEETS] {$record['message']}";
            return $record;
        });

        $this->service = $service;

        $this->uploadInfoFromPreciousUy();
        $this->uploadInfoFromIne();

        return 0;
    }

    private function uploadInfoFromPreciousUy() : void
    {
        // The A1 notation of a range to search for a logical table of data.
        // Values will be appended after the last row of the table.
        $range = 'PreciosUY!B:B';

        // append empty row as a hack to find the value of the latest row with data
        $requestBody = new ValueRange(['values' => [['']]]);
        $requestBody->setRange($range);
        $params = ['valueInputOption' => 'USER_ENTERED'];

        $response = $this->service->spreadsheets_values->append($this->spreadsheetId, $range, $requestBody, $params);
        /** @var string $lastRow */
        $lastRow = Arr::last(explode(':', $response->getTableRange()));
        $lastDateRange = 'PreciosUY!' . preg_replace('/\D{1,}/', 'B', $lastRow);

        // Find out what's the latest date on the sheet
        $params = ['valueRenderOption' => 'UNFORMATTED_VALUE'];
        $response = $this->service->spreadsheets_values->get($this->spreadsheetId, $lastDateRange, $params);
        $values = $response->getValues();
        $lastDate = GoogleSheets::getDateFromSerialNumber(Arr::get($values, '0.0'));

        // Update the sheet with all the missing data from the local file
        $this->updateSheetWithPreciousUy($lastDate, $lastDateRange);
    }

    private function uploadInfoFromIne() : void
    {
        // PRECIOS MEDIOS
        $sheet = "'IPC - Precios medios'";
        // Calculate the highest column currently to get the InsertRange (next column)
        $datesRange = "$sheet!2:2";
        $params = ['valueRenderOption' => 'UNFORMATTED_VALUE'];
        $response = $this->service->spreadsheets_values->get($this->spreadsheetId, $datesRange, $params);
        $lastDate = GoogleSheets::getDateFromSerialNumber(Arr::last(Arr::first($response->getValues())));
        $lastColumn = mb_substr(Arr::last(explode(':', $response->getRange())), 0, -1);

        $lastColumnIdx = Coordinate::columnIndexFromString($lastColumn);
        $insertRange = "$sheet!" . Coordinate::stringFromColumnIndex($lastColumnIdx + 1) . '1';

        $this->updateSheetFromIne(self::AVG_PRICES, $lastDate, $insertRange);

        // ÍNDICES
        $sheet = "'IPC - Índice'";
        // Calculate the highest column currently to get the InsertRange (next column)
        $datesRange = "$sheet!2:2";
        $params = ['valueRenderOption' => 'UNFORMATTED_VALUE'];
        $response = $this->service->spreadsheets_values->get($this->spreadsheetId, $datesRange, $params);
        $lastDate = GoogleSheets::getDateFromSerialNumber(Arr::last(Arr::first($response->getValues())));

        $lastColumn = mb_substr(Arr::last(explode(':', $response->getRange())), 0, -1);
        $lastColumnIdx = Coordinate::columnIndexFromString($lastColumn);
        $insertRange = "$sheet!" . Coordinate::stringFromColumnIndex($lastColumnIdx + 1) . '1';
        $this->updateSheetFromIne(self::INDEXES, $lastDate, $insertRange);
    }

    public function updateSheetFromIne(string $sheetType, Carbon $lastDate, string $insertRange) : void
    {
        $datesRow = 8;
        $generalIndexRow = 13;
        $firstProductRow = 15;
        if ($sheetType === self::AVG_PRICES) {
            $sheet = "'IPC - Precios medios'";
            $sheetId = $this->preciosMediosId;
        } elseif ($sheetType === self::INDEXES) {
            $sheet = "'IPC - Índice'";
            $sheetId = $this->indicePreciosId;
        } else {
            throw new RuntimeException('Unknown info type');
        }
        $spreadsheet = $this->readIneSpreadsheet();
        $highestCol = $spreadsheet->getActiveSheet()->getHighestColumn();
        $datesData = $spreadsheet->getActiveSheet()
            ->rangeToArray(
                "A{$datesRow}:{$highestCol}{$datesRow}",     // The worksheet range that we want to retrieve
                null,        // Value that should be returned for empty cells
                false,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
                false,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
                true         // Should the array be indexed by cell row and cell column
            );
        /** @var array<string, string> $row */
        $row = $datesData[$datesRow];
        /** @var \Illuminate\Support\Collection<string, string> $availableMonths */
        $availableMonths = collect($row)->filter()->flip();
        $dateToUpload = $lastDate->copy()->addMonth();
        $highestRow = $spreadsheet->getActiveSheet()->getHighestRow();
        $dateString = ucfirst($dateToUpload->locale('es_UY')->isoFormat('MMMM YYYY'));
        while ($availableMonths->has($dateString)) {
            $columnToRead = $availableMonths->get($dateString);

            if ($columnToRead === null) {
                throw new Exception('No column found for the given month: ' . $dateString);
            }

            if ($sheetType === self::INDEXES) {
                $columnIdx = Coordinate::columnIndexFromString($columnToRead);
                $columnToRead = Coordinate::stringFromColumnIndex($columnIdx + 1);
            }
            $this->info('Processing ' . $sheetType . ' for ' . $dateToUpload->locale('es_UY')->isoFormat('MMMM YYYY'));
            $codes = $spreadsheet->getActiveSheet()->rangeToArray(
                "A{$firstProductRow}:A{$highestRow}",     // The worksheet range that we want to retrieve
                null,        // Value that should be returned for empty cells
                false,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
                false,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
                true         // Should the array be indexed by cell row and cell column
            );
            $codes = collect($codes)->mapWithKeys(function ($cell, $key) {
                return [$key => $cell['A']];
            });

            $values = $spreadsheet->getActiveSheet()->rangeToArray(
                "{$columnToRead}{$firstProductRow}:{$columnToRead}{$highestRow}",     // The worksheet range that we want to retrieve
                null,        // Value that should be returned for empty cells
                false,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
                false,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
                true         // Should the array be indexed by cell row and cell column
            );
            $values = collect($values)->mapWithKeys(function ($cell, $key) {
                return [$key => Arr::first($cell)];
            });
            $data = $codes->combine($values)->filter(function ($price, $code) {
                return is_numeric($code);
            });
            // Now, upload it to GSheets

            // Add new empty column
            $appendDimRequest = new AppendDimensionRequest();
            $appendDimRequest->setSheetId($sheetId);
            $appendDimRequest->setDimension('COLUMNS');
            $appendDimRequest->setLength(1);
            $request = new Request();
            $request->setAppendDimension($appendDimRequest);
            $batchUpdate = new BatchUpdateSpreadsheetRequest();
            $batchUpdate->setRequests($request);
            $response = $this->service->spreadsheets->batchUpdate($this->spreadsheetId, $batchUpdate);
            $datesRange = "$sheet!A:A";
            $params = ['valueRenderOption' => 'UNFORMATTED_VALUE'];
            $response = $this->service->spreadsheets_values->get($this->spreadsheetId, $datesRange, $params);
            $sheetsCodes = $response->getValues();
            $this->info(count($sheetsCodes) . ' codes found, building data object');
            $columnToAdd = collect(['', $dateToUpload->format('Y-m-j')]);
            // Add 'general index'
            if ($sheetType === self::INDEXES) {
                $columnToAdd->push($spreadsheet->getActiveSheet()->getCellByColumnAndRow(Coordinate::columnIndexFromString($columnToRead), $generalIndexRow)->getValue());
            }
            foreach ($sheetsCodes as $row => $code) {
                if ($data->has(Arr::first($code))) {
                    $columnToAdd->put($row, $data->get(Arr::first($code)) ?? '');
                } elseif ($row > 2) {
                    $this->info('new product');
                    Log::notice('New product found: ' . $code);
                }
            }
            // Add the info to the new column
            $body = new ValueRange(['values' => [$columnToAdd->toArray()]]);
            $body->setMajorDimension('COLUMNS');
            $params = [
                'valueInputOption' => 'USER_ENTERED',
            ];
            $this->info('appending');
            $response = $this->service->spreadsheets_values->update($this->spreadsheetId, $insertRange, $body, $params);
            $dateToUpload->addMonth();
            $dateString = ucfirst($dateToUpload->locale('es_UY')->isoFormat('MMMM YYYY'));

            // Get column name
            $insertedColumn = mb_substr(Arr::first(explode(':', Arr::last(explode('!', $response->getUpdatedRange())))), 0, -1);
            // Move to the next column
            $columnIdx = Coordinate::columnIndexFromString($insertedColumn);
            $newColumnToInsert = Coordinate::stringFromColumnIndex($columnIdx + 1);
            $insertRange = str_replace($insertedColumn, $newColumnToInsert, $insertRange);
        }
    }

    private function updateSheetWithPreciousUy(Carbon $lastDate, string $range) : void
    {
        $files = $this->getAllPreciousUYFiles();
        $month = $lastDate->copy()->addMonth();
        $expectedFilename = $this->getPreciosUYFilename($month);
        while ($files->has($expectedFilename)) {
            $this->info('Uploading ' . $expectedFilename);
            /** @var \SplFileInfo $file */
            $file = $files->get($expectedFilename);
            $fileObject = $file->openFile();
            $csvData = [];
            $headers = true;
            while (!$fileObject->eof()) {
                $row = $fileObject->fgetcsv(';');
                if ($headers) {
                    $headers = false;
                    continue;
                }
                if (!is_array($row) || !isset($row[1])) {
                    continue;
                }
                $row = array_map('utf8_encode', $row);
                // $rowData = Arr::prepend($row, GoogleSheets::getSerialNumberFromDate($month));
                $rowData = Arr::prepend($row, $month->format('Y-m-j'));
                $rowData = Arr::prepend($rowData, "{$row[1]} - {$row[2]} - {$row[3]}");
                // Fix decimals
                // $rowData = array_map(function ($cell) {
                //     $number = str_replace(',', '.', $cell);
                //     if (is_numeric($number)) {
                //         return $number;
                //     }
                //     return $cell;
                // }, $rowData);
                $csvData[] = $rowData;
            }
            $body = new ValueRange(['values' => $csvData]);
            $body->setRange($range);
            $body->setMajorDimension('ROWS');
            $params = [
                'valueInputOption' => 'USER_ENTERED',
                // 'valueInputOption' => 'RAW',
                'insertDataOption' => 'INSERT_ROWS',
            ];
            $response = $this->service->spreadsheets_values->append($this->spreadsheetId, $range, $body, $params);

            $month = $month->copy()->addMonth();
            $expectedFilename = $this->getPreciosUYFilename($month);
        }
    }

    /**
     * @return \Illuminate\Support\Collection<string, \SplFileInfo>
     */
    private function getAllPreciousUYFiles() : Collection
    {
        /** @var \Illuminate\Support\Collection<string, \SplFileInfo> $files */
        $files = new Collection();
        // Retrieve all files on resources/dicts
        /** @var \SplFileInfo $file */
        foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->ipapFilesBasePath)) as $file) {
            // filter out "." and ".."
            if ($file->isDir() || !preg_match('/precios_\w+_\d{4}\.csv/', $file->getFilename())) {
                continue;
            }
            $files->put($file->getFilename(), $file);
        }
        return $files;
    }

    private function readIneSpreadsheet() : Spreadsheet
    {
        $file = $this->ipapFilesBasePath . 'ine_serie_historica.xls';
        $reader = IOFactory::createReader('Xls');
        $reader->setReadDataOnly(true);
        return $reader->load($file);
    }

    private function getPreciosUYFilename(Carbon $month) : string
    {
        /** @phpstan-ignore-next-line */
        return  'precios_' . $month->locale('es_UY')->monthName . '_' . $month->format('Y') . '.csv';
    }
}
