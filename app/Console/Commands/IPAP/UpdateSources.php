<?php

namespace App\Console\Commands\IPAP;

use DiDom\Document;
use DiDom\Element;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

use function Safe\parse_url;
use function Safe\preg_match;

use SimpleXMLElement;
use Symfony\Component\Console\Output\Output;
use Throwable;

class UpdateSources extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ipap:update-sources';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update sources';
    private string $ipapFilesBasePath;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->ipapFilesBasePath = storage_path('app/ipap/');
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /** @phpstan-ignore-next-line */
        app()->log->pushProcessor(static function (array $record) {
            $record['message'] = "[IPAP][UPDATE-SOURCES] {$record['message']}";
            return $record;
        });

        $this->getInfoFromPreciosUy();
        $this->getInfoFromINE();
        return 0;
    }

    private function getInfoFromPreciosUy() : void
    {
        $presentFiles = $this->getAllPreciousUYFiles();
        $rss = 'http://www.precios.uy/category/reporte-mensual/feed';
        $response = Http::get($rss);

        if (!$response->successful()) {
            $response->throw();
        }
        $feed = new SimpleXMLElement($response->body(), LIBXML_NOWARNING | LIBXML_NOERROR | LIBXML_NOCDATA);
        $this->info(count($feed->channel->item) . ' items found on the RSS feed');
        /** @var \SimpleXMLElement $item */
        foreach ($feed->channel->item as $item) {
            $downloadUrl = null;
            $date = Carbon::createFromLocaleFormat('j F Y', 'es_UY', "1 {$item->title}", 'UTC');
            if (!($date instanceof Carbon)) {
                throw new Exception("Could not parse date {$item->title}");
            }
            // Only download files once
            if ($presentFiles->has($this->getPreciosUYFilename($date))) {
                $this->line($this->getPreciosUYFilename($date) . ' already present, next!', null, Output::VERBOSITY_VERBOSE);
                continue;
            }
            try {
                $document = new Document((string) $item->link, true);
                if (!($document instanceof Document)) {
                    throw new Exception('Could not parse document at ' . $item->link);
                }
                $lists = $document->find('.post-content  ul');
                /** @var \DiDom\Element $list */
                foreach ($lists as $list) {
                    /** @var \DiDom\Element $item */
                    foreach ($list->find('li') as $item) {
                        if ($item->text() === 'Descargar planilla de precios') {
                            $a = $item->first('a');
                            if (!($a instanceof Element)) {
                                throw new Exception('Could not find download link');
                            }
                            $downloadUrl = $a->attr('href', '');
                            if (!is_string($downloadUrl)) {
                                throw new Exception('Could not retrieve download link');
                            }
                            $path = parse_url($downloadUrl, PHP_URL_PATH);
                            if (!is_string($path)) {
                                throw new Exception('Could not retrieve file path');
                            }
                            $filename = Arr::last(explode('/', $path));
                            $filepath = $this->ipapFilesBasePath . $filename;
                            if (file_exists($filepath)) {
                                $this->line("$filename exists...NEXT!!");
                                continue;
                            }
                            $response = Http::withOptions([
                                'sink' => $filepath,
                            ])->get($downloadUrl);
                            if ($response->failed()) {
                                $response->throw();
                            }
                        }
                    }
                }
            } catch (Throwable $e) {
                Log::error($e->getMessage());
                $this->error($e->getMessage());
                $this->info('Trying to guess the url instead');
                $downloadUrl = null;
            }

            if (!is_string($downloadUrl)) {
                $this->tryToDownloadFromGuessUrl($date);
            }
        }
    }

    /**
     * @return \Illuminate\Support\Collection<string, \SplFileInfo>
     */
    private function getAllPreciousUYFiles() : Collection
    {
        $files = new Collection();
        // Retrieve all files on resources/dicts
        /** @var \SplFileInfo $file */
        foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->ipapFilesBasePath)) as $file) {
            // filter out "." and ".."
            if ($file->isDir() || !preg_match('/precios_\w+_\d{4}\.csv/', $file->getFilename())) {
                continue;
            }
            $files->put($file->getFilename(), $file);
        }
        return $files;
    }

    private function getInfoFromINE() : void
    {
        $filepath = $this->ipapFilesBasePath . 'ine_serie_historica.xls';
        $seriesUrl = 'https://www.ine.gub.uy/c/document_library/get_file?uuid=ad969d52-cebc-4b40-9a1f-34ce277e463e&groupId=10181';
        $response = Http::withOptions([
            'sink' => $filepath,
        ])->get($seriesUrl);
        if ($response->failed()) {
            $response->throw();
        }
    }

    private function tryToDownloadFromGuessUrl(Carbon $date) : void
    {
        $downloadUrl = $this->guessPreciosUyDownloadUrl($date);
        $path = parse_url($downloadUrl, PHP_URL_PATH);
        if (!is_string($path)) {
            throw new Exception('Could not retrieve file path');
        }
        $filename = Arr::last(explode('/', $path));
        $filepath = $this->ipapFilesBasePath . $filename;
        $response = Http::withOptions([
            'sink' => $filepath,
        ])->get($downloadUrl);

        if ($response->failed()) {
            $response->throw();
        }
    }

    /**
     * Fallback function, if RSS changes, this can used to try to guess the download url
     * Eg.-
     * http://www.precios.uy/wp-content/uploads/2021/08/precios_julio_2021.csv
     *
     * @param \Illuminate\Support\Carbon $month
     * @return string
     */
    private function guessPreciosUyDownloadUrl(Carbon $month) : string
    {
        $baseUrl = 'http://www.precios.uy/wp-content/uploads/';
        $uploadDate = $month->copy()->addMonth()->format('Y/m/');
        $filename = $this->getPreciosUYFilename($month);
        return $baseUrl . $uploadDate . $filename;
    }

    private function getPreciosUYFilename(Carbon $month) : string
    {
        /** @phpstan-ignore-next-line */
        return  'precios_' . $month->locale('es_UY')->monthName . '_' . $month->format('Y') . '.csv';
    }
}
