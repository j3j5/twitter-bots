<?php

namespace App\Console\Commands\EveryPurchase\Civico;

use Illuminate\Console\Command;
use Illuminate\Support\LazyCollection;

use function Safe\fopen;

class CivicoMerge extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everypurchase:civico-merge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $hidden = true;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $extraFile = fopen(resource_path('data/everypurchase/items-extra2.csv'), 'r');

        $extraHeaders = [];
        $extraItems = collect();
        while (($row = fgetcsv($extraFile)) !== false) {
            if (empty($extraHeaders)) {
                $extraHeaders = collect($row);
                continue;
            }
            $extraItems->push($extraHeaders->combine($row)->toArray());
        }
        fclose($extraFile);

        $extraIds = $extraItems->pluck('ocid')->unique()->values();
        $originalFile = fopen(resource_path('data/everypurchase/items.csv'), 'r');
        $originalHeaders = collect(fgetcsv($originalFile));
        fclose($originalFile);
        $c = LazyCollection::make(function () use ($originalHeaders) {
            $originalFile = fopen(resource_path('data/everypurchase/items.csv'), 'r');
            $i = 0;
            while (($row = fgetcsv($originalFile)) !== false) {
                $i++;
                if ($i === 1) {
                    continue;
                }
                yield $originalHeaders->combine(array_pad($row, 12, ''))->toArray();
            }

            fclose($originalFile);
            // })->count();
        })->filter(fn ($row) => !$extraIds->contains('ocid', $row['ocid']))->count();
        dd($c);

        return Command::SUCCESS;
    }
}
