<?php

namespace App\Console\Commands\EveryPurchase\Civico;

use DiDom\Document;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\LazyCollection;

use function Safe\fopen;
use function Safe\fputcsv;

class Civico extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everypurchase:civico {file}  {--local} {--localHtmlPath=} {--offset=1} {--total=10} {--continue}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Civico scraper';

    private $exportHandle;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // $this->exportFile = ;
        $mode = $this->option('continue') ? 'a' : 'w';
        $this->exportHandle = fopen(resource_path('data/everypurchase/items-2021.csv'), $mode);

        if (!$this->option('continue')) {
            fputcsv($this->exportHandle, ['url', 'ocid', 'nombre', 'item_id', 'cod_articulo', 'proveedor', 'proveedor_doc', 'variacion', 'cantidad', 'precio_unitario', 'caracteristica', 'valor']);
        }

        $total = $this->option('total');
        $this->output->progressStart($total);

        LazyCollection::make(function () {
            $filename = $this->argument('file');
            $handle = fopen($filename, 'r');
            $first = true;
            while (($row = fgetcsv($handle, 1000, ';')) !== false) {
                yield Arr::last(explode('-', $row[0]));
            }

            fclose($handle);
        })->skip($this->option('offset'))->take($total)->each(function ($row) {
            $baseUrl = 'https://www.comprasestatales.gub.uy/consultas/detalle/id/';
            $url = $baseUrl . $row;
            if ($this->option('local')) {
                $localPath = $this->option('localHtmlPath') . "/$row.html";
                $localPaginatedPath = $this->option('localHtmlPath') . "/$row-1.html";

                if (file_exists($localPath)) {
                    $document = new Document($localPath, true);
                } elseif (file_exists($localPaginatedPath)) {
                    for ($i = 1; $i < 5; $i++) {
                        $localPaginatedPath = $this->option('localHtmlPath') . "/$row-$i.html";
                        if (file_exists($localPaginatedPath)) {
                            $document = new Document($localPaginatedPath, true);
                            $this->parseItemsFromDocument($document, $url);
                        }
                    }
                    $this->output->progressAdvance();
                    return;
                }
            } else {
                $document = new Document($url, true);
            }
            $this->parseItemsFromDocument($document, $url);
            $this->output->progressAdvance();
        });

        $this->output->progressFinish();
        return 0;
    }

    public function parseItemsFromDocument(Document $document, string $url)
    {
        $id = Arr::last(explode('/', $url));
        $ocid = "ocds-yfs5dr-$id";
        $domItems = $document->find('.item  .desc-item');
        foreach ($domItems as $domItem) {
            $cod_articulo = $item_id = '';
            $title = $domItem->first('h3');
            foreach ($title->findInDocument('span') as $i => $span) {
                if ($i === 0) {
                    $item_id = trim($span->text(), " \n\r\t\v\0 ");
                } else {
                    $cod_articulo = trim($span->text(), " \n\r\t\v\0 ");
                } // Includes &nbsp;
                $span->remove();
            }
            $itemData['url'] = $url;
            $itemData['ocid'] = $ocid;
            $itemData['nombre'] = trim($title->text(), " \n\r\t\v\0 "); // Includes &nbsp;
            $itemData['item_id'] = $item_id;
            $itemData['cod_articulo'] = $cod_articulo;
            $itemData['proveedor'] = $domItem->first('.provider-name > strong')->text();
            $itemData['proveedor_doc'] = $domItem->first('.provider-name > span')->text();

            $itemData['variacion'] = '';
            if ($domItem->first('.list-inline')->has('li > strong')) {
                $itemData['variacion'] = $domItem->first('.list-inline')->first('li > strong')->text();
            }
            $details = $domItem->find('.list-inline')[1];
            $itemData['cantidad'] = self::getCleanQuantity($details->child(1)->text());
            if (is_array($details->children()) && count($details->children()) >= 4) {
                $itemData['precio_unitario'] = $details->child(3)->text();
            } else {
                $itemData['precio_unitario'] = '';
                $this->error("Sin precio unitario: $url");
            }
            // $itemData['precio_unitario'] = str_replace(['$ ', ',00'], ['$', ''], $details->child(3)->text());
            // $itemData['total_amount'] = str_replace(['$ ', ',00'], ['$', ''], $details->child(5)->text());

            $caracteristicas = [];
            if ($domItem->parent()->has('table.features-table')) {
                $table = $domItem->parent()->first('table.features-table');
                foreach ($table->find('tbody > tr') as $row) {
                    $columns = $row->find('td');
                    $caracteristicas[$columns[0]->text()] = $columns[1]->text();
                }
            }

            if (empty($caracteristicas)) {
                fputcsv($this->exportHandle, $itemData);
            } else {
                foreach ($caracteristicas as $carac => $value) {
                    fputcsv($this->exportHandle, array_merge($itemData, [$carac, $value]));
                }
            }
        }
    }

    protected static function getCleanQuantity(?string $value) : ?string
    {
        if ($value === null) {
            return $value;
        }
        $value = str_replace(',00', '', $value);
        // if (mb_strpos($value, 'UNIDAD') !== false) {
        //     $qty = intval(str_replace('UNIDAD', '', $value));
        //     if ($qty > 1) {
        //         return str_replace('UNIDAD', 'uds.', $value);
        //     }
        //     return str_replace('UNIDAD', 'ud.', $value);
        // }
        return (string) $value;
    }
}
