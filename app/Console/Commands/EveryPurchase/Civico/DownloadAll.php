<?php

namespace App\Console\Commands\EveryPurchase\Civico;

use App\Services\EffectiveUrlMiddleware;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

use function Safe\fclose;
use function Safe\fopen;

class DownloadAll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everypurchase:download {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download all pages';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $existing = collect();
        $h = fopen('compras-new', 'r');
        while (($id = fgets($h)) !== false) {
            $existing->put(rtrim($id, "\n"), '');
        }
        fclose($h);

        // dump($existing->count());

        $stack = HandlerStack::create();
        $stack->push(EffectiveUrlMiddleware::middleware());
        $client = new Client([
            'handler' => $stack,
            'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64; rv:93.0) Gecko/20100101 Firefox/93.0',
            'Accept-Language' => 'en-US,en;q=0.5',
            'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
            // 'Accept-Encoding' => 'gzip, deflate, br',
        ]);

        $this->output->progressStart(896802);

        $requests = function () use ($existing) {
            $i = 0;
            $filename = $this->argument('file');
            if (!is_string($filename)) {
                throw new Exception('Invalid filename ');
            }
            $handle = fopen($filename, 'r');
            $baseUri = 'https://www.comprasestatales.gub.uy/consultas/detalle/id/';
            while (($row = fgetcsv($handle, 1000, ';')) !== false) {
                $i++;
                if ($i < 1) {
                    $this->output->progressAdvance();
                    continue;
                }
                $id = Arr::last(explode('-', Arr::get($row, '1')));

                if ($existing->has($id)) {
                    $this->output->progressAdvance();
                    continue;
                }

                $uri = $baseUri . $id;
                $headers = [
                    'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
                    'Accept-Language' => 'en-US,en;q=0.5',
                    'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
                    // 'Accept-Encoding' => 'gzip, deflate, br',
                ];
                yield new Request('GET', $uri, $headers);
            }
        };

        $pool = new Pool($client, $requests(), [
            'concurrency' => 10,
            // 'options' => [
            // ],
            'fulfilled' => function (Response $response, $index) {
                $uri = Arr::first($response->getHeader('X-GUZZLE-EFFECTIVE-URL'));
                $id = Arr::last(explode('/', $uri));
                Storage::put("arce/$id.html", (string) $response->getBody());
                // $this->info($id . ' stored');
                $this->output->progressAdvance();
            },
            'rejected' => function (RequestException $reason, $index) {
                Storage::append('arce/failed', $reason->getMessage() . PHP_EOL);
                $this->error('failed, sleep for a bit');
            },
        ]);

        // Initiate the transfers and create a promise
        $promise = $pool->promise();

        // Force the pool of requests to complete.
        $promise->wait();

        $this->output->progressFinish();

        return 0;
    }
}
