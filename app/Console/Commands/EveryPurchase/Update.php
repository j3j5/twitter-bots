<?php

namespace App\Console\Commands\EveryPurchase;

use App\Models\EveryPurchase\Purchase;
use App\Models\EveryPurchase\PurchaseSource;
use App\Services\UserAgent;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use SimpleXMLElement;
use Throwable;

class Update extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everypurchase:update {source?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update purchases from RSS and store them in the DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $sources = PurchaseSource::query();

        if ($this->argument('source')) {
            $sources->where('slug', $this->argument('source'));
        }

        $sources->get()->each(function (PurchaseSource $source) {
            $this->updateFromSource($source);
        });
        return 0;
    }

    private function updateFromSource(PurchaseSource $source) : void
    {
        $this->info('Updating ' . $source->name);
        $this->line($source->rss);

        /** @var \App\Services\UserAgent $userAgent */
        $userAgent = resolve(UserAgent::class);

        $response = Http::withHeaders([
            'User-Agent' => $userAgent->get(),
        ])->get($source->rss);

        if (!$response->successful()) {
            $response->throw();
        }
        $feed = new SimpleXMLElement($response->body(), LIBXML_NOWARNING | LIBXML_NOERROR | LIBXML_NOCDATA);
        $this->output->progressStart(count($feed->channel->item));
        /** @var \SimpleXMLElement $item */
        foreach ($feed->channel->item as $item) {
            $this->output->progressAdvance();
            if (Purchase::where('link', $item->link)->exists()) {
                continue;
            }

            try {
                Purchase::createOrUpdateFromRSSItem($item, $source);
            } catch (Throwable $e) {
                $this->error('Ignoring ' . $item->link);
                app('sentry')->captureException($e);
                Log::warning($e->getMessage());
            }
        }
        $this->output->progressFinish();
    }
}
