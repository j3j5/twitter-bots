<?php

namespace App\Console\Commands\EveryPurchase;

use App\DTO\Twitter\Tweet;
use App\Models\EveryPurchase\Purchase;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use j3j5\TwitterApio;

class Post extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everypurchase:post';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post the tweets for everypurchase';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(TwitterApio $twitterApi)
    {
        $purchase = Purchase::notTweetedYet()->has('items')->orderBy('bought_at')->first();
        if (!$purchase instanceof Purchase) {
            return 0;
        }

        $twitterApi->reconfigure($purchase->source->twitter_config);
        $tweets = $purchase->tweets;
        $this->info('Tweeting ' . count($tweets) . ' tweets');
        $reply = null;
        $tweet1 = null;
        foreach ($tweets as $tweet) {
            $tweetOptions = [
                'status' => $tweet,
            ];
            if ($reply !== null) {
                $tweetOptions['in_reply_to_status_id'] = $reply;
            }
            Log::debug('tweeting with ', $tweetOptions);
            $tweetResponse = $twitterApi->post('statuses/update', $tweetOptions);
            if (is_array($tweetResponse)) {
                $reply = Arr::get($tweetResponse, 'id_str');
                if ($tweet1 === null) {
                    $tweet1 = new Tweet($tweetResponse);
                }
            }
        }
        if ($tweet1 instanceof Tweet) {
            $purchase->tweet_id = $tweet1->getStatusId();
            $purchase->user_id = $tweet1->getAuthor()->getId();
            $purchase->username = $tweet1->getAuthor()->getUsername();
            $purchase->tweeted_at = $tweet1->getCreatedAt();
            $purchase->save();
        }

        return 0;
    }
}
