<?php

namespace App\Console\Commands\EveryPurchase;

use App\Models\EveryPurchase\PurchaseSource;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class AddSource extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everypurchase:add-source';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add new sources to EveryPurchase';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Welcome, do you want to add a new source?');
        $source = new PurchaseSource();
        $source->name = $this->ask("What's the name?");
        $source->slug = Str::slug($source->name);
        $source->rss = $this->ask("What's the RSS?");
        $source->twitter_config = $this->ask("What's the slug for the twitter config?");

        $source->save();

        $this->info('Done!');

        return 0;
    }
}
