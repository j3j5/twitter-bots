<?php

namespace App\Console\Commands\LitClock;

use App\Services\FediApi;
use Google\Service\Sheets;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class PostTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'litclock:post-time';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post a literary quote with the current time';

    public static string $cacheKey;

    public function __construct()
    {
        parent::__construct();
        self::$cacheKey = 'litclock-' . now()->format('H');
    }

    /**
     * Execute the console command.
     */
    public function handle(Sheets $service, FediApi $api)
    {
        $spreadsheetId = '1YdsWAyYN_EJkHtqaZhPan8zEi_M9XgVuSnR_mt6PK4c';
        $sheet = "'litclock_annotated_es'";
        // Calculate the highest column currently to get the InsertRange (next column)
        $datesRange = "$sheet!A:F";
        $params = ['valueRenderOption' => 'UNFORMATTED_VALUE'];
        $response = $service->spreadsheets_values->get($spreadsheetId, $datesRange, $params);
        // /** @var array<int, array<int, string|int|float>> */
        $values = collect($response->getValues());
        $headers = collect($values->shift());
        $values->transform(fn (array $row) => $headers->combine($row));

        $currentTime = now('America/Montevideo')->format('H:i');
        $currentTimeQuotes = $values->where('Hora', '=', $currentTime);
        if ($currentTimeQuotes->isEmpty()) {
            Log::debug('No quote found for ' . $currentTime);
            $this->error('No quote found for ' . $currentTime);
            return;
        }
        $quote = $currentTimeQuotes->random();
        $message = '<blockquote>';
        $message .= str_replace($quote['Resaltar'], '<strong>' . $quote['Resaltar'] . '</strong>', $quote['Cita']);
        $message .= '</blockquote><p>';
        $message .= '&emsp;&emsp;&emsp;&emsp;- ' . $quote['Autor'] . ', <em>' . $quote['Libro'] . '</em></p>';

        $this->info($message);
        $api->publishPost($message);
        $times = (int) Cache::get(self::$cacheKey) + 1;
        Cache::set(self::$cacheKey, $times, now()->addHour());
    }
}
