<?php

namespace App\Console\Commands\Bicisenda;

use App\Services\Bicisenda\BiciApi;
use App\Services\Bicisenda\Graphs;
use App\Services\FediApi;
use App\Services\Inumet;
use App\Traits\Graphs\GeneratesGraphs;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use RuntimeException;

use function Safe\json_decode;
use Sentry;

use Throwable;

class Post extends Command
{
    use GeneratesGraphs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bici:post {--dry-run} {--type=hourly}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private FediApi $fediApi;
    private BiciApi $biciApi;
    private Graphs $graphs;
    private bool $dryRun;
    private string $filePrefix = 'Ciclovia18';

    /**
     * Execute the console command.
     */
    public function handle(BiciApi $api, FediApi $fediApi, Graphs $graphs)
    {
        $this->biciApi = $api;
        $this->fediApi = $fediApi;
        $this->graphs = $graphs;
        $this->dryRun = (bool) $this->option('dry-run');

        [$message, $options] = match($this->option('type')) {
            'hourly' => $this->hourly(),
            'daily' => $this->daily(),
            'weekly' => $this->weekly(),
        };

        if ($this->option('dry-run')) {
            $this->info(
                str_replace(
                    search: ['<br>', '<br/>', '<ul>', '<li>', '<p>'],
                    replace: [PHP_EOL, PHP_EOL, "\n<ul>", "\n<li>", "\n<p>"],
                    subject: $message
                )
            );
            dump($options);
            exit;
        }

        // Publish
        $this->fediApi->publishPost($message, $options);
    }

    private function hourly() : array
    {
        $now = now('America/Montevideo')->minute(0)->second(0)->microsecond(0);

        // Last hour
        $start = $now->copy()->subHour();
        $end = $now->copy();
        $currentData = $this->biciApi->getDataGroupByHour($start, $end);
        // Previous hour
        $prevStart = $now->copy()->subHours(2);
        $prevEnd = $prevStart->copy()->addHour();
        $prevHourData = $this->biciApi->getDataGroupByHour($prevStart, $prevEnd);

        $diffPrevHour = [
            BiciApi::CIUDAD_VIEJA => $currentData[BiciApi::CIUDAD_VIEJA] - $prevHourData[BiciApi::CIUDAD_VIEJA],
            BiciApi::TRES_CRUCES => $currentData[BiciApi::TRES_CRUCES] - $prevHourData[BiciApi::TRES_CRUCES],
        ];

        $message = '<p>🗓️' . $start->toDateString() . ' | ';
        $message .= $this->getEmojiForTime($start) . $start->format('H:i') . '-';
        $message .= $this->getEmojiForTime($end) . $end->format('H:i') . '<br>';
        $message .= '📍 18 de Julio y Magallanes<br>';

        // Add weather
        /** @var \App\Services\Inumet $inumet */
        $inumet = resolve(Inumet::class);
        // Get data from previous hour
        $weatherData = Storage::get('inumet/observations/' . $start->format('H\.\j\s\o\n'));
        try {
            $inumet->setData(collect(json_decode($weatherData, true)));
        } catch(Throwable $e) {
            Log::notice($e->getMessage());
        }

        $weatherStation = 'Prado';

        try {
            // Temperature
            $temperature = $inumet->getCurrentTemperatureForStation($weatherStation);
            $message .= "🌡️ {$temperature}°C<br>";
        } catch (Exception $e) {
            Log::error('Error getting temperature: ' . $e->getMessage());
            Sentry::captureException($e);
        }

        try {
            // Weather emoji plus sky status
            $weatherEmoji = $inumet->getCurrentWeatherEmojiForStation($weatherStation);
            $sky = $inumet->getCurrentSkyStatusForStation($weatherStation);
            $message .= "$weatherEmoji $sky<br>";
        } catch (Exception $e) {
            Log::error('Error getting weather data: ' . $e->getMessage());
            Sentry::captureException($e);
        }

        try {
            // Wind speed plus direction emoji
            $windEmoji = $inumet->getCurrentWindDirectionEmojiForStation($weatherStation);
            $windSpeed = $inumet->getCurrentWindSpeed($weatherStation);
            $message .= "🌬️ $windSpeed km/h $windEmoji<br>";
        } catch (Exception $e) {
            Log::error('Error getting weather data: ' . $e->getMessage());
            Sentry::captureException($e);
        }
        $totalDiff = ($currentData[BiciApi::CIUDAD_VIEJA] + $currentData[BiciApi::TRES_CRUCES])
            - ($prevHourData[BiciApi::CIUDAD_VIEJA] + $prevHourData[BiciApi::TRES_CRUCES]);
        $message .= '<br>' . ($currentData[BiciApi::CIUDAD_VIEJA] + $currentData[BiciApi::TRES_CRUCES]);
        $message .= $totalDiff > 0 ? ' (+' : ' (';
        $message .= "$totalDiff ";
        $message .= $totalDiff > 0 ? '📈' : '📉';
        $message .= ') bicis pasaron esta última hora:</p>';

        $message .= "<ul><li>🏛️ {$currentData[BiciApi::CIUDAD_VIEJA]} 🚲 hacia Ciudad Vieja (";
        if ($diffPrevHour[BiciApi::CIUDAD_VIEJA] > 0) {
            $message .= "+{$diffPrevHour[BiciApi::CIUDAD_VIEJA]}";
        } elseif ($diffPrevHour[BiciApi::CIUDAD_VIEJA] < 0) {
            $message .= "{$diffPrevHour[BiciApi::CIUDAD_VIEJA]}";
        } else {
            $message .= '=';
        }
        $message .= ' hora previa)</li>';
        $message .= "<li>🚍 {$currentData[BiciApi::TRES_CRUCES]} 🚲 hacia Tres Cruces (";
        if ($diffPrevHour[BiciApi::TRES_CRUCES] > 0) {
            $message .= "+{$diffPrevHour[BiciApi::TRES_CRUCES]}";
        } elseif ($diffPrevHour[BiciApi::TRES_CRUCES] < 0) {
            $message .= $diffPrevHour[BiciApi::TRES_CRUCES];
        } else {
            $message .= '=';
        }
        $message .= ' hora previa)</li></ul><p>#Uruguay #Montevideo #Heber #Ciclovia18PorHora</p>';

        return [$message, []];
    }

    private function daily() : array
    {
        $yesterday = now('America/Montevideo')->subDay(1);

        // Get data for the previous 4 weeks
        $start = $yesterday->copy()->subWeeks(4)->startOfWeek();
        $end = $yesterday->copy()->subWeeks(1)->endOfWeek();

        $where = 'WHERE fecha_conteo >= ' . $start->getTimestampMs();
        $where .= ' AND fecha_conteo <= ' . $end->getTimestampMs();
        $where .= " AND extract('dow' FROM fecha_conteo at time zone '-03') = " . $yesterday->dayOfWeekIso;

        $sql = <<<QUERY
            SELECT
            in_counts as sentido_ciudad_vieja,
            out_counts as sentido_tres_cruces,
            fecha_conteo
            FROM observatorio.v_omo_conteo_bicisenda
            $where
            ORDER BY fecha_conteo
        QUERY;
        $response = $this->biciApi->query(sql: $sql);

        $fields = $response->collect('results.A.frames.0.schema.fields')->pluck('name');
        $values = $response->collect('results.A.frames.0.data.values');

        // Get the data for the past 4 weeks
        $pastData = collect(range(0, count($values->first()) - 1))
            // Add readable headers
            ->map(fn ($index) => $fields->combine($values->pluck($index)))
            // Get only the days we need
            ->filter(fn (Collection $item) => Carbon::createFromTimestampMs($item['fecha_conteo'], 'America/Montevideo')->dayOfWeekIso === $yesterday->dayOfWeekIso)
            // Group values by day (date)
            ->groupBy(fn (Collection $item) => Carbon::createFromTimestampMs($item['fecha_conteo'], 'America/Montevideo')->format('Y-m-d'))
            // Group values by hour (on each day)
            ->mapWithKeys(
                fn (Collection $dayData, string $date) => [$date => $dayData->groupBy(
                    fn (Collection $item) => Carbon::createFromTimestampMs($item['fecha_conteo'], 'America/Montevideo')->format('H') . ':00'
                )->sortKeys()]
                // Sum all values on each hour (to get the count by hour)
            )->mapWithKeys(
                fn (Collection $dayData, string $date) => [
                    $date => $dayData->mapWithKeys(fn (Collection $hourData, string $hour) => collect([
                        $hour => [
                            BiciApi::CIUDAD_VIEJA => $hourData->sum(BiciApi::CIUDAD_VIEJA),
                            BiciApi::TRES_CRUCES => $hourData->sum(BiciApi::TRES_CRUCES),
                        ],
                    ])),
                ]
            );

        $avgData = collect(range(0, 23))->mapWithKeys(
            fn (int $hour) => [str_pad($hour, 2, '0', STR_PAD_LEFT) . ':00' => $pastData->map(
                fn (Collection $dayData, $day) => $dayData->get(str_pad($hour, 2, '0', STR_PAD_LEFT) . ':00')
            )]
        );
        $avgDataCV = $avgData->map(
            fn (Collection $hourData) => $hourData->avg(BiciApi::CIUDAD_VIEJA)
        );
        $avgDataTC = $avgData->map(
            fn (Collection $hourData) => $hourData->avg(BiciApi::TRES_CRUCES)
        );

        // Get daily data
        $start = $yesterday->copy()->startOfDay();
        $end = $yesterday->copy()->endOfDay();
        $dailyData = $this->biciApi->getDataGroupByHour(start: $start, end: $end, singleValue: false);

        $ciudadVieja = $dailyData->pluck(BiciApi::CIUDAD_VIEJA);
        $tresCruces = $dailyData->pluck(BiciApi::TRES_CRUCES);

        $labels = $dailyData->map(
            fn ($hourData) => $yesterday->copy()
                ->hour($hourData['hora'])
                ->startOfHour()
                ->timestamp
        )->all();

        $dayData = collect([
            'Hacia Ciudad Vieja' => $ciudadVieja,
            'Hacia Tres Cruces' => $tresCruces,
            'Media hacia CV 4 ' . $yesterday->locale('es_UY')->isoFormat('ddd') . ' ant.' => $avgDataCV->values(),
            'Media hacia TC 4 ' . $yesterday->locale('es_UY')->isoFormat('ddd') . ' ant.' => $avgDataTC->values(),
        ]);

        $title = 'Número de bicis';
        $subtitle = '18 de Julio y Magallanes';
        $subsubtitle = ucwords($start->copy()->locale('es_UY')->isoFormat('dddd D MMMM YYYY'));

        $chart = $this->graphs->generateGroupedBarChartWithLine(
            data: $dayData,
            labels: $labels,
            width: 1920,
            height: 1080,
            title: $title,
            subtitle: $subtitle,
            subsubtitle: $subsubtitle,
            addWeather: true,
            type: 'hourly',
        );

        // Compose the message
        $date = $yesterday->locale('es_UY')->isoFormat('LL');
        $day = Str::plural($yesterday->locale('es_UY')->isoFormat('dddd'), 4);
        $totalDay = $ciudadVieja->sum() + $tresCruces->sum();
        $totalAvg = $avgDataCV->sum() + $avgDataTC->sum();

        $diff = [
            BiciApi::CIUDAD_VIEJA => round($ciudadVieja->sum() - $avgDataCV->sum(), 0),
            BiciApi::TRES_CRUCES => round($tresCruces->sum() - $avgDataTC->sum(), 0),
        ];

        $message = "<p>Cantidad de bicis que pasaron ayer ($date)";
        $message .= ' por la ciclovía de 18 de Julio (cruce con Magallanes) y ';
        $message .= "la comparación con la media de los 4 $day anteriores.</p>";

        $message .= '<ul><li>🏛️ ' . number_format(num: $ciudadVieja->sum(), thousands_separator: '.') . ' 🚲 hacia Ciudad Vieja (';
        if ($diff[BiciApi::CIUDAD_VIEJA] > 0) {
            $message .= "+{$diff[BiciApi::CIUDAD_VIEJA]}";
        } elseif ($diff[BiciApi::CIUDAD_VIEJA] < 0) {
            $message .= "{$diff[BiciApi::CIUDAD_VIEJA]}";
        } else {
            $message .= '=';
        }
        $message .= " media de los últimos 4 $day)</li>";
        $message .= '<li>🏛️ ' . number_format(num: $tresCruces->sum(), thousands_separator: '.') . ' 🚲 hacia Tres Cruces (';
        if ($diff[BiciApi::TRES_CRUCES] > 0) {
            $message .= "+{$diff[BiciApi::TRES_CRUCES]}";
        } elseif ($diff[BiciApi::TRES_CRUCES] < 0) {
            $message .= $diff[BiciApi::TRES_CRUCES];
        } else {
            $message .= '=';
        }
        $message .= " media de los últimos 4 $day)</li></ul><p>#Uruguay #Montevideo #Heber #Ciclovia18Diario</p>";

        // Upload the image
        if ($this->dryRun) {
            $imgPath = storage_path('app/bicisenda/weekly-bargroup-' . now('America/Montevideo')->format('Y-m-d-H:i:s') . '.png');
            file_put_contents($imgPath, $chart);
            $options = [$imgPath];
        } else {
            $imgPath = $this->filePrefix . '-daily-' . $start->format('Y-m-d') . '.jpg';
            $uuid = Storage::disk('uploadcare')->putGetUuid($imgPath, $chart);
            if (empty($uuid)) {
                throw new RuntimeException('Error while uploading the bike graph');
            }
            $date = $yesterday->locale('es_UY')->isoFormat('[día] D [de] MMMM [de] YYYY');
            $day = Str::plural($yesterday->locale('es_UY')->isoFormat('dddd'), 4);
            $alt = "Gráfico de barras que muestra la cantidad de bicis que cirucularon por el cruce de 18 de Julio y Magallanes (en Montevideo) en ambas direcciones durante el $date por hora. Superpuestos se pueden ver dos gráficos de líneas que muestran, de media, la cantidad de bicis que pasaron durante los últimos cuatro $day anteriores.";
            $alt .= 'En total pasaron ' . number_format(num: $totalDay, thousands_separator: '.');
            $alt .= ' bicis (' . number_format(num: $ciudadVieja->sum(), thousands_separator: '.') . ' hacia Ciudad Vieja, ';
            $alt .= number_format(num: $tresCruces->sum(), thousands_separator: '.');
            $alt .= ' hacia Tres Cruces), esto es, ' . number_format(num: intval(abs($totalAvg - $totalDay)), thousands_separator: '.') . ' bicis ';
            $alt .= $totalAvg > $totalDay ? 'menos' : 'más';
            $alt .= " que la media de los últimos 4 $day (";
            $alt .= number_format(num: $totalAvg, thousands_separator: '.') . ' bicis).';
            $options = ['media' => [[
                'mediaType' => 'image/jpeg',
                'url' => config('filesystems.disks.uploadcare.cdn') . '/' . $uuid . '/' ,
                'name' => $alt,
            ]]];
        }

        return [$message, $options];
    }

    private function weekly() : array
    {
        $lastWeek = now('America/Montevideo')->subWeeks(1)->startOfDay();

        $start = $lastWeek->copy()->subWeeks(4)->startOfWeek();
        $end = $lastWeek->copy()->subWeeks(1)->endOfWeek();
        $avgQuery = sprintf(<<<QUERY
            SELECT
            sum(in_counts) as %s,
            sum(out_counts) as %s,
            count(*) as count,
            dia, mes, anio
            FROM observatorio.v_omo_conteo_bicisenda
            WHERE fecha_conteo >= %d AND fecha_conteo <= %d
            GROUP BY dia, mes, anio
        QUERY, BiciApi::CIUDAD_VIEJA, BiciApi::TRES_CRUCES, $start->getTimestampMs(), $end->getTimestampMs());

        $response = $this->biciApi->query(sql: $avgQuery);

        $fields = $response->collect('results.A.frames.0.schema.fields')->pluck('name');
        $values = $response->collect('results.A.frames.0.data.values');

        $days = [
            'Lunes' ,
            'Martes' ,
            'Miércoles' ,
            'Jueves' ,
            'Viernes' ,
            'Sábado' ,
            'Domingo' ,
        ];

        // Get data grouped by day of the week
        $avgData = collect(range(0, count($values->first()) - 1))
            ->map(fn ($index) => $fields->combine($values->pluck($index)))
            ->groupBy(
                fn ($data) => ucfirst(Carbon::parse(
                    implode(
                        ' ',
                        [
                            $data['dia'],
                            $data['mes'],
                            $data['anio'],
                        ]
                    )
                )->locale('es')->isoFormat('dddd'))
            )->sortKeysUsing(
                fn ($a, $b) => array_search($a, $days, true) - array_search($b, $days, true)
            );
        /** @var \Illuminate\Support\Collection<string, float> $avgDataCV */
        $avgDataCV = $avgData->map->avg(BiciApi::CIUDAD_VIEJA);
        /** @var \Illuminate\Support\Collection<string, float> $avgDataTC */
        $avgDataTC = $avgData->map->avg(BiciApi::TRES_CRUCES);

        // Last week
        $start = $lastWeek->copy()->startOfWeek();
        $end = $lastWeek->copy()->endOfWeek();

        $currentData = $this->biciApi->getDataGroupByDay(start: $start, end: $end);
        $labels = $currentData->keys();
        $ciudadVieja = $currentData->pluck(BiciApi::CIUDAD_VIEJA);
        $tresCruces = $currentData->pluck(BiciApi::TRES_CRUCES);

        $dayData = collect([
            'Hacia Ciudad Vieja' => $ciudadVieja,
            'Hacia Tres Cruces' => $tresCruces,
            'Media hacia CV 4 sem. ant.' => $avgDataCV->values(),
            'Media hacia TC 4 sem. ant.' => $avgDataTC->values(),
        ]);

        $title = 'Cantidad de bicis';
        $subtitle = '18 de Julio y Magallanes, ambas direcciones';
        $subsubtitle = 'Semana del ' . $start->locale('es_UY')->isoFormat('D');
        $subsubtitle .= ' al ' . $end->locale('es_UY')->isoFormat('LL');

        // $formatLabels = ;
        $chart = $this->graphs->generateGroupedBarChartWithLine(
            data: $dayData,
            labels: $labels->all(),
            width: 1920,
            height: 1080,
            title: $title,
            subtitle: $subtitle,
            subsubtitle: $subsubtitle,
            addWeather: true,
            type: 'daily',
        );

        // Compose the message and the alt
        $totalWeek = $ciudadVieja->sum() + $tresCruces->sum();
        $totalAvg = $avgDataCV->sum() + $avgDataTC->sum();

        $date = 'del ' . $start->locale('es_UY')->isoFormat('D')
            . ' al ' . $end->locale('es_UY')->isoFormat('LL');

        $message = "<p>Cantidad de bicis que pasaron la semana pasada ($date)";
        $message .= ' por la ciclovía de 18 de Julio (cruce con Magallanes) y ';
        $message .= 'la comparación con la media de las 4 semanas anteriores.</p>';
        $message .= '<p>En total pasaron ' . number_format(num: $totalWeek, thousands_separator: '.');
        $message .= ' bicis (' . number_format(num: $ciudadVieja->sum(), thousands_separator: '.') . ' hacia Ciudad Vieja, ';
        $message .= number_format(num: $tresCruces->sum(), thousands_separator: '.');
        $message .= ' hacia Tres Cruces), esto es, ' . number_format(num: intval(abs($totalAvg - $totalWeek)), thousands_separator: '.') . ' bicis ';
        $message .= $totalAvg > $totalWeek ? 'menos' : 'más';
        $message .= ' que la media de las últimas 4 semanas (';
        $message .= number_format(num: $totalAvg, thousands_separator: '.') . ' bicis).</p>';

        $options = [];

        if ($this->dryRun) {
            $imgPath = storage_path('app/bicisenda/weekly-bargroup-' . now('America/Montevideo')->format('Y-m-d-H:i:s') . '.png');
            file_put_contents($imgPath, $chart);
            $options = [$imgPath];
        } else {
            $imgPath = $this->filePrefix . '-weekly-' . $start->format('Y-m-d') . '.jpg';
            $uuid = Storage::disk('uploadcare')->putGetUuid($imgPath, $chart);
            if (empty($uuid)) {
                throw new RuntimeException('Error while uploading the bike graph');
            }
            $date = $start->locale('es_UY')->isoFormat('D') . ' al ' . $end->locale('es_UY')->isoFormat('LL');
            $alt = "Gráfico de barras que muestra la cantidad de bicis que cirucularon por el cruce de 18 de Julio y Magallanes (en Montevideo) en ambas direcciones durante la semana de $date por día. Superpuesto se pueden ver dos gráficos de líneas que muestran, de media, la cantidad de bicis que pasaron durante las últimas cuatro semanas.";
            $options = ['media' => [[
                'mediaType' => 'image/jpeg',
                'url' => config('filesystems.disks.uploadcare.cdn') . '/' . $uuid . '/' ,
                'name' => $alt,
            ]]];
        }

        return [$message, $options];
    }

    private function getEmojiForTime(Carbon $time) : string
    {
        $times = [
            '🕛',
            '🕐',
            '🕑',
            '🕒',
            '🕓',
            '🕔',
            '🕕',
            '🕖',
            '🕗',
            '🕘',
            '🕙',
            '🕚',
            '🕛',
            '🕐',
            '🕑',
            '🕒',
            '🕓',
            '🕔',
            '🕕',
            '🕖',
            '🕗',
            '🕘',
            '🕙',
            '🕚',
        ];
        return $times[$time->hour];
    }
}
