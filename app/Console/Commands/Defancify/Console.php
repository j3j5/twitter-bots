<?php

namespace App\Console\Commands\Defancify;

use App\Repositories\Dictionaries;
use Illuminate\Console\Command;

class Console extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'defancify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $original = 'abc 𝘾𝙖𝙣𝙩𝙞𝙙𝙖𝙙 𝙙𝙚 𝙘𝙖𝙨𝙤𝙨 𝙖𝙘𝙩𝙞𝙫𝙤𝙨 𝙙𝙚 𝘾𝙊𝙑𝙄𝘿-19 𝙙𝙚𝙨𝙘𝙚𝙣𝙙𝙞ó 𝙖 1.812 𝙚𝙨𝙩𝙚 𝙢𝙞é𝙧𝙘𝙤𝙡𝙚𝙨 4.';
        $translated = app(Dictionaries::class)->translate($original);

        $this->error($original);
        $this->newLine();
        $this->line($translated);

        return 0;
    }
}
