<?php

namespace App\Console\Commands\Defancify;

use App\DTO\Twitter\Tweet;
use App\Repositories\Dictionaries;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use j3j5\TwitterApio;
use RuntimeException;

// use function Safe\mb_str_split;

class TestResponse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'defancify:test-response {tweetId} {--tweet}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test defancification for a given tweet';

    protected Tweet $tweet;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(TwitterApio $api) : int
    {
        $tweetId = $this->argument('tweetId');
        if (!is_string($tweetId)) {
            $this->error('Tweet id must be a single value');
            return -1;
        }
        $tweetArr = $api->get('statuses/show', [
            'id' => $tweetId,
            'include_entities' => true,
            'tweet_mode' => 'extended',
        ]);

        if (!is_array($tweetArr)) {
            $this->error("Could not retrieve the original tweet {$tweetId}");
            return -1;
        }

        Log::debug('Response:', $tweetArr);

        $this->tweet = new Tweet($tweetArr);

        // Only process replies
        if (!$this->tweet->isReply()) {
            $this->error("Tweet {$this->tweet->id_str} is not a reply, ignoring");
            return -1;
        }

        // Don't reply to yourself
        if ($this->tweet->getReplyUserId() === Arr::get($api->getConfig(), 'bot_user_id', '')) {
            $this->error('Original tweet is from the bot, ignore');
            return -1;
        }

        $this->line('Retrieving original tweet, id => ' . $this->tweet->getReplyStatusId());
        $originalArr = $api->get('statuses/show', [
            'id' => $this->tweet->getReplyStatusId(),
            'tweet_mode' => 'extended',
        ]);

        if (!is_array($originalArr)) {
            $this->error("Could not retrieve the original tweet {$this->tweet->getReplyStatusId()} from @{$this->tweet->getReplyUsername()}");
            return -1;
        }

        $this->line('Translating tweet');
        $original = new Tweet($originalArr);
        $translation = app(Dictionaries::class)->translate($original->getText());

        // Don't reply to normal tweets
        if ($original->getText() === $translation) {
            $this->info('Not a fancy tweet, only a mention');
            return 0;
        }

        $reply = $this->tweet->getStatusId();

        $this->info($translation);
        $this->info('Replying to tweet: ' . $reply);

        $tweets = $this->getTweets($translation);
        dump($tweets);

        foreach ($tweets as $tweet) {
            Log::debug('Sending reply: ' . $tweet);
            if ($this->option('tweet')) {
                // Send reply
                $response = $api->post('statuses/update', [
                    'status' => $tweet,
                    'in_reply_to_status_id' => $reply,
                ]);
                if (!is_array($response)) {
                    throw new RuntimeException('Error while tweeting');
                }
                $reply = Arr::get($response, 'id_str');
            }
        }

        return 0;
    }

    private function getTweets(string $translation) : array
    {
        $tweets = [];
        $tweet = '';
        $originalText = collect(mb_str_split('@' . $this->tweet->getAuthor()->getUsername() . ' ' . $translation))->reverse();
        while ($originalText->isNotEmpty()) {
            while (Tweet::countChars($tweet) <= 265 && $originalText->isNotEmpty()) { // Being on the safe side
                $tweet .= $originalText->pop();
            }
            // Another tweet is coming, cut the string on the previous word
            if ($originalText->isNotEmpty()) {
                $lastSpacePos = mb_strrpos($tweet, ' ');
                if ($lastSpacePos !== false) {
                    $remaining = trim(mb_substr($tweet, $lastSpacePos));
                    $tweet = mb_substr($tweet, 0, $lastSpacePos);
                }

                if (!empty($remaining)) {
                    $remainingArray = mb_str_split(Tweet::mbStrrev($remaining));
                    foreach ($remainingArray as $char) {
                        $originalText->push($char);
                    }
                }
            }
            $tweets[] = $tweet;
            $tweet = '';
        }

        if (count($tweets) > 1) {
            foreach ($tweets as $i => &$tweet) {
                $tweet .= ' (' . ($i + 1) . '/' . count($tweets) . ')';
            }
        }

        return $tweets;
    }
}
