<?php

namespace App\Console\Commands\Defancify;

use App\Processors\DefancifyStream;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use j3j5\TwitterApio;
use Phirehose;

use function Safe\define;

class MentionsStream extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'defancify:mentions-stream {user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Listen for mentions to defancify for a given user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(TwitterApio $api)
    {
        $this->addLogId('DEFANCIFY');

        $user = $this->argument('user');
        if (!is_string($user)) {
            $this->error('invalid username');
            return -1;
        }
        $config = $api->getConfig();

        define('TWITTER_CONSUMER_KEY', Arr::get($config, 'consumer_key', ''));
        define('TWITTER_CONSUMER_SECRET', Arr::get($config, 'consumer_secret', ''));

        $stream = new DefancifyStream(
            Arr::get($config, 'token', ''),
            Arr::get($config, 'secret', ''),
            Phirehose::METHOD_FILTER
        );
        $stream->setTrack(["@$user"]);
        $stream->consume();

        return 0;
    }

    protected function addLogId(string $id) : void
    {
        app()->log->pushProcessor(static function (array $record) use ($id) {
            $record['message'] = "[$id] {$record['message']}";
            return $record;
        });
    }
}
