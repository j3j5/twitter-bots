<?php

namespace App\Console\Commands\EveryUruguayan;

use App\DTO\Twitter\Tweet;
use App\Models\EveryUruguayan\Uruguayan;
use Illuminate\Console\Command;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everyuru:test {number=20}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    protected $hidden = true;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $uruguayans = Uruguayan::inRandomOrder()->take($this->argument('number'))->get();
        $uruguayans->each(function ($uruguayan) {
            $this->line($uruguayan->hogar_id . '-' . $uruguayan->persona_id);
            $tweet = $uruguayan->getTweet();
            $this->info($tweet);
            $this->error(Tweet::countChars($tweet));
        });
        return 0;
    }
}
