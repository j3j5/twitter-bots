<?php

namespace App\Console\Commands\EveryUruguayan;

use App\Models\EveryUruguayan\Uruguayan;
use Illuminate\Database\QueryException;
use Illuminate\Support\Arr;
use Illuminate\Support\LazyCollection;

use function Safe\fclose;
use function Safe\fgetcsv;
use function Safe\fopen;
use function Safe\json_encode;

class ImportData3 extends BaseImportData
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everyuru:import3 {csvfile} {--hogar_id=} {--offset=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update data from CSV';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $csvFile = $this->argument('csvfile');
        if (!is_string($csvFile)) {
            $this->error('Only one file at at time');
            return -1;
        }

        $count = 145167;
        $this->output->progressStart($count);
        LazyCollection::make(function () use ($csvFile) {
            $handle = fopen($csvFile, 'r');

            while ($line = fgetcsv($handle)) {
                yield $line;
            }
            fclose($handle);
            //
        })->each(function (?array $line, int $index) {
            if (!is_array($line)) {
                return;
            }

            $this->output->progressAdvance();
            if ($index === 0) {
                $this->headers = array_flip($line);
                return;
            }

            if ($index < intval($this->option('offset'))) {
                return;
            }

            if (!empty($this->option('hogar_id')) && $this->getData($line, 'numero') != $this->option('hogar_id')) {
                return;
            }

            try {
                Uruguayan::where('hogar_id', Arr::get($line, $this->headers['numero']))
                    ->where('persona_id', Arr::get($line, $this->headers['nper']))
                    ->update([
                        // pre-escolar
                        'asistio_preescolar' => $this->getData($line, 'e193'),
                        'tipo_centro_preescolar' => $this->getData($line, 'e194'),
                        'recibe_comida_preescolar' => $this->getData($line, 'e196') === 'Sí',
                        'cantidad_comidas_semanales_desayuno_pre' => $this->getData($line, 'e196_1', true),
                        'cantidad_comidas_semanales_almuerzo_pre' => $this->getData($line, 'e196_2', true),
                        'cantidad_comidas_semanales_merienda_pre' => $this->getData($line, 'e196_3', true),

                        // primaria
                        'asistio_primaria' => $this->getData($line, 'e197'),
                        'finalizo_primaria' => $this->getData($line, 'e197_1') === 'Sí',
                        'años_aprobados_primaria' => $this->getData($line, 'e51_2', true),
                        'años_aprobados_primaria_especial' => $this->getData($line, 'e51_3', true),
                        'tipo_centro_primaria' => $this->getData($line, 'e198'),
                        'recibe_comida_primaria' => $this->getData($line, 'e200') === 'Sí',
                        'cantidad_comidas_semanales_desayuno_pri' => $this->getData($line, 'e200_1', true),
                        'cantidad_comidas_semanales_almuerzo_pri' => $this->getData($line, 'e200_2', true),
                        'cantidad_comidas_semanales_merienda_pri' => $this->getData($line, 'e200_3', true),

                        // MEDIA
                        'asistio_media' => $this->getData($line, 'e201'),
                        // 'finalizo_media' => $this->getData($line, 'e201_1') === 'Sí',
                        'razon_no_termino_media' => $this->getData($line, 'e202_7'),
                        'tipo_liceo' => $this->getData($line, 'e210_2'),
                        'años_aprobados_media_basico' => $this->getData($line, 'e51_4', true),
                        'años_aprobados_bachillerato' => $this->getData($line, 'e51_5', true),
                ]);
            } catch (QueryException $e) {
                $this->error('QueryException: ' . $e->getMessage());
                $this->error(json_encode($line));
                dd('error');
            }
        });

        $this->output->progressFinish();
        return 0;
    }
}
