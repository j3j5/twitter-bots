<?php

namespace App\Console\Commands\EveryUruguayan;

use App\DTO\Twitter\Tweet;
use App\Models\EveryUruguayan\Uruguayan;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use j3j5\TwitterApio;

class Post extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everyuru:post';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post the biography of one uruguayan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(TwitterApio $twitterApi)
    {
        $person = Uruguayan::whereNull('tweeted_at')->inRandomOrder()->firstOrFail();

        $tweetOptions = [
            'status' => $person->getTweet(),
            'lat' => $person->lat,
            'long' => $person->long,
            'display_coordinates' => true,
        ];

        $tweetResponse = $twitterApi->post('statuses/update', $tweetOptions);

        if (!is_array($tweetResponse)) {
            Log::error('Something went wrong when tweeting the bio', Arr::wrap($tweetResponse));
            $this->error('Something went wrong when tweeting the bio');
            return -1;
        }

        $tweet = new Tweet($tweetResponse);

        $person->tweet_id = $tweet->getStatusId();
        $person->user_id = $tweet->getAuthor()->getId();
        $person->username = $tweet->getAuthor()->getUsername();
        $person->tweeted_at = $tweet->getCreatedAt();
        $person->save();

        return 0;
    }
}
