<?php

namespace App\Console\Commands\EveryUruguayan;

use App\Models\EveryUruguayan\Uruguayan;
use Illuminate\Database\QueryException;
use Illuminate\Support\Arr;
use Illuminate\Support\LazyCollection;

use function Safe\fclose;
use function Safe\fgetcsv;
use function Safe\fopen;
use function Safe\json_encode;

class ImportData extends BaseImportData
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everyuru:import {csvfile}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import all data from CSV';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $csvFile = $this->argument('csvfile');
        if (!is_string($csvFile)) {
            $this->error('Only one file at at time');
            return -1;
        }

        // $count = LazyCollection::make(function () use ($csvFile) {
        //     $handle = fopen($csvFile, 'r');

        //     while (($line = fgetcsv($handle)) !== false) {
        //         yield $line;
        //     }
        //     fclose($handle);
        // })->count();

        $count = 145167;
        $this->output->progressStart($count);
        LazyCollection::make(function () use ($csvFile) {
            $handle = fopen($csvFile, 'r');

            while ($line = fgetcsv($handle)) {
                yield $line;
            }
            fclose($handle);
            //
        })->each(function (?array $line, int $index) {
            if (!is_array($line)) {
                return;
            }

            $this->output->progressAdvance();
            if ($index === 0) {
                $this->headers = array_flip($line);
                return;
            }

            $tipoCentro = trim(Arr::get(
                $line,
                $this->headers['e194'],   // 3 años o más NA
                Arr::get($line, $this->headers['e50_cv']) // AL
            ), "0 \n\r\t\v\0");

            $actividad = trim(Arr::get(
                $line,
                $this->headers['f124_1'],
                Arr::get(
                    $line,
                    $this->headers['f124_2'],
                    Arr::get(
                        $line,
                        $this->headers['f124_3'],
                        Arr::get(
                            $line,
                            $this->headers['f124_4'],
                            Arr::get($line, $this->headers['f124_5'])
                        )
                    )
                )
            ), "0 \n\r\t\v\0");
            $beca = null;
            $beca1 = trim(Arr::get($line, $this->headers['e562']), "0 \n\r\t\v\0");
            $beca2 = trim(Arr::get($line, $this->headers['e562_cv']), "0 \n\r\t\v\0");
            if (!in_array($beca1, ['0', 'No recibe', 'Sin dato'])) {
                $beca = str_replace('Sí, ', '', $beca1);
            } elseif (!empty($beca2) && !in_array($beca2, ['0', 'No recibe', 'Sin dato'])) {
                $beca = str_replace('Sí, ', '', $beca2);
            }

            try {
                Uruguayan::updateOrcreate([
                    'hogar_id' => Arr::get($line, $this->headers['numero']),
                    'persona_id' => Arr::get($line, $this->headers['nper']),
                ], [
                // Uruguayan::create([
                    // 'hogar_id' => Arr::get($line, $this->headers['numero']),
                    // 'persona_id' => Arr::get($line, $this->headers['nper']),
                    'identificacion' => $this->getData($line, 'e557'),
                    'departamento' => $this->getData($line, 'dpto'),
                    'barrio' => $this->getData($line, 'nombarrio'),
                    'personas_vivienda' => $this->getData($line, 'ht19'),
                    'region' => $this->getData($line, 'region_4'),

                    // Estudios
                    'estudia' => trim(Arr::get($line, $this->headers['e49_cv'])) === 'Sí',
                    'tipo_centro' => $tipoCentro,
                    'beca' => !empty($beca),
                    'tipo_beca' => $beca,
                    'alfabetización' => $this->getData($line, 'e48'),
                    'universidad' => $this->getData($line, 'e218'),
                    'termino_universidad' => $this->getData($line, 'e218_1') === 'Sí',

                    'actividad' => $actividad,

                    // Identidad
                    // 'estrato' => trim(Arr::get($line, $this->headers[]])-, "0 \n\r\t\v\0")>
                    'ascendencia' => $this->getData($line, 'e29_6'),
                    'identidad_genero' => $this->getData($line, 'e563'),
                    'edad' => $this->getData($line, 'e27', true),
                    'sexo' => $this->getData($line, 'e26') === '1' ? 'Hombre' : 'Mujer',

                    // Economía
                    'ingresos' => $this->getData($line, 'pt4'),
                    'pobre' => $this->getData($line, 'pobre_06') === 'Pobre',
                    'indigente' => $this->getData($line, 'indigente_06') === 'Indigente',
                    'condicion_actividad' => $this->getData($line, 'pobpcoac'),
                    'razon_dejar_trabajo' => $this->getData($line, 'f122'),
                    // f71 y f72, but need to decode the codes for CIUU and CIUO_08
                    // 'ocupacion' => $this->getData($line, ),

                    'jornada_laboral' => $this->getData($line, 'f85'),

                    // Salud
                    'insti_salud' => $this->getData($line, 'e45_cv'),
                    'acceso_insti_salud' => $this->getAccesoInstiSalud($line),
                    'emergencia' => $this->getData($line, 'e46_cv') === 'Sí',
                    'acceso_emergencia' => $this->getData($line, 'e47_cv'),

                    // Jubilación
                    'aporta_caja' => $this->getData($line, 'f82') === 'Sí',
                    'caja_aportes' => $this->getData($line, 'f83'),
                ]);
            } catch (QueryException $e) {
                $this->error('QueryException: ' . $e->getMessage());
                $this->error(json_encode($line));
                dd('error');
            }
        });

        $this->output->progressFinish();
        return 0;
    }

    private function getAccesoInstiSalud(array $line) : ?string
    {
        $fields = [
            'e45_1_1_cv',
            // 'e45_1_1_1_cv',
            'e45_2_1_cv',
            // 'e45_2_1_1_cv',
            'e45_3_1_cv',
            // 'e45_3_1_1_cv',
            'e45_4_1_cv',
            // 'e45_4_1_1_cv'
        ];
        foreach ($fields as $field) {
            if ($acceso = $this->getData($line, $field)) {
                return $acceso;
            }
        }
        return null;
    }
}
