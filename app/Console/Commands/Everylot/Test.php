<?php

namespace App\Console\Commands\Everylot;

use App\Models\Lot;
use App\Models\Nomenclator;
use App\Services\PolygonHelper;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use proj4php\Point;
use proj4php\Proj;
use proj4php\Proj4php;
use function Safe\file_get_contents;
use Shapefile\Shapefile;
use Shapefile\ShapefileException;

use Shapefile\ShapefileReader;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everylot:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $lot = Lot::where('nombre_via', 'like', 'Avenida Ocho De Octubre')->inRandomOrder()->first();
        $nomenclator = Nomenclator::findStreetOnNomenclator($lot->nombre_via);
        dd($nomenclator);

        try {
            // Open Shapefile
            $Shapefile = new ShapefileReader(resource_path('shapes/ine_barrios_mvd/ine_barrios_mvd.shp'));

            // Read all the records
            while ($Geometry = $Shapefile->fetchRecord()) {
                // Skip the record if marked as "deleted"
                if ($Geometry->isDeleted()) {
                    continue;
                }

                dump($Geometry->getDataArray());
                $shapeProjection = file_get_contents(resource_path('shapes/ine_barrios_mvd/ine_barrios_mvd.prj'));
                $proj4 = new Proj4php();
                // Your initial projection information from your .prj file
                $srcProj = new Proj($shapeProjection, $proj4);
                // WGS84 (for google map)
                $dstProj = new Proj('EPSG:4326', $proj4);

                $polygonHood = collect(Arr::get($Geometry->getArray(), 'rings.0.points'))
                    ->map(function (array $points) use ($proj4, $srcProj, $dstProj) {
                        // Your original point
                        $srcPoint = new Point($points['x'], $points['y'], $srcProj);

                        // New point in WGS84 projection for Google Maps
                        return $proj4->transform($dstProj, $srcPoint);
                        // $lat = $dstPoint->y;
                        // $long = $dstPoint->x;
                    });

                // Pza Zabala -34.90766385235194, -56.20813859118186
                // $test = new Point(-56.20813859118186, -34.90766385235194, $dstProj);
                // Intendencia -34.90599574460787, -56.186094050674996
                $test = new Point(-56.186094050674996, -34.90599574460787, $dstProj);

                $inPolygon = app(PolygonHelper::class)->pointInPolygon($test, $polygonHood->toArray());
                dd($inPolygon);
                // Print Geometry as WKT
                // print_r($Geometry->getWKT());

                // Print Geometry as GeoJSON
                // print_r($Geometry->getGeoJSON());

                // Print DBF data
            }
        } catch (ShapefileException $e) {
            // Print detailed error information
            echo 'Error Type: ' . $e->getErrorType()
                . "\nMessage: " . $e->getMessage()
                . "\nDetails: " . $e->getDetails();
        }
        return 0;
    }
}
