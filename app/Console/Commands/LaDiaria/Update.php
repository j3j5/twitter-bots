<?php

namespace App\Console\Commands\LaDiaria;

use App\Services\FediApi;
use Carbon\Carbon;
use DiDom\Document;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use function Safe\preg_replace;

use SimpleXMLElement;

class Update extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ladiaria:update {--dry-run}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the articles from La Diaria\'s RSS feed';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(FediApi $api)
    {
        $publishedFile = '/LaDiaria/published';
        $published = explode(PHP_EOL, (string) Storage::get($publishedFile));

        $feedUrl = 'https://ladiaria.com.uy/feeds/articulos';
        $response = Http::get($feedUrl);
        $feedXml = new SimpleXMLElement($response->throw()->body());
        $feed = json_decode(json_encode($feedXml->channel->children()), true);
        // Regex to clean up markdown links
        $mdLinksPattern = '/\[.*\]\(https:\/\/.*?(\)|\.{3})/';
        $items = array_reverse($feed['item']);
        foreach ($items as $item) {
            // Check if already published
            if (in_array((string) $item['guid'], $published)) {
                Log::debug('Ignoring ' . $item['guid'] . '; Already published');
                continue;
            }

            // Check in case it's too old
            $pubDate = Carbon::parse($item['pubDate'])->setTimezone('America/Montevideo');
            if (!$pubDate instanceof Carbon || !$pubDate->isSameDay(now('America/Montevideo'))) {
                Log::debug('Ignoring ' . $item['guid'] . '; Too old to publish: ' . $pubDate . ' | ' . now());
                continue;
            }

            // Publish
            $message = "<p><a href=\"{$item['link']}\">{$item['title']}</a>";
            if (in_array((string) $item['category'], ['Humor'])) {
                // Add #category hashtag
                $message .= '<br>#' . (string) $item['category'] . '<br>';
            }
            $message .= '</p>';
            $description = str_replace(['<br/>', '<br>'], '', (string) $item['description']);
            $message .= preg_replace($mdLinksPattern, "...\n", $description);

            $article = Http::get($item['link']);
            $document = new Document($article->throw()->body());

            $options = [];
            $src = $document->first('figure.principal source');
            if ($src) {
                $mediaUrl = $src->getAttribute('srcset');
                $options = ['media' => [[
                    'mediaType' => 'image/jpeg',
                    'url' => 'https://ladiaria.com.uy' . $mediaUrl,
                ]]];
            }
            $pic = $document->first('.ld-article__main-photo');
            if ($pic) {
                $alt = $pic->getAttribute('alt');
                $options['media'][0]['name'] = $alt;
            }

            $sectionRegex = '/ladiaria\.com\.uy\/(?<seccion>[\w-]+)\//';
            $tags = [];
            if (preg_match($sectionRegex, $item['link'], $matches)) {
                $tags[] = ucfirst(Str::camel($matches['seccion']));
            }
            $sections = $document->first('.article-date-author > .small > a');
            if (!empty($sections)) {
                foreach ($sections as $a) {
                    if (preg_match($sectionRegex, (string) $a->href, $matches)) {
                        $tags[] = ucfirst(Str::camel($matches['seccion']));
                    }
                }
            } else {
                // paywall
                $sections = $document->find('.article-container__article-card__section > span');
                foreach ($sections as $section) {
                    $text = Arr::last(explode(separator: '›', string: $section->text()));
                    $tags[] = ucfirst(Str::camel(Str::slug($text)));
                }
            }
            $tags = collect($tags)->unique()->reject(fn($tag) => in_array($tag, ['Humor']));
            if ($tags->isNotEmpty()) {
                $message .= '<p>';
                $message .= $tags->map(fn(string $tag) => '#' . $tag)->join(' ') . '</p>';
            }
            $message = trim($message);

            if ($this->option('dry-run')) {
                $this->info(str_replace(['<br>', '<br/>'], PHP_EOL, $message));
                if (isset($options['media'])) {
                    $this->line('https://ladiaria.com.uy' . $mediaUrl);
                }
                if (!$this->confirm('Continue?')) {
                    exit;
                }
            } else {
                $api->publishPost($message, $options);
                Storage::disk('local')->append($publishedFile, $item['guid']);
            }
        }

        return Command::SUCCESS;
    }
}
