<?php

namespace App\Console\Commands\LaDiaria;

use App\Services\FediApi;
use DiDom\Document;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Pluralizer;
use Illuminate\Support\Str;
use RuntimeException;

class Covers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ladiaria:covers {--dry-run}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private string $tz;

    /**
     * Execute the console command.
     */
    public function handle(FediApi $api) : void
    {
        Pluralizer::useLanguage('spanish');
        $this->tz = 'America/Montevideo';

        // Get Today' s cover!!
        $coversUrl = 'https://papel.ladiaria.com.uy/library';
        $response = Http::get($coversUrl);
        $document = new Document((string) $response);

        $currentMonthIssues = $document->first('.Library-containerIssues');
        $latestCover = $currentMonthIssues->first('article')->first('.Issue-cover > img');
        $src = $latestCover->src ?? $latestCover->{'data-src'};
        $alt = $latestCover->alt;

        $date = $this->getDateFromAlt($alt);

        if (!$date->startOfDay()->isSameDay(now($this->tz)->startOfDay())) {
            $msg = 'There does not seem to be and issue for today yet; latest issue is ' . $alt . ' and today is ';
            $msg .= $date->format('d.m.Y');
            $this->info($msg);
            return;
        }

        $message = '<p>La portada de hoy (' . $date->locale('es_UY')->isoFormat('dddd D MMMM YYYY') . ') de <a href="https://ladiaria.com.uy">la diaria</a>.</p>';
        $options = ['media' => [[
            'mediaType' => 'image/jpeg',
            'url' => $src,
            'name' => 'Portada del ' . $date->locale('es_UY')->isoFormat('dddd D MMMM YYYY') . ' del diario uruguayo La Diaria',
        ]]];

        if ($this->option('dry-run')) {
            $this->info($message);
            return;
        }

        $response = $api->publishPost($message, $options);
        $lastMsgId = $response->json('id');
        if($this->output->isVerbose()) {
            $this->info($message);
            $this->info('Request:');
            $this->line(json_encode($options, JSON_PRETTY_PRINT));
            $this->info('Response:');
            $this->line($response->body());
            $this->line($response->status());
            $this->line($response->reason());
        }

        $covers = $this->findCoversForPreviousYears();
        foreach ($covers as $year => $cover) {
            $options = [
                'media' => [[
                    'mediaType' => 'image/jpeg',
                    'url' => $cover['src'],
                    'name' => 'Portada del ' . $cover['date']->locale('es_UY')->isoFormat('dddd D MMMM YYYY') . ' del diario uruguayo La Diaria',
                ]],
                'in_reply_to_id' => $lastMsgId,
            ];
            $yearsDiff = now($this->tz)->year - $year;
            $message = "<p>Hace $yearsDiff " . Str::plural('año', $yearsDiff) .
                ' (el ' . $cover['date']->locale('es_UY')->isoFormat('dddd D MMMM YYYY') .
                '), la portada de <a href="https://ladiaria.com.uy">la diaria</a> fue esta:</p>';
            $message .= "<p>#laDiariaNoticiasDeAyer #año{$year}</p>";
            $response = $api->publishPost($message, $options);
            if($this->output->isVerbose()) {
                $this->info($message);
                $this->info('Request:');
                $this->line(json_encode($options, JSON_PRETTY_PRINT));
                $this->info('Response:');
                $this->line($response->body());
                $this->line($response->status());
                $this->line($response->reason());
                $this->line('Next id: ' . $response->json('id'));
            }
            $lastMsgId = $response->json('id');
        }
    }


    private function findCoversForPreviousYears() : array
    {
        $covers = [];
        $years = array_reverse(range(2019, now()->subYear()->year));
        foreach ($years as $year) {
            $coversUrl = 'https://papel.ladiaria.com.uy/library/filter';
            $filter = ['month' => $year. '-' . str_pad(now($this->tz)->month, 2, '0', STR_PAD_LEFT)];
            $response = Http::get($coversUrl, $filter);
            $document = new Document($response->body());

            $currentMonthIssues = $document->first('.Library-containerIssues');
            $issues = $currentMonthIssues->find('article');
            foreach($issues as $issue) {
                $cover = $issue->first('.Issue-cover > img');
                $alt = $cover->alt;
                try {
                    $date = $this->getDateFromAlt($alt);
                } catch(RuntimeException) {
                    continue;
                }

                if ($date->day === now($this->tz)->day) {
                    $covers[$year] = [
                        'src' => $cover->src ?? $cover->{'data-src'},
                        'alt' => $alt,
                        'date' => $date,
                    ];
                    break;
                }
            }
        }

        return $covers;
    }

    private function getDateFromAlt(string $alt) : Carbon
    {
        if (!preg_match('/\d{2}\.\d{2}\.\d{4}/', $alt, $matches)) {
            Log::debug('No date was found on alt: ' . $alt);
            throw new RuntimeException('No date was found on alt: ' . $alt);
        }

        return Carbon::createFromFormat('d.m.Y', $matches[0], $this->tz);
    }
}
