<?php

namespace App\Console\Commands\EveryFlight;

use App\Models\EveryFlight\FlightStatus;
use Illuminate\Console\Command;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everyflight:test {--all}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $hidden = true;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $pendingStatuses = FlightStatus::query();
        if (!$this->option('all')) {
            $pendingStatuses->whereNull('tweeted_at');
        }
        $pendingStatuses = $pendingStatuses->oldest()
            ->get();

        $pendingStatuses->each(function ($flightStatus) {
            $tweetOptions = [
                'status' => $flightStatus->getTweet(),
            ];

            if (
                $flightStatus->siblings->isNotEmpty() &&
                !empty($flightStatus->siblings->first()->tweet_id)
            ) {
                $tweetOptions['in_reply_to_status_id'] = $flightStatus->siblings->first()->tweet_id;
            } elseif ($flightStatus->siblings->isNotEmpty()) {
                $this->line('It has siblings, but they\'ve got no tweet id');
            }

            dump($tweetOptions);
        });

        return 0;
    }
}
