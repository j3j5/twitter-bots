<?php

namespace App\Console\Commands\Misc;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use j3j5\TwitterApio;

class TruncateTimeline extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twitter:truncate {serviceAccount} {--username=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'DELETE ALL TWEETS from the timeline of the logged in user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(TwitterApio $twitterApi)
    {
        $account = $this->argument('serviceAccount');
        if (!is_string($account)) {
            $this->error('Invalid account');
            return -1;
        }
        $config = config('services.twitter.' . $account);
        $twitterApi->reconfigure($config);

        foreach ($twitterApi->getTimeline('statuses/user_timeline', ['screen_name' => $this->option('username'), 'count' => 200]) as $page) {
            $this->output->progressStart(count($page));
            foreach (Arr::wrap($page) as $tweet) {
                $twitterApi->post('statuses/destroy', ['id' => $tweet['id_str']]);
                $this->output->progressAdvance();
            }
            $this->output->progressFinish();
        }
        return 0;
    }
}
