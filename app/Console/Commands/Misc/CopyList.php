<?php

namespace App\Console\Commands\Misc;

use Illuminate\Console\Command;
use j3j5\TwitterApio;

class CopyList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add-list-members {serviceAccount} {--from=} {--to=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add all members of a list to another one';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(TwitterApio $api)
    {
        $account = $this->argument('serviceAccount');
        if (!is_string($account)) {
            $this->error('Invalid account');
            return -1;
        }
        $config = config('services.twitter.' . $account);
        $api->reconfigure($config);

        $params = [
            'list_id' => $this->option('from'),
            'skip_status' => true,
            'count' => 5000,    // max
        ];
        $response = $api->get('lists/members', $params);
        if (!isset($response['users'])) {
            $this->error('Empty response');
            return -1;
        }
        $members = collect($response['users'])->map(function (array $user) {
            return $user['id_str'];
        });

        foreach ($members->chunk(100) as $chunk) {
            $data = [
                'list_id' => $this->option('to'),
                'user_id' => $chunk->implode(','),
            ];
            $api->post('lists/members/create_all', $data);
        }

        return 0;
    }
}
