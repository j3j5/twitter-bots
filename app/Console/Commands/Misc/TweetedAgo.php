<?php

namespace App\Console\Commands\Misc;

use App\DTO\Twitter\User;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use j3j5\TwitterApio;

class TweetedAgo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twitter:tweeted-ago {user} {--hashtag=} {--start_date=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(TwitterApio $api)
    {
        Log::debug('Tweeting for @' . $this->argument('user'));
        $response = $api->getUser($this->argument('user'));

        if (!$response) {
            $this->error('Empty response');
            return -1;
        }
        if ($response['code'] === 403 && $this->option('start_date')) {
            $errors = json_decode($response['errors'], true);
            if (collect($errors['errors'])->firstWhere('code', 63)) {
                $daysAgo = now()->diffInDays(Carbon::parse($this->option('start_date')));
                $status = 'Hace ' . $daysAgo . ' ' .
                    Str::plural('día', $daysAgo) . ' que @' . $this->argument('user') . ' está '
                    . Arr::random([
                        'suspendido',
                        'en cana twittera',
                        'privado de libertad para twittear',
                        'obligado a laburar en lugar de perder el tiempo por aquí',
                        'en penitencia twittera',
                        'sin cuenta en twitter (no lo aguantan ni en su casa)',
                        'aburrido laburando',
                        'acumulando memes que solo comparte en whatsapp',
                        'informándose solo a través de la TV',
                        'suspendido (como será que hasta instaló la app de Antel para ver los canales abiertos en el celu)',
                    ])
                    . ' ' . Arr::random(['😟', '😱', '🥺', '🤨', '🧐'])
                    . PHP_EOL . PHP_EOL;
                if ($this->option('hashtag')) {
                    $status .= '#' . $this->option('hashtag');
                }

                // $this->info($status);
                $response = $api->post('statuses/update', ['status' => $status]);
                Log::debug('Response: ', Arr::wrap($response));

                return 0;
            }
        }

        $user = new User($response);
        if (!$user->status) {
            $this->error('No status');
            dump($user);
            return -1;
        }

        $tweet = $user->getLastTweet();

        $daysAgo = $tweet->getCreatedAt()->diffInDays();

        if ($daysAgo > 0) {
            $status = 'Hace ' . $daysAgo .
            ' días que @' . $this->argument('user') . ' no twittea '
            . Arr::random(['😟', '😱', '🥺', '🤨', '🧐'])
            . PHP_EOL . PHP_EOL;
            if ($this->option('hashtag')) {
                $status .= '#' . $this->option('hashtag');
            }
        } else {
            $emojis = ['🤩', '🥳', '🥰', '🙌', '🤟', '🚀', ''];
            $status = '¡DÍA GRANDE! ' . Arr::random($emojis) . ' Twitteó @' . $this->argument('user');
        }

        // $this->info($status);
        $response = $api->post('statuses/update', ['status' => $status]);
        Log::debug('Response: ', Arr::wrap($response));

        return 0;
    }
}
