<?php

namespace App\Console\Commands\Misc;

use Atymic\Twitter\ApiV1\Contract\Twitter as TwitterV1;
use Atymic\Twitter\Contract\Querier as TwitterV2;
use Atymic\Twitter\Twitter;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;

use function Safe\json_decode;

class WebMonitor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'web:up {url} {--start-date=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks whether a given URL returns a 200 response';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(TwitterV1 $twitterApiV1, TwitterV2 $twitterApiV2)
    {
        $lastTweetsJson = $twitterApiV2->get(
            sprintf('users/%s/tweets', config('services.twitter.webmonitor.bot_user_id')),
            [
                Twitter::KEY_RESPONSE_FORMAT => Twitter::RESPONSE_FORMAT_JSON,
                'max_results' => 5,
                'expansions' => 'author_id',
                'tweet.fields' => 'created_at,text',
                // 'user.fields' => '',
                'exclude' => 'retweets,replies',
            ]
        );
        $lastTweets = json_decode($lastTweetsJson, true);

        $lastTweet = Arr::get($lastTweets, 'data.0.text');
        $lastTweetCreatedAt = Carbon::parse(Arr::get($lastTweets, 'data.0.created_at'));

        $lastStatus = mb_substr($lastTweet, 0, 1);

        $url = (string) $this->argument('url');

        $response = Http::head($url);

        $status = '';
        if ($response->ok() && $lastStatus === '🔴') {
            $status = '🟢 ¡SÍ! ¡Volvió!' . PHP_EOL . "La web $url parece estar funcionando (" . $response->status() . ') '
                . Arr::random(['😀', '😃', '😄', '😁', '😆', '😅', '🤟', '🤘', '👏', '🙌']);
        } elseif ($response->failed() && ($lastStatus === '🟢' || $lastTweetCreatedAt->diffInHours(now()) >= 1)) {
            $status = '🔴 NO' . PHP_EOL . "La web $url está fallando con un código " . $response->status() . ' '
                . Arr::random(['😱', '😨', '😰', '😥', '😓', '😭', '😤', '😠', '😡', '🤬']);
        }

        if (empty($status)) {
            return 0;
        }

        $response = $twitterApiV1->postTweet(['status' => $status, 'response_format' => 'json']);

        return 0;
    }
}
