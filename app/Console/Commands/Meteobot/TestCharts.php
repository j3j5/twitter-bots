<?php

namespace App\Console\Commands\Meteobot;

use App\Services\Meteobot\Graphs;
use Illuminate\Console\Command;

use function Safe\date;
use function Safe\file_put_contents;

class TestCharts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'meteo:test-charts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $hidden = true;

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Graphs $graphs)
    {
        $stations = $graphs->generateGraphs();
        foreach ($stations as $station => $charts) {
            foreach ($charts as $name => $chart) {
                file_put_contents(storage_path('app/meteobot/' . $station . '-' . $name . '-' . date('Y-M-d H:i:s') . '.png'), $chart);
            }
        }
        return 0;
    }
}
