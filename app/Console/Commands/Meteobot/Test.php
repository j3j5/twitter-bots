<?php

namespace App\Console\Commands\Meteobot;

use App\Services\Meteobot\Graphs;
use Illuminate\Console\Command;
use j3j5\TwitterApio;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'meteo:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $hidden = true;

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Graphs $graphs, TwitterApio $twitterApi)
    {
        $graphs->generateGraphs();
        return 0;
    }
}
