<?php

namespace App\Console\Commands\Meteobot;

use App\Services\FediApi;
use App\Services\Meteobot\AmbienteUY;
use App\Services\Meteobot\Graphs;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use RuntimeException;

/**
 * @phpstan-type Station array{id: int, name: string, location: array{lat: numeric-string, long: numeric-string}, department: string}
 */
class Post extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'meteo:post {--date=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post the graphs for the MeteoBot';

    private Graphs $graphs;
    private Collection $stationsData;
    private Collection $data;
    private string $filePrefix = 'meteobot';
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Graphs $graphs, FediApi $twitterApi)
    {
        $this->graphs = $graphs;

        if (!empty($this->option('date'))) {
            $date = Carbon::parse($this->option('date'), 'America/Montevideo');
            $this->graphs->setDate($date);
        }

        $this->stationsData = collect($this->graphs->getAmbienteService()->getStations());
        $stations = $graphs->generateGraphs();
        $this->data = $graphs->getStoredData();
        $this->output->progressStart(count($stations));
        foreach ($stations as $station => $charts) {
            /**
             * @phpstan-var Station $stationData
             * @var array $stationData
             */
            $stationData = $this->stationsData->firstWhere('name', $station);
            if (!is_array(($stationData))) {
                throw new RuntimeException("No station data found for $station");
            }
            $date = Carbon::parse($this->data->get($stationData['id'])->last()->last()->get('Fecha'));
            $humidity = $this->data->get($stationData['id'])->last()->pluck(AmbienteUY::HUMEDAD);
            $temperatures = $this->data->get($stationData['id'])
                ->mapWithKeys(fn (Collection $row, string $key) : array => [
                        $key => $row->pluck(AmbienteUY::TEMP),
                ]);
            $wind = $this->data->get($stationData['id'])->last()
                ->mapWithKeys(fn (Collection $row, string $key) : array => [
                    $key => [$row->get(AmbienteUY::VIENTO_DIR), $row->get(AmbienteUY::VIENTO_INT)],
                ]);
            $status = $stationData['name'] . ' #' . str_replace(' ', '', $stationData['department']) . PHP_EOL;
            $status .= '📅 ' . ucfirst($date->locale('es_ES')->isoFormat('dddd D [de] MMMM YYYY')) . PHP_EOL;
            $status .= $this->getTempLine($temperatures) . PHP_EOL;
            $status .= $this->getHumidityLine($humidity) . PHP_EOL;
            $status .= $this->getWindLine($wind) . PHP_EOL;
            $media = [];
            foreach ($charts as $chartName => $chart) {
                // file_put_contents(storage_path('app/meteobot/' . $stationData['name']. '-' . $chartname . '-' . date('Y-M-d H:i:s') . '.png'), $chart);
                // $mediaResponse = $twitterApi->uploadMedia([
                //     'media' => $chart,
                // ]);
                // Upload the graph
                $filepath = $this->filePrefix . $stationData['name'] . '-' . $chartName . '-' . date('Y-M-d') . '.png';
                $uuid = Storage::disk('uploadcare')->putGetUuid($filepath, $chart);
                if (empty($uuid)) {
                    throw new RuntimeException('Error while uploading the chart ' . $chartName);
                }

                $altText = $this->getAltText($chartName, $stationData, $date);
                $media[] = [
                    'mediaType' => 'image/png',
                    'url' => config('filesystems.disks.uploadcare.cdn') . '/' . $uuid . '/' ,
                    'name' => $altText,
                ];
            }
            $options = [
                // 'lat' => $stationData['location']['lat'],
                // 'long' => $stationData['location']['long'],
                // 'display_coordinates' => true,
                'media' => $media,
            ];
            $twitterApi->publishPost($status, $options);
            $this->output->progressAdvance();
            sleep(1);
        }
        $this->output->progressFinish();

        return 0;
    }

    private function getTempLine(Collection $temp) : string
    {
        $line = '🌡️ ' . round($temp->last()->min()) . 'º/' . round($temp->last()->max()) . 'º - ' . round($temp->last()->filter()->avg(), 1) . 'º de media';

        // $minMaxSeries = $temp->mapWithKeys(fn(Collection $series, string $dateStr) => [$dateStr => [$series->min(), $series->max()]]);
        // $min = $minMaxSeries->map(fn(array $minMax) => $minMax[0])->sort()->first();
        // $max = $minMaxSeries->map(fn(array $minMax) => $minMax[1])->sort()->last();

        // $minDate = $minMaxSeries->where(0, '=' , $min)->keys()->first();
        // $maxDate = $minMaxSeries->where(1, '=' , $max)->keys()->first();
        // $latestDate = $temp->keys()->last();
        // if ($minDate === $maxDate && $minDate === $latestDate) {
        //     $line .= '(mínima más baja y máxima más alta en los últimos ' . $temp->keys()->count() - 1 . ' años)';
        // } elseif ($minDate === $latestDate) {
        //     $line .= ' (la mínima más baja en los últimos ' . $temp->keys()->count() - 1 . ' años)';

        // } elseif ($maxDate === $latestDate) {
        //     $line .= ' (la máxima más alta en los últimos ' . $temp->keys()->count() - 1 . ' años)';
        // }

        return $line;
    }

    private function getHumidityLine(Collection $humidity) : string
    {
        $line = '💧 ' . round((int) $humidity->avg()) . '% de humedad relativa (media) durante el día';

        return $line;
    }

    private function getWindLine(Collection $wind) : string
    {
        // 0 direction, 1 speed
        $line = '🌬️ ';
        $wind = $wind->map(
            fn ($row) => is_numeric($row[0])
            ? [$this->graphs->findClosestAngle((float) $row[0]), $row[1]]
            : [$row[0], $row[1]]
        );
        $avg = $wind->pluck(1)->filter()->avg();
        $max = $wind->pluck(1)->max();
        $mainDir = $this->graphs->findClosestAngle((float) $wind->pluck(0)->filter()->median());

        if ($avg <= 1) {
            $line .= 'Mayormente sin viento';
        } elseif ($avg < 4) {
            $line .= 'Viento bajo';
            if ($mainDir) {
                $line .= ', principalmente del ' . $this->readableWindDirection($mainDir);
            }
        } else {
            if ($mainDir) {
                $line .= 'Viento principalmente del ' . $this->readableWindDirection($mainDir) . ',';
            }
            $line .= ' "' . $this->readableWindSpeed($avg) . '" en la escala de Beaufort';
            if ($this->readableWindSpeed($avg) !== $this->readableWindSpeed($max)) {
                $line .= ' llegando a "' . $this->readableWindSpeed($max) . '"';
            }
        }
        return trim($line);
    }

    private function readableWindDirection(float|int|string|null $angle) : string
    {
        return match ((int) $angle) {
            0 => 'norte', // ⬆️',
            45 => 'noreste', // ↗️',
            90 => 'este', // ➡️',
            135 => 'sureste', // ↘️',
            180 => 'sur', // ⬇️',
            225 => 'suroeste', // ↙️',
            270 => 'oeste', // ⬅️',
            315 => 'noroeste', // ↖️',
            default => '',
        };
    }

    /** See Beaufort scale https://es.wikipedia.org/wiki/Escala_de_Beaufort */
    private function readableWindSpeed(int|float|string $speed) : string
    {
        $speed = round((float) $speed);
        if ($speed <= 1) {
            return 'Calma';
        } elseif ($speed > 1 && $speed < 4) {
            return 'Ventolina';
        } elseif ($speed >= 5 && $speed < 7) {
            return 'Flojito (Brisa muy débil)';
        } elseif ($speed >= 7 && $speed < 11) {
            return 'Flojo (Brisa Ligera)';
        } elseif ($speed >= 11 && $speed < 17) {
            return 'Bonancible (Brisa moderada)';
        } elseif ($speed >= 17 && $speed < 22) {
            return 'Fresquito (Brisa fresca)';
        } elseif ($speed >= 22 && $speed < 28) {
            return 'Fresco (Brisa fuerte)';
        } elseif ($speed >= 28 && $speed < 34) {
            return 'Frescachón (Viento fuerte)';
        } elseif ($speed >= 34 && $speed < 41) {
            return 'Temporal (Viento duro)';
        } elseif ($speed >= 41 && $speed < 48) {
            return 'Temporal fuerte (Muy duro)';
        } elseif ($speed >= 48 && $speed < 56) {
            return 'Temporal duro (Temporal)';
        } elseif ($speed >= 56 && $speed < 63) {
            return 'Temporal muy duro (Borrasca)';
        } elseif ($speed >= 64) {
            return 'Temporal huracanado (Huracán)';
        }
        return '';
        // 1 	Calma
        // 1 a 3 	Ventolina
        // 4 a 6 	Flojito (Brisa muy débil)
        // 7 a 10 	Flojo (Brisa Ligera)
        // 11 a 16 	Bonancible (Brisa moderada)
        // 17 a 21 	Fresquito (Brisa fresca)
        // 22 a 27 	Fresco (Brisa fuerte)
        // 28 a 33 	Frescachón (Viento fuerte)
        // 34 a 40 	Temporal (Viento duro)
        // 41 a 47 	Temporal fuerte (Muy duro)
        // 48 a 55 	Temporal duro (Temporal)
        // 56 a 63 	Temporal muy duro (Borrasca)
        // +64 	Temporal huracanado (Huracán)
    }

    private function getAltText(string $chartName, array $stationData, Carbon $date) : string
    {
        return match ($chartName) {
            'humidity' => "Un gráfico de línea mostrando el nivel de humedad relativa en la {$stationData['name']} de {$stationData['department']} el " . $date->locale('es_ES')->isoFormat('dddd D [de] MMMM YYYY') . '.',
            'pressure' => "Un gráfico de línea mostrando el nivel de presión atmosférica en la {$stationData['name']} de {$stationData['department']} el " . $date->locale('es_ES')->isoFormat('dddd D [de] MMMM YYYY') . '.',
            'temperature' => 'Un gráfico de línea mostrando la evolución de la temperatura el ' . $date->locale('es_ES')->isoFormat('dddd D [de] MMMM YYYY') . " en la {$stationData['name']} de {$stationData['department']}.",
            'wind' => "Un gráfico de línea mostrando la velocidad y dirección del viento en la {$stationData['name']} de {$stationData['department']} el " . $date->locale('es_ES')->isoFormat('dddd D [de] MMMM YYYY') . '.',
            default => '',
        };
    }
}
