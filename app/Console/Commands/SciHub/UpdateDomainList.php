<?php

namespace App\Console\Commands\SciHub;

use App\Services\FediApi;
use App\Services\UserAgent;
use Carbon\Carbon;
use DiDom\Document;
use DiDom\Element;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class UpdateDomainList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scihub:update-domain-list {--dry-run}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check the updated list of valid domains and post it if it changed';

    /**
     * Execute the console command.
     */
    public function handle(UserAgent $userAgent, FediApi $api)
    {
        $urls = [
            'https://sci-hub.se',
            'https://sci-hub.ru',
        ];
        $lastUpdatedFile = 'SciHub/last_updated';
        $lastMessageFile = 'SciHub/last_msg';

        $url = Arr::random($urls) . '/mirrors';

        $response = Http::withHeaders([
            'User-Agent' => $userAgent->get(),
        ])->get($url);

        if (!$response->successful()) {
            $response->throw();
        }

        try {
            $document = new Document($response->body());
            $links = $document->first('#mirrors')->find('a');
            // $links = $document->first('#mirrors');
        } catch (Exception $e) {
            $this->error($e->getMessage());
            exit(-1);
        }

        // $links = collect($links)->map(fn (Element $anchor) => 'https:' . $anchor->getAttribute('href'))
        //     ->filter(function (string $mirror) use ($userAgent) {
        //         $response = Http::withHeaders([
        //             'User-Agent' => $userAgent->get(),
        //         ])->head($mirror);

        //         dump($mirror, $response->status());
        //         return $response->ok();
        //     });

        $htmlLinks = collect($links)->map(function (Element $a) {
            $a->setAttribute('href', 'https:' . $a->getAttribute('href'));
            return $a->html();
        })->implode('<br>');

        $message = '<p>The latest Sci-Hub mirrors:<br><br>';
        $message .= $htmlLinks . '<br><br>';
        $message .= 'Retrieved from <a href="' . $url . '">' . $url . '</a><br><br>';
        $message .= 'Love Science, Love Sci-Hub!</p>';

        $lastPosted = Storage::disk('local')->get($lastUpdatedFile);
        $lastMessage = Storage::disk('local')->get($lastMessageFile);
        if ($lastMessage === $htmlLinks && Carbon::createFromTimestamp($lastPosted)->diffInHours() < 23) {
            if ($this->output->isVerbose()) {
                $this->line('List of links is the same, skipping posting');
                $this->line('Last post: ' . Carbon::createFromTimestamp($lastPosted)->toDateTimeString());
                $this->line('Diff in mins: ' . Carbon::createFromTimestamp($lastPosted)->diffInMinutes());
            }
            exit;
        }

        Storage::disk('local')->put($lastMessageFile, $htmlLinks);

        if ($this->output->isVerbose()) {
            $this->info($message);
        }
        if (!$this->option('dry-run')) {
            $api->publishPost($message);
            Storage::disk('local')->put($lastUpdatedFile, \time());
        }
    }
}
