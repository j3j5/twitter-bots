<?php

namespace App\Console\Commands\IndustrialProp;

use App\Jobs\IndProp\CreateNiceClassInfographic;
use App\Models\IndProp\Brand;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

use function Safe\file_put_contents;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'registra:test {--request=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $hidden = true;

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //     if (empty($this->option('request'))) {
        //         $brands = Brand::latest()->take(10)->get();
        //     } else {
        //         $brands = Brand::where('requestNumber', $this->option('request'))->get();
        //     }
        //     // $brand = Brand::where('requestNumber', 543128)->first();
        //     foreach ($brands as $brand) {
        //         $img = CreateNiceClassInfographic::dispatchSync($brand);
        //         $img->save(storage_path('app/' . $brand->requestNumber . '.png'));
        //         foreach ($brand->getTweets() as $tweet) {
        //             $this->info($tweet);
        //         }
        //     }
        //     return Command::SUCCESS;

        $brands = Brand::whereNotNull('logo')->whereNull('tweeted_at')->get();
        $brands->each(function ($brand) {
            $path = storage_path('app/miem/');
            $file = Str::slug($brand->brand . '-' . $brand->owner . '-' . $brand->id) . '.png';
            file_put_contents($path . $file, $brand->logoBinary);
        });
        return Command::SUCCESS;
    }
}
