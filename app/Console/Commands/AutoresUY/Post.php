<?php

namespace App\Console\Commands\AutoresUY;

use App\DTO\Twitter\Tweet;
use App\Models\Obra;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use j3j5\TwitterApio;
use RuntimeException;

use function Safe\file_get_contents;
use function Safe\unlink;

use Sentry;
use Symfony\Component\Console\Output\Output;
use Throwable;

class Post extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'autores:post';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Find a random work and tweet it';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(TwitterApio $twitterApi)
    {
        $work = null;
        $i = 0;
        do {
            $i++;
            $this->info('searching work', Output::VERBOSITY_VERBOSE);
            try {
                /** @var \App\Models\Obra $work */
                $work = Obra::notTweetedYet()->where('tipo_obra', Obra::VISUAL)->inRandomOrder()->firstOrFail();
                $work->getExtraFromSite();
            } catch (Throwable $e) {
                Log::warning($e->getMessage());
                Sentry::captureException($e);
                $work = null;
            }
            if ($i > 5) {
                throw new RuntimeException('There is no available work', 404);
            }
        } while ($work === null);
        $mediaResponse = null;
        /** @var \App\Models\Obra $work */
        if ($work->downloadAsset()) {
            // Upload pic
            $mediaResponse = $twitterApi->uploadMedia([
                'media' => file_get_contents($work->filename),
            ]);
            Log::debug('mediaREsponse', $mediaResponse);

            // Add alt text
            $twitterApi->addMetadata([
                'media_id' => $mediaResponse['media_id_string'],
                'alt_text' => [
                    'text' => "«{$work->titulo}» por {$work->autores}",
                ],
            ]);
        }

        $tweetOptions = [
            'status' => $work->getTweet(),
            // Playita
            'lat' => '-34.89791109197715',
            'long' => '-56.117469015922474',
            'display_coordinates' => true,
        ];
        if (isset($mediaResponse['media_id_string'])) {
            $tweetOptions['media_ids'] = $mediaResponse['media_id_string'];
        }
        Log::debug('tweeting with ', $tweetOptions);
        $tweetResponse = $twitterApi->post('statuses/update', $tweetOptions);

        if (!is_array($tweetResponse)) {
            Log::error('Something went wrong when tweeting the image', Arr::wrap($tweetResponse));
            $this->error('Something went wrong when tweeting the image');
            return -1;
        }

        $tweet = new Tweet($tweetResponse);

        $work->tweet_id = $tweet->getStatusId();
        $work->user_id = $tweet->getAuthor()->getId();
        $work->username = $tweet->getAuthor()->getUsername();
        $work->tweeted_at = $tweet->getCreatedAt();
        $work->save();

        // Delete image
        unlink($work->filename);

        return 0;
    }
}
