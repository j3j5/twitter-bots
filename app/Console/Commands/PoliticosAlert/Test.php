<?php

namespace App\Console\Commands\PoliticosAlert;

use App\Models\PoliticosAlert\FriendsList;
use App\Models\PoliticosAlert\TwitterUser;
use Illuminate\Console\Command;
use j3j5\TwitterApio;
use Symfony\Component\Console\Output\Output;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pol-alert:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $hidden = true;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(TwitterApio $api)
    {
        $params = [
            // 'list_id' => '169558037',   // UY CHECK
            'list_id' => '1255155125812748292',     // Senadores UY
            'skip_status' => true,
            'count' => 5000,    // max
        ];
        $response = $api->get('lists/members', $params);

        foreach ($response['users'] as $userTweet) {
            $user = TwitterUser::createOrUpdateFromTweet($userTweet);
            $this->info('processing @' . $user->username, Output::VERBOSITY_VERBOSE);

            $userParams = [
                'user_id' => $user->twitter_id,
                'count' => 200, // max
            ];
            $friends = [];
            $pageNr = 1;

            if ($user->friends()->exists()) {
                continue;
            }

            foreach ($api->getFriends($userParams) as $page) {
                $this->info('Page ' . $pageNr++, Output::VERBOSITY_VERBOSE);
                if (is_array($page)) {
                    $friends = array_merge($friends, $page);
                }
            }
            FriendsList::updateOrCreate(['user_id' => $user->id], ['list' => $friends]);

            // dd($friends);
        }

        return 0;
    }
}
