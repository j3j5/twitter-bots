<?php

namespace App\Console\Commands\Inumet;

use App\Models\Inumet\Observation;
use App\Models\Inumet\Station;
use App\Models\Inumet\Variable;
use App\Services\Inumet;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use RuntimeException;

class GetCurrentData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inumet:get-current';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Current Weather data from INUMET\'s hidden api and store it';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Inumet $inumet)
    {
        $currentWeather = $inumet->getCurrentWeather();

        Storage::disk('local')->put('inumet/observations/' . now('America/Montevideo')->format('H\.\j\s\o\n'), $currentWeather->toJson());

        // Update available stations
        $stations = $inumet->rawData['estaciones'];
        $fecha = collect($inumet->rawData['fechas'])->map(fn (string $fecha) : Carbon => Carbon::parse($fecha, 'America/Montevideo'))->first();
        if (!$fecha instanceof Carbon) {
            throw new RuntimeException('There seems to be no date on the data');
        }

        collect($inumet->rawData['variables'])->each(
            fn (array $variable) => Variable::firstOrCreate(
                ['id' => $variable['idInt']],
                [
                    'variable' => $variable['variable'],
                    'name' => $variable['nombre'],
                    'unit' => $variable['unidad'],
                ]
            )
        );

        $this->output->progressStart(count($stations));
        foreach ($stations as $stationArr) {
            $station = Station::firstOrCreate(['id' => $stationArr['id']], [
                'codeName' => $stationArr['Estacion'],
                'name' => $stationArr['NombreEstacion'],
            ]);

            $obserData = $currentWeather->mapWithKeys(
                fn (array $row, string $key) => [
                    array_search($key, $inumet->variables) => $row[$station->name],
                ]
            );
            $key = [
                'updated_at' => $fecha->timestamp,
                'station_id' => $station->id,
            ];

            Observation::updateOrCreate($key, $obserData->toArray());
            $this->output->progressAdvance();
        }
        $this->output->progressFinish();

        return 0;
    }
}
