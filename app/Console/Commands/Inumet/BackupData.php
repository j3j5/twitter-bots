<?php

namespace App\Console\Commands\Inumet;

use App\Models\Inumet\Observation;
use App\Models\Inumet\Station;
use App\Models\Inumet\Variable;
use Carbon\Carbon;
use Github\Api\Gists;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use RuntimeException;

use function Safe\fclose;
use function Safe\fopen;
use function Safe\fputcsv;
use function Safe\ob_start;

class BackupData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inumet:backup {gistId} {--first-time}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup the current db data into CSV files';

    private array $filesToUpload;

    private Gists $gistService;

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Gists $gistsApi)
    {
        $this->gistService = $gistsApi;

        $gistId = $this->argument('gistId');
        if (!is_string($gistId)) {
            throw new RuntimeException('You need to provide a valid Gist ID');
        }
        $firstTime = (bool) $this->option('first-time');
        if ($firstTime) {
            $this->info('Generating stations CSV');
            $this->backupStations();

            $this->info('Generating variables CSV');
            $this->backupVariables();
        }
        $this->info('Generating observations CSV');
        $this->backupObservations($firstTime);

        $upload = [
            'files' => $this->filesToUpload,
            'description' => 'Backup de los datos históricos de las mediciones de INUMET. ' . PHP_EOL . 'Última actualización: ' . now()->format('Y-m-d'),
            'public' => false,
        ];

        $this->info('Uploading to Github');

        $this->gistService->update($gistId, $upload);
        return 0;
    }

    private function backupModel(string $filename, Collection $models) : void
    {
        ob_start();
        $h = fopen('php://output', 'a');
        $this->output->progressStart($models->count());
        fputcsv($h, $models->first()->getTableColumns());
        $models->each(function ($model) use ($h) {
            fputcsv($h, $model->toArray());
            $this->output->progressAdvance();
        });
        fclose($h);
        $csv = ob_get_clean();
        if (!is_string($csv)) {
            throw new RuntimeException("There was a problem generation $filename");
        }
        $this->output->progressFinish();

        $this->filesToUpload[$filename] = ['content' => $csv];
    }

    private function backupStations() : void
    {
        $this->backupModel('stations.csv', Station::all());
    }

    private function backupVariables() : void
    {
        $this->backupModel('variables.csv', Variable::all());
    }

    private function backupObservations(bool $firstTime) : void
    {
        // Read earliest dates from the DB
        $earliestObservation = Carbon::createFromTimestamp(Observation::orderBy('updated_at')
            ->firstOrFail('updated_at')->updated_at);
        // Read latest dates from the DB
        $latestObservation = Carbon::createFromTimestamp(Observation::orderBy('updated_at', 'desc')
            ->firstOrFail('updated_at')->updated_at);

        if ($firstTime) {
            $date = $earliestObservation->copy();
        } else {
            // Read latest month from GH
            $gists = $this->gistService->all();
            if (!is_array($gists)) {
                throw new RuntimeException('Could not retrieve gists from user');
            }

            $observationsGist = Arr::first($gists, fn ($gist) => $gist['id'] === $this->argument('gistId'));
            if ($observationsGist === null) {
                throw new RunTimeException('The given gist ID was not found');
            }

            /** @var array<string, array{filename: string, type: string, language: string, raw_url: string, size: int}> $files */
            $files = $observationsGist['files'];
            /** @var string $latestFile */
            $latestFile = collect($files)
                ->keys()
                ->filter(fn ($filename) => Str::startsWith($filename, 'observations-'))
                ->sort()
                ->last();

            // Start the update from the latest file available
            $date = Carbon::createFromFormat(
                'Y-m',
                str_replace(['observations-', '.csv'], '', $latestFile)
            );
            if (!$date instanceof Carbon) {
                throw new RuntimeException('Could not transform the filename to date');
            }
        }

        while ($date->month <= $latestObservation->month) {
            $filename = 'observations-' . $date->format('Y-m') . '.csv';
            $data = Observation::whereBetween('updated_at', [
                $date->copy()->startOfMonth()->timestamp,
                $date->copy()->endOfMonth()->timestamp,
            ])->get();
            // Only push to GH if there's something to push
            if ($data->isNotEmpty()) {
                $this->backupModel($filename, $data);
            }
            $date->addMonth();
        }
    }
}
