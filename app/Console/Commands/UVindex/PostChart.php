<?php

namespace App\Console\Commands\UVindex;

use App\Services\FediApi;
use App\Services\UVIndex\Graphs;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use RuntimeException;

class PostChart extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uv:post-chart';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post a chart with the evolution of the UV index during the day';

    private string $filePrefix = 'UVindexMVD';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Graphs $graphs, FediApi $fediApi)
    {
        $charts = $graphs->generateGraphs();
        $data = $graphs->data->get('today');

        $firstDataPoint = $data->firstWhere('uvindex', '>=', 1);
        $lastDataPoint = $data->where('uvindex', '>', 0)->last();
        $maxUVIndex = $data->pluck('uvindex')->max();
        $maxUVIndexData = $data->firstWhere('uvindex', $maxUVIndex);

        $status = '📅 ' . ucfirst($graphs->dates->last()->isoFormat('dddd D [de] MMMM YYYY')) . PHP_EOL;
        $status .= '☀️ Hoy se llegó a un Índice UV ' . round($maxUVIndex) . ' a las ' . Carbon::createFromTimestampMs($maxUVIndexData['timestamp'])->timezone('America/Montevideo')->format('H:i');
        $status .= ' (hasta ' . round($maxUVIndexData['radiation'], 2) . ' W/m²)' . PHP_EOL . PHP_EOL;
        $status .= '😎 ¡A cuidarse!';
        foreach ($charts as $chartName => $chart) {
            // file_put_contents(storage_path('app/uvindex/radiation-' . $chartname . '-' . date('Y-M-d H:i:s') . '.png'), $chart);
            // $mediaResponse = $fediApi->uploadMedia([
            //     'media' => $chart,
            // ]);

            // if (!isset($mediaResponse['media_id_string'])) {
            //     $msg = 'Error while uploading media';
            //     Log::error($msg, Arr::wrap($mediaResponse));
            //     throw new RuntimeException($msg);
            // }

            // Upload the graph
            $filepath = $this->filePrefix . '-' . $chartName . '-' . date('Y-M-d H:i:s') . '.png';
            $uuid = Storage::disk('uploadcare')->putGetUuid($filepath, $chart);
            if (empty($uuid)) {
                throw new RuntimeException('Error while uploading the line graph');
            }

            $altText = 'Gráfico de línea mostrando la evolución durante el ' . $graphs->dates->last()->isoFormat('dddd D [de] MMMM YYYY');
            $altText .= ' de la radiación solar en Montevideo.' . PHP_EOL;
            $altText .= 'La radiación superó el índice 1 por primera vez a las ';
            $altText .= Carbon::createFromTimestampMs($firstDataPoint['timestamp'])->timezone('America/Montevideo')->format('H:i');
            $altText .= ', alcanzó su máximo a las ' . Carbon::createFromTimestampMs($maxUVIndexData['timestamp'])->timezone('America/Montevideo')->format('H:i');
            $altText .= ' (índice ' . round($maxUVIndex) . ')';
            $altText .= ' y se llegó a su punto más bajo a las ' . Carbon::createFromTimestampMs($lastDataPoint['timestamp'])->timezone('America/Montevideo')->format('H:i') . '.' . PHP_EOL;
            $altText .= 'Las medidas fueron tomadas en la azotea del Edificio Anexo del Palacio Municipal, en la calle Soriano, con el radiómetro de la Intendencia de Montevideo. ';

        }
        // $tweetOptions = [
        // 'status' => $status,
        // 'lat' => -34.90706976618914,
        // 'long' => -56.18601472514095,
        // 'display_coordinates' => true,
        // 'media_ids' => implode(',', $tweetImgs),
        // ];

        $options = ['media' => [[
            'mediaType' => 'image/jpeg',
            'url' => config('filesystems.disks.uploadcare.cdn') . '/' . $uuid . '/' ,
            'name' => $altText,
        ]]];
        $response = $fediApi->publishPost($status, $options);
        return 0;
    }
}
