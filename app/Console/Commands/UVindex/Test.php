<?php

namespace App\Console\Commands\UVindex;

use App\Services\UVIndex\Graphs;
use App\Traits\Graphs\GeneratesGraphs;
use Illuminate\Console\Command;

class Test extends Command
{
    use GeneratesGraphs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uv:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $hidden = true;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Graphs $graphs)
    {
        $charts = $graphs->generateGraphs();
        foreach ($charts as $name => $chart) {
            file_put_contents(storage_path('app/uvindex/' . $name . '-' . date('Y-M-d H:i:s') . '.png'), $chart);
        }
        return 0;
    }
}
