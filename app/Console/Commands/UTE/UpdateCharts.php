<?php

namespace App\Console\Commands\UTE;

use App\Services\UTE\ADME;
use App\Services\UTE\Graphs;
use App\Services\UTE\Portal;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use j3j5\TwitterApio;
use RuntimeException;

class UpdateCharts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ute:update-charts {--avatar-only} {--banner-only}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the charts from the profile';

    private Graphs $graphFactory;
    private TwitterApio $api;

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Graphs $graphFactory, TwitterApio $api)
    {
        if ($this->option('avatar-only') && $this->option('banner-only')) {
            $this->error('Only one of the options can be passed at a time.');
            $this->error('If you want to update avatar and banner do not pass any option');
            return Command::INVALID;
        }

        $this->graphFactory = $graphFactory;
        $this->api = $api;

        if ($this->option('avatar-only') || $this->option('banner-only')) {
            if ($this->option('avatar-only')) {
                $this->updateProfilePic();
            }
            if ($this->option('banner-only')) {
                $this->updateBanner();
            }
            return Command::SUCCESS;
        }

        $this->updateProfilePic();
        $this->updateBanner();

        return Command::SUCCESS;
    }

    /**
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Exception
     * @throws \DiDom\Exceptions\InvalidSelectorException
     * @throws \InvalidArgumentException
     * @throws \Carbon\Exceptions\InvalidFormatException
     * @throws \RuntimeException
     * @throws \Amenadiel\JpGraph\Util\JpGraphExceptionL
     * @throws \Safe\Exceptions\OutcontrolException
     * @return bool|array<string, mixed>
     */
    private function updateProfilePic() : bool|array
    {
        $this->info('Updating avatar', 'v');

        /** @var \App\Services\UTE\Portal $portal */
        $portal = app(Portal::class);
        $rawData = $portal->getInstantPower();

        $orderedKeys = [
            'Biomasa' => 'Biomasa',
            'Generación Fotovoltaica' => 'Solar',
            'Generación Eólica' => 'Eólica',
            'Generación Hidráulica' => 'Hidráulica',
            'Generación Térmica' => 'Térmica',
            'Interconexión con Brasil' => 'Importación',
        ];

        foreach ($orderedKeys as $key) {
            $data[$key] = $rawData[$key] ?? 0;
        }

        $demand = $portal->getInstantDemand();
        $currentDemand = $demand->get('DemandaActual');
        $lastUpdateTime = $demand->get('HoraAccesoScada', '');
        $midTitle = '';
        if (!empty($currentDemand)) {
            $midTitle = "Demanda\nActual\n{$currentDemand} MW";
        }
        $lastUpdated = Carbon::createFromFormat('H:i', $lastUpdateTime, 'America/Montevideo') ?: now();
        $piechart = $this->graphFactory->getDistributionPie($data, $midTitle, $lastUpdated);

        $avatarResponse = $this->api->post('account/update_profile_image', [
            'image' => base64_encode($piechart),
            'include_entities' => false,
            'skip_status' => false,
        ]);
        return $avatarResponse;
    }

    /**
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Exception
     * @throws \DiDom\Exceptions\InvalidSelectorException
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     * @throws \Illuminate\Http\Client\RequestException
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Calculation\Exception
     * @throws \Carbon\Exceptions\NotLocaleAwareException
     * @throws \Amenadiel\JpGraph\Util\JpGraphExceptionL
     * @throws \Carbon\Exceptions\InvalidFormatException
     * @throws \Safe\Exceptions\OutcontrolException
     * @return bool|array<string, mixed>
     */
    private function updateBanner() : bool|array
    {
        $this->info('Updating banner', 'v');

        $start = now()->timezone('America/Montevideo')->subDays(7);
        $end = now()->timezone('America/Montevideo');

        /** @var \App\Services\UTE\ADME $adme */
        $adme = app(ADME::class);
        $data = $adme->getDataForInterval($start, $end->copy()->addDay());
        $series = $data->first();
        if (!$series instanceof Collection) {
            throw new RuntimeException('Empty series of data for the given interval');
        }

        $title = 'Generación Eléctrica en Uruguay';
        $subtitle = '(datos del SCADA proporcionados por ADME)';
        $subsubtitle = $start->locale('es_UY')->isoFormat('ll') . ' - ' . $end->locale('es_UY')->isoFormat('ll');

        $lastUpdateTs = $series->keys()->last();
        if (!is_int($lastUpdateTs)) {
            throw new RuntimeException('Last update timestamp not found');
        }
        $lastUpdate = Carbon::createFromTimestamp($lastUpdateTs, 'America/Montevideo');
        $bottomMsg = 'Ultima actualización ' . $lastUpdate->format('Y-m-d H:i');

        // Smooth the series
        $data->transform(fn ($series) => $series->smoother(4));
        $lineGraph = $this->graphFactory->getHistoricalDistributionLine($data, 1500, 500, $title, $subtitle, $subsubtitle, $bottomMsg);

        $bannerResponse = $this->api->post('account/update_profile_banner', [
            'banner' => base64_encode($lineGraph),
        ]);
        return $bannerResponse;
    }
}
