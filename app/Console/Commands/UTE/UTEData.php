<?php

namespace App\Console\Commands\UTE;

use App\Console\Output;
use App\Services\UserAgent;
use App\Services\UTE\Graphs;
use Illuminate\Console\Command;
use Illuminate\Console\OutputStyle;
use Illuminate\Console\View\Components\Factory;
use function Safe\file_put_contents;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UTEData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ute:test-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    public function __construct()
    {
        parent::__construct();
        //
    }

    // /**
    //  * Overrride the command so we can ignore the OutputInterface provided by artisan
    //  * and use our own instead
    //  */
    // public function run(InputInterface $input, OutputInterface $output): int
    // {
    //     $this->output = $this->laravel->make(
    //         OutputStyle::class,
    //         ['input' => $input, 'output' => $this->laravel->make(Output::class)]
    //     );

    //     $this->components = $this->laravel->make(Factory::class, ['output' => $this->output]);

    //     try {
    //         return parent::run(
    //             $this->input = $input,
    //             $this->output
    //         );
    //     } finally {
    //         $this->untrap();
    //     }
    // }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(UserAgent $userAgent)
    {
        // $data = [
        //     'Biomasa' => 111.918277,
        //     'Solar' => 228.910914,
        //     'Eólica' => 838.440089,
        //     'Hidráulica' => 158.5423,
        //     'Térmica' => 25.11733,
        //     // 'Importación' => 0,
        // ];
        // $midTitle = "Demanda\nActual\n1589 MW";
        // /** @var \App\Services\UTE\Graphs $graphFactory */
        // $graphFactory = app(Graphs::class);
        // $graphFactory->getDistributionPie($data, $midTitle);

        // return Command::SUCCESS;

        // dd($headers, $data);

        // ob_start();
        // $this->table(array_keys($demand), [array_values($demand)]);
        // $this->table($headers, $data);
        // $this->info($lastUpdate);

        // file_put_contents('ute.test', ob_get_clean(), FILE_APPEND);

        return Command::SUCCESS;
    }
}
