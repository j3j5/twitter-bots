<?php

namespace App\Console\Commands\UTE;

use App\Services\UTE\ADME;
use App\Services\UTE\Graphs;
use App\Services\UTE\Portal;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use RuntimeException;

use function Safe\file_put_contents;

class TestCharts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ute:test-charts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $hidden = true;

    private Graphs $graphFactory;

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Graphs $factory)
    {
        $this->graphFactory = $factory;

        // dump(now('America/Montevideo')->subDays(3)->subMonths(1));
        $line = $this->spot(now('America/Montevideo')->subMonths(1)->subDays(3));
        file_put_contents(storage_path('app/ute/line-chart-spot-' . now('America/Montevideo')->format('Y-m-d-H:i:s') . '.png'), $line);

        $line = $this->dayChart();
        file_put_contents(storage_path('app/ute/day-line-chart-' . now('America/Montevideo')->format('Y-m-d-H:i:s') . '.png'), $line);

        $pie = $this->profilePic();
        file_put_contents(storage_path('app/ute/profile-pie-' . now('America/Montevideo')->format('Y-m-d-H:i:s') . '.png'), $pie);

        $line = $this->banner();
        file_put_contents(storage_path('app/ute/banner-chart-' . now('America/Montevideo')->format('Y-m-d-H:i:s') . '.png'), $line);

        $bar = $this->weeklyChart();
        file_put_contents(storage_path('app/ute/weekly-bar-' . now('America/Montevideo')->format('Y-m-d-H:i:s') . '.png'), $bar);

        return Command::SUCCESS;
    }

    private function spot(Carbon $date)
    {
        /** @var \App\Services\UTE\ADME $adme */
        $adme = app(ADME::class);
        $data = $adme->getSpotPrice($date);

        /** @var \Illuminate\Support\Collection<int, float> $dataAvg */
        $dataAvg = $data->smoother(6, 'avg');
        $dataSeries = collect([$dataAvg]);

        $title = 'Precios SPOT sancionado de la energía eléctrica';
        $subtitle = ucfirst($date->locale('es_UY')->isoFormat('MMMM YYYY'));
        $subsubtitle = 'Valor medio cada 6 horas en U$S/MWh';

        return $this->graphFactory->getSpotLine($dataSeries, 1920, 1080, $title, $subtitle, $subsubtitle);
    }

    private function dayChart() : string
    {
        $start = now()->timezone('America/Montevideo')->subDay();
        $end = now()->timezone('America/Montevideo');

        /** @var \App\Services\UTE\ADME $adme */
        $adme = app(ADME::class);
        $data = $adme->getDataForInterval($start, $end);
        $series = $data->first();
        if (!$series instanceof Collection) {
            throw new RuntimeException('Empty series of data for the given interval');
        }

        $title = 'Generación Eléctrica en Uruguay';
        $subtitle = '(Distribución por fuente)';
        $subsubtitle = $start->locale('es_UY')->isoFormat('ll');

        // Smooth the series
        $smoothData = $data->map(fn ($series) => $series->smoother(3));
        /** @var \App\Services\UTE\Graphs $graphFactory */
        $this->graphFactory = app(Graphs::class);
        return $this->graphFactory->getHistoricalDistributionLine($smoothData, 1920, 1080, $title, $subtitle, $subsubtitle);
    }

    private function profilePic() : string
    {
        /** @var \App\Services\UTE\Portal $portal */
        $portal = app(Portal::class);
        $rawData = $portal->getInstantPower();

        $orderedKeys = [
            'Biomasa' => 'Biomasa',
            'Generación Fotovoltaica' => 'Solar',
            'Generación Eólica' => 'Eólica',
            'Generación Hidráulica' => 'Hidráulica',
            'Generación Térmica' => 'Térmica',
            'Interconexión con Brasil' => 'Importación',
        ];

        foreach ($orderedKeys as $key) {
            $data[$key] = $rawData[$key] ?? 0;
        }

        $demand = $portal->getInstantDemand();
        $currentDemand = $demand->get('DemandaActual');
        $lastUpdateTime = $demand->get('HoraAccesoScada', '');
        $midTitle = '';
        $lastUpdated = now();
        if (!empty($currentDemand)) {
            $midTitle = "Demanda\nActual\n{$currentDemand} MW";
        }
        if (!empty($lastUpdateTime)) {
            $lastUpdated = Carbon::createFromFormat('H:i', $lastUpdateTime, 'America/Montevideo') ?: now();
        }
        return $this->graphFactory->getDistributionPie($data, $midTitle, $lastUpdated);
    }

    public function banner() : string
    {
        $start = now('America/Montevideo')->subDays(7);
        $end = now('America/Montevideo');

        /** @var \App\Services\UTE\ADME $adme */
        $adme = app(ADME::class);
        $data = $adme->getDataForInterval($start, $end->copy()->addDay());

        $series = $data->first();
        if (!$series instanceof Collection) {
            throw new RuntimeException('Empty series of data for the given interval');
        }

        $title = 'Generación Eléctrica en Uruguay';
        $subtitle = '(datos del SCADA proporcionados por ADME)';
        $subsubtitle = $start->locale('es_UY')->isoFormat('ll') . ' - ' . $end->locale('es_UY')->isoFormat('ll');

        $lastUpdateTs = $series->keys()->last();
        if (!is_int($lastUpdateTs)) {
            throw new RuntimeException('Last update timestamp not found');
        }
        $lastUpdate = Carbon::createFromTimestamp($lastUpdateTs, 'America/Montevideo');
        $bottomMsg = 'Ultima actualización ' . $lastUpdate->format('Y-m-d H:i');

        // Smooth the series
        $data->transform(fn (Collection $series) => $series->smoother(4));
        $this->graphFactory = app(Graphs::class);
        return $this->graphFactory->getHistoricalDistributionLine($data, 1500, 500, $title, $subtitle, $subsubtitle, $bottomMsg);
    }

    private function weeklyChart() : string
    {
        $start = now()->timezone('America/Montevideo')->subWeek()->startOfWeek();
        $end = now()->timezone('America/Montevideo')->subWeek()->endOfWeek();

        /** @var \App\Services\UTE\ADME $adme */
        $adme = app(ADME::class);
        $data = $adme->getDataForInterval($start, $end->copy()->addDay());
        $series = $data->first();
        if (!$series instanceof Collection) {
            throw new RuntimeException('Empty series of data for the given interval');
        }

        $title = 'Generación Eléctrica en Uruguay';
        $subtitle = '(Distribución por fuente)';
        $subsubtitle = 'Semana del ' . $start->locale('es_UY')->isoFormat('D');
        $subsubtitle .= ' al ' . $end->locale('es_UY')->isoFormat('LL');

        // Group and sum by day
        $dayData = $data->map(
            fn ($series) => $series->groupBy(
                fn (float $value, int $ts) => Carbon::createFromTimestamp($ts, 'America/Montevideo')->format('Y-m-d')
            )->filter(
                fn (Collection $dayHours, string $date) => Carbon::createFromFormat('Y-m-d', $date)->weekNumberInMonth === $start->weekNumberInMonth
            )->mapWithKeys(
                fn (Collection $dayHours, string $date) => [
                    // Data points every 10 min in MW, convert to GW
                    // Carbon::createFromFormat('Y-m-d', $date)->timestamp => ($dayHours->chunk(6)->map(fn(Collection $chunk) => $chunk->avg())->sum()) / 1000
                    Carbon::createFromFormat('Y-m-d', $date, 'America/Montevideo')->timestamp => ($dayHours->sum()) / 6 / 1000,
                ]
            )
        );
        return $this->graphFactory->getWeeklyChart($dayData, 1920, 1080, $title, $subtitle, $subsubtitle);
    }
}
