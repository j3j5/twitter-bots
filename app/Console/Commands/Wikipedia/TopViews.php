<?php

namespace App\Console\Commands\Wikipedia;

use App\Services\FediApi;
use App\Services\Wikimedia;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class TopViews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wiki:top-views {articlesAmount}
        {--project=es.wikipedia.org}
        {--date=yesterday}
        {--dry-run}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     */
    public function handle(Wikimedia $wikiApi, FediApi $fediApi)
    {
        $project = (string) $this->option('project');
        $date = Carbon::parse((string) $this->option('date'));
        $access = 'all-access';
        $limit = (int) $this->argument('articlesAmount');

        $response = $wikiApi->topViews($project, $access, $date->format('Y'), $date->format('m'), $date->format('d'));
        $articles = $response->json('items.0.articles');

        $ignore = [
            'Wikipedia:Portada',
            'Especial:Buscar',
        ];

        $message = collect($articles)
            ->take($limit + count($ignore))
            ->reject(fn (array $article) : bool => in_array($article['article'], $ignore))
            ->take($limit)
            ->values()
            ->map(function (array $article, int $index) use ($project) : string {
                $url = 'https://' . $project . '/wiki/' . $article['article'];
                $name = str_replace(
                    search: ['_'],
                    replace: [' '],
                    subject: $article['article']
                );

                return "<li><a href=\"{$url}\">{$name}</a> - "
                    . number_format(num: $article['views'], thousands_separator: '.') . ' 👀</li>';
            })->implode('');

        $post = '<p>Top 10 📜 <a href="https://' . $project . '/">' . $project . '</a><br>';
        $post .= $date->format('Y-m-d') . '</p><ol>' . $message . '</ol>';

        if (!$this->option('dry-run')) {
            $fediApi->publishPost($post);
        } else {
            $this->line($message);
        }
    }
}
