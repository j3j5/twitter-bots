<?php

namespace App\Console\Commands\Wikipedia;

use App\Services\FediApi;
use App\Services\Wikipedia;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use RuntimeException;

class NotabilityDeletion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wiki:notability-deletion {--dry-run}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post all articles that are proposed for deletion due to unclear notability';

    /**
     * Execute the console command.
     */
    public function handle(FediApi $fediApi, Wikipedia $wiki)
    {
        // https://www.mediawiki.org/wiki/API:Search
        $params = [
            'list' => 'search',
            'srsearch' => 'incategory:"All_articles_proposed_for_deletion" incategory:"All_articles_with_topics_of_unclear_notability"',
            'srlimit' => 500,
            'srsort' => 'create_timestamp_desc',
            // 'srsort' => 'user_random',
        ];

        // First page
        $response = $wiki->parameters($params)->get();
        if ($response->offsetExists('query.searchinfo.totalhits')) {
            $this->info($response->json('query.searchinfo.totalhits') . ' total hits found.');
        }

        $results = $response->json('query.search');
        $articles = collect();
        if (is_array($results)) {
            $articles = collect($results);
        }
        while (true) {
            usleep(50000);
            try {
                $response = $wiki->nextPage($response);
                if (is_array($results)) {
                    $articles = $articles->merge($results);
                }
            } catch (RuntimeException $e) {
                if ($this->output->isVerbose()) {
                    $this->error($e->getMessage());
                }
                break;
            }
        }

        $postedPath = 'wikipedia/notabilityTweeted';
        if (!Storage::disk('local')->exists($postedPath)) {
            Storage::disk('local')->put($postedPath, '');
        }
        $posted = explode(PHP_EOL, Storage::disk('local')->get($postedPath));
        $notPostedYetArticles = $articles->filter(fn ($item) => !in_array($item['pageid'], $posted));

        if ($notPostedYetArticles->isEmpty()) {
            Log::info('There are not articles which have not been published on the list', $articles->pluck('title')->toArray());
            return Command::SUCCESS;
        }

        $article = $notPostedYetArticles->random();
        if ($this->output->isVerbose()) {
            dump($article);
        }

        $url = 'https://en.wikipedia.org/wiki/' . urlencode(str_replace(' ', '_', $article['title']));

        $msg = '<p>🚨 Article proposed for deletion due to unclear notability 🚨</p>';
        $msg .= "<p><a href=\"{$url}\">{$article['title']}</a></p>";
        if (data_get($article, 'snippet')) {
            $msg .= "<blockquote>{$article['snippet']}…</blockquote>";
        }
        $msg .= '<p>~' . $this->humanFilesize($article['size']) . " ({$article['wordcount']} words) - " . Carbon::parse($article['timestamp'])->toFormattedDateString() . '</p>';
        $msg .= '<p>#wikipedia #proposed4Deletion #unclearNotability</p>';

        if (!$this->option('dry-run')) {
            $fediApi->publishPost($msg);
            Storage::disk('local')->append($postedPath, $article['pageid']);
        } else {
            $this->line($msg);
        }
    }

    /**
     * rommel at rommelsantor dot com
     * https://www.php.net/manual/en/function.filesize.php#106569
     *
     */
    private function humanFilesize(int $bytes, int $decimals = 1) : string
    {
        $sz = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];
        $factor = floor((strlen((string) $bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . Arr::get($sz, $factor);
    }
}
