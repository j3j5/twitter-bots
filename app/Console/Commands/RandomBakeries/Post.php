<?php

namespace App\Console\Commands\RandomBakeries;

use App\DTO\Twitter\Tweet;
use App\Jobs\RandomBakeries\StoreData;
use App\Services\Google\Places;
use App\Services\TwitterHelper;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use j3j5\TwitterApio;
use RuntimeException;
use function Safe\fclose;

use function Safe\fgetcsv;
use function Safe\file_get_contents;
use function Safe\file_put_contents;
use function Safe\fopen;
use function Safe\json_encode;
use function Safe\preg_match;

use Sentry;

class Post extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bakery:post {--filepath=} {--max-tries=5}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post a random bakery';

    protected Places $placesApi;
    protected TwitterHelper $twitterHelper;

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Places $places, TwitterApio $twitter, TwitterHelper $helper)
    {
        $this->placesApi = $places;
        $this->twitterHelper = $helper;
        $tweetedPath = storage_path('app/randomBakeries/tweeted');
        $tweeted = explode("\n", file_get_contents($tweetedPath));
        $found = false;
        $tries = 1;
        $maxTries = (int) $this->option('max-tries');
        $place = [];
        while ($found === false && $tries < $maxTries) {
            $town = $this->getRandomTown();
            try {
                $place = $this->findPlaceForTown($town, $tweeted);
                if (isset($place['photos'])) {
                    $found = true;
                }
            } catch (RuntimeException $e) {
                $this->deleteTownFromFile($town);
                Log::error($e->getMessage());
            }
            $tries++;
        }

        if (!$found) {
            $msg = "No bakery found after $maxTries attempts";
            $this->error($msg);
            Log::warning($msg);
            return -1;
        }

        $fields = 'name,geometry,photos,place_id,price_level,rating,formatted_address,website,url,user_ratings_total,type,reviews';
        $bakery = $places->getDetails($place['place_id'], $fields);

        StoreData::dispatchSync($bakery);
        $bakeryGeo = Arr::get($bakery, 'result.geometry');
        /** @var array<int, array<string, mixed>> $photos */
        $photos = Arr::get($bakery, 'result.photos', []);
        $bakeryPhotosGoogle = collect($photos)->map(function ($item) {
            $attr = reset($item['html_attributions']);
            $reference = $item['photo_reference'];
            return compact('reference', 'attr');
        })->shuffle();

        $refs = $bakeryPhotosGoogle->pluck('reference')->toArray();
        $limit = 4;
        $images = $places->getImages($bakery['result']['name'], $refs, $limit);
        $tweetImgs = [];
        foreach ($images as $ref => $path) {
            $mediaResponse = $twitter->uploadMedia([
                'media' => file_get_contents($path),
            ]);
            $attrs = $bakeryPhotosGoogle->firstWhere('reference', $ref);
            if (isset($attrs['attr'])) {
                // Add alt text
                $twitter->addMetadata([
                    'media_id' => $mediaResponse['media_id_string'],
                    'alt_text' => [
                        'text' => 'Foto hecha por ' . strip_tags($attrs['attr']),
                    ],
                ]);
            }

            $tweetImgs[] = $mediaResponse['media_id_string'];
        }
        $address = $this->getNiceAddress($bakery['result']['formatted_address']);

        $lines = [];
        $breadEmojis = ['🍞', '🥖', '🫓', '🥪', '🥐', '🥯'];
        $lines[] = Arr::random($breadEmojis) . ' ' . $bakery['result']['name'];
        $lines[] = "🗺️ {$address}";
        if (isset($bakery['result']['rating'], $bakery['result']['user_ratings_total'])) {
            $lines[] = $bakery['result']['rating'] . ' ' . Str::repeat('⭐', (int) $bakery['result']['rating']) . " ({$bakery['result']['user_ratings_total']} reviews)";
        }
        if (isset($bakery['result']['price_level'])) {
            $line = Str::repeat('💲', $bakery['result']['price_level']);
            if (isset($lines[2])) {
                $lines[2] .= ' ⸻ ' . $line;
            } else {
                $lines[] = $line;
            }
        }
        if (!empty($bakery['result']['website'])) {
            $lines[] = "🌏 {$bakery['result']['website']}" . PHP_EOL;
        }

        // Don't tweet if the bakery has no images, sorry!
        if (empty($tweetImgs)) {
            file_put_contents($tweetedPath, $bakery['result']['place_id'] . "\n", FILE_APPEND);
            return -1;
        }
        $lines[] = "🔗 {$bakery['result']['url']}";

        $status = implode(PHP_EOL, $lines);
        $tweet = null;
        $tweetOptions = [
                'status' => $status,
                'lat' => $bakeryGeo['location']['lat'],
                'long' => $bakeryGeo['location']['lng'],
                'display_coordinates' => true,
                'media_ids' => implode(',', $tweetImgs),
            ];
        $tweetResponse = $twitter->post('statuses/update', $tweetOptions);
        if (!is_array($tweetResponse)) {
            Log::error('Something went wrong when tweeting the image', Arr::wrap($tweetResponse));
            $this->error('Something went wrong when tweeting the image');
            return -1;
        }
        // Add the bakery to the tweeted list
        file_put_contents($tweetedPath, $bakery['result']['place_id'] . "\n", FILE_APPEND);
        $tweet = new Tweet($tweetResponse);

        $reviewTweets = $this->getReviewTweets($bakery);
        $reply = $tweet->getStatusId();
        foreach ($reviewTweets as $tweet) {
            $tweetOptions = [
                'status' => $tweet,
                'in_reply_to_status_id' => $reply,
            ];
            Log::debug('tweeting with ', $tweetOptions);
            $tweetResponse = $twitter->post('statuses/update', $tweetOptions);
            Log::debug('tweetResponse', Arr::wrap($tweetResponse));

            if (!is_array($tweetResponse)) {
                throw new RuntimeException('Error while tweeting');
            }
            $reply = Arr::get($tweetResponse, 'id_str');
            sleep(random_int(1, 3));
        }

        return 0;
    }

    private function getRandomTown() : string
    {
        $file = $this->option('filepath');
        if (!is_string($file)) {
            $file = storage_path('app/localidades.csv');
        }

        $handle = fopen($file, 'r');
        /** @var string[] $towns */
        $towns = [];

        /** @var array<int, string> $towns */
        while ($data = fgetcsv($handle)) {
            if (!is_array($data) || !isset($data[0], $data[1])) {
                continue;
            }

            $town = Str::title(mb_strtolower($data[1]));
            $province = Str::title(mb_strtolower($data[0]));
            $towns[] = "$town, Departamento de $province, Uruguay";
        }
        fclose($handle);

        return Arr::random($towns);
    }

    private function findPlaceForTown(string $town, array $tweeted) : array
    {
        $name = "Panadería en {$town}";
        $biasSW = [
            'lat' => -34.97487624147656, // South
            'long' => -58.29620361328124,   // West
        ];
        $biasNE = [
            'lat' => -30.128499916692782,   // North
            'long' => -53.1353759765625,    // East
        ];
        $bias = "rectangle:{$biasSW['lat']},{$biasSW['long']}|{$biasNE['lat']},{$biasNE['long']}";
        // $fields = 'geometry,photos,place_id,type,price_level,rating,formatted_address,formatted_phone_number,opening_hours,website,url';
        $response = $this->placesApi->findPlace($name, $bias);
        if (Arr::get($response, 'candidates') === null) {
            $this->error('No bakery found for ' . $name);
            $this->error(json_encode($response));
            throw new RunTimeException('No bakery found for ' . $name);
        }
        /** @var array<int, array> */
        $candidates = $response['candidates'];
        $places = collect($candidates)->reject(function ($place) {
            return !in_array('bakery', Arr::get($place, 'types', []));
        })->filter(function ($place) use ($tweeted) {
            $placeId = Arr::get($place, 'place_id');
            return !in_array($placeId, $tweeted);
        });

        if ($places->isEmpty()) {
            throw new RuntimeException('No places found for ' . $name);
        }

        return $places->random();
    }

    private function getNiceAddress(string $formattedAddress) : string
    {
        // Remove country
        $address = str_replace(', Uruguay', '', $formattedAddress);
        // Remove repeated capitals
        $address = str_replace('Artigas, Departamento de Artigas', 'Artigas', $address);
        $address = str_replace('Canelones, Departamento de Canelones', 'Canelones', $address);
        $address = str_replace('Colonia del Sacramento, Departamento de Colonia', 'Colonia del Sacramento', $address);
        $address = str_replace('Durazno, Departamento de Durazno', 'Durazno', $address);
        $address = str_replace('Florida, Departamento de Florida', 'Florida', $address);
        $address = str_replace('Maldonado, Departamento de Maldonado', 'Maldonado', $address);
        $address = str_replace('Montevideo, Departamento de Montevideo', 'Montevideo', $address);
        $address = str_replace('Paysandú, Departamento de Paysandú', 'Paysandú', $address);
        $address = str_replace('Rivera, Departamento de Rivera', 'Rivera', $address);
        $address = str_replace('Rocha, Departamento de Rocha', 'Rocha', $address);
        $address = str_replace('Salto, Departamento de Salto', 'Salto', $address);
        $address = str_replace('San José de Mayo, Departamento de San José', 'San José de Mayo', $address);
        $address = str_replace('Tacuarembó, Departamento de Tacuarembó', 'Tacuarembó', $address);
        $address = str_replace('Treinta y Tres, Departamento de Treinta y Tres', 'Treinta y Tres', $address);

        // Remove 'Departamento de'
        $address = str_replace('Departamento de ', '', $address);
        $address = str_replace(' Department', '', $address);

        return $address;
    }

    private function getReviewTweets(array $bakery, string $replyToUser = '@PanaderiasUy') : array
    {
        $tweets = [];
        $emojis = ['🔊', '📢', '📣', '🔔', '📱'];
        // Post reviews if they are present
        if (isset($bakery['result']['reviews']) && count($bakery['result']['reviews']) > 0) {
            $lines = [];
            $emojis = Arr::shuffle($emojis);
            $reviews = Arr::shuffle($bakery['result']['reviews']);
            $reviews = array_slice($reviews, 0, 6);
            foreach ($reviews as $i => $review) {
                $rev = trim($review['text']);
                if (empty($rev)) {
                    continue;
                }

                $emoji = $emojis[$i % count($emojis)];
                $date = Carbon::createFromTimestamp($review['time'])->locale('es_UY')->isoFormat('MMM YY');
                $date = ucfirst(str_replace('.', '', $date));
                $text = "$emoji $rev ($date)";
                $lines[] = $text;
            }
            $tweets = $this->twitterHelper->splitTextIntoTweets($replyToUser . ' REVIEWS' . PHP_EOL . implode(PHP_EOL, $lines));
        }
        return $tweets;
    }

    private function deleteTownFromFile(string $town) : void
    {
        $file = $this->option('filepath');
        if (!is_string($file)) {
            $file = storage_path('app/localidades.csv');
        }
        $townsData = file_get_contents($file);
        $regex = '/(?<town>.*), Departamento de (?<prov>.*), Uruguay/';
        if (!preg_match($regex, $town, $matches)) {
            return;
        }
        $dataTown = mb_strtoupper($matches['prov']) . ',' . mb_strtoupper($matches['town']);

        $removedTown = str_replace($dataTown . PHP_EOL, '', $townsData);
        file_put_contents($file, $removedTown);

        $message = 'No bakeries to tweet at ' . $town . '. It has been removed from the towns list';
        Log::notice($message);
        // Sentry::captureMessage($message);
    }
}
