<?php

namespace App\Console\Commands\CDF;

use App\Services\FediApi;
use App\Services\UserAgent;
use Arr;
use DiDom\Document;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use RuntimeException;

use function Safe\file_get_contents;
use function Safe\file_put_contents;
use function Safe\json_decode;

class Post extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cdf:post {--filepath=} {--dry-run}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post images from the CDF archive';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(FediApi $api)
    {
        $dryRun = (bool) $this->option('dry-run');

        $tweetedPath = storage_path('app/CDF/tweeted');
        $tweeted = explode("\n", file_get_contents($tweetedPath));

        $photoInfo = $this->getPhotoInfo($tweeted);
        if (!isset($photoInfo['archivo'])) {
            Log::notice('No picture found, ignoring');
            $photoInfo = $this->getPhotoInfo($tweeted);
            if (!isset($photoInfo['archivo'])) {
                // WTF?? quit with an exception, make noise!
                throw new RuntimeException('No picture found after 2 tries!!');
            }
        }

        $date = $photoInfo['fecha'];
        if (preg_match('/(\d{4})/', $photoInfo['fecha'], $matches)) {
            $year = '<a href="https://cdf.montevideo.gub.uy/buscar/fotos?filters=anio:' . $matches[1] . '" target="_blank">' . $matches[1] . '</a>';
            $date = str_replace($matches[1], $year, $photoInfo['fecha']);
        }
        try {
            /** @var \App\Services\UserAgent $userAgent */
            $userAgent = app(UserAgent::class);
            // Get keywords
            $response = Http::withHeaders([
                'User-Agent' => $userAgent->get(),
            ])->get($photoInfo['detallesUrl']);

            $doc = new Document($response->body());

            $tr = $doc->find('.catalogo-ficha-foto > table.sticky-enabled > tbody > .odd')[1];
            $tags = Str::of($tr->find('td')[1]->text())->explode(',')
                ->mapWithKeys(
                    fn ($tag) => [trim($tag) => 'https://cdf.montevideo.gub.uy/buscar/fotos/?filters=' . rawurlencode('entity:foto sm_prop_534:"' . trim($tag) . '"')]
                )->reduce(fn ($str, $url, $tag) => $str . '<a href="' . $url . '" target="_blank">' . $tag . '</a>, ', '');
            $tags = rtrim($tags, ' \n\r\t\v\0,');
        } catch(Exception $e) {
            Log::notice($e->getMessage());
        }

        $tweet = '<p><a href="' . $photoInfo['detallesUrl'] . '" target="_blank">' . $photoInfo['descripcion'] . '</a><br><br>';
        $tweet .= "📅 {$date}<br>";
        if (!empty($tags)) {
            $tweet .= '🏷️ ' . $tags . '<br>';
        }
        $tweet .= '📷 Equipo de fotógrafos del <a href="https://elpopular.uy/" target="_blank">Diario El Popular</a><br>';
        $tweet .= '📃 <a href="https://cdf.montevideo.gub.uy/articulo/archivo-fotografico-del-diario-el-popular-disponible-en-alta-resolucion" target="_blank">CC BY-NC-ND 4.0</a></p>';

        $alt = 'Foto del archivo de El Popular, descripción en la primera frase del mensaje.';

        if (!$dryRun) {
            $options = ['media' => [[
                'mediaType' => 'image/jpeg',
                'url' => $photoInfo['archivo'],
                'name' => $alt,
            ]]];
            $response = $api->publishPost($tweet, $options);
            Log::debug('tweetresponse', Arr::wrap($response->body()));

            if ($response->created()) {
                // Add the id to the tweeted list
                file_put_contents($tweetedPath, $photoInfo['nombre'] . "\n", FILE_APPEND);
                return Command::SUCCESS;
            }
            dump($response->body());
            return Command::FAILURE;
        }

        $this->info(str_replace(['<br>'], ["\n"], $tweet));
        $this->line($photoInfo['archivo']);
        $this->line($alt);

        return Command::SUCCESS;
    }

    private function getPhotoInfo(array $tweeted) : array
    {
        $filepath = storage_path('app/CDF/cdf-el-popular.json');
        if (is_string($this->option('filepath'))) {
            $filepath = $this->option('filepath');
        }

        $data = json_decode(file_get_contents($filepath), true);

        if (!is_array($data)) {
            throw new Exception('Invalid data on json file');
        }
        return collect($data)->filter(fn ($item) => !in_array($item['nombre'], $tweeted))->random();
    }
}
