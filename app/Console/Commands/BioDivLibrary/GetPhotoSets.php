<?php

namespace App\Console\Commands\BioDivLibrary;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use JeroenG\Flickr\Flickr;
use RuntimeException;

use function Safe\file_put_contents;
use function Safe\json_encode;

class GetPhotoSets extends Command
{
    private const USER_ID = '61021753@N02';
    // private const USERNAME = 'BioDivLibrary';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'biodivlib:photosets {filepath=biodivlibrary-photosets.json}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Flickr $flickr)
    {
        $filepath = $this->argument('filepath');
        if (!is_string($filepath)) {
            throw new RuntimeException('Filepath must be a string');
        }
        $photosets = new Collection();
        $pages = 1;
        $page = 1;
        do {
            $response = $flickr->request('flickr.photosets.getList', ['user_id' => self::USER_ID, 'page' => $page]);
            dump(Arr::only(data_get($response, 'contents.photosets', []), ['page', 'pages', 'perpage', 'total']));
            $page = data_get($response, 'contents.photosets.page', 1) + 1;
            $pages = data_get($response, 'contents.photosets.pages', 1);
            if ($page === 1) {
                $this->output->progressStart($pages);
            }
            /** @var array<int, array<string, mixed>> $photoset */
            $photoset = data_get($response, 'contents.photosets.photoset', [[]]);
            $cleanSet = collect($photoset)->map(
                fn ($i) => Arr::only($i, ['id', 'primary', 'secret', 'count_views', 'count_photos', 'count_videos', 'title', 'description', 'date_create', 'date_update'])
            );
            $photosets = $photosets->merge($cleanSet);
        } while ($page <= $pages);

        file_put_contents($filepath, json_encode($photosets->toArray(), JSON_PRETTY_PRINT));
        return 0;
    }
}
