<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraFieldsToUruguayansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {

            // Universidad
            $table->string('tipo_universidad')->nullable()->comment('e219');
            $table->string('años_aprobados_uni')->nullable()->comment('e51_9');
            $table->string('carrera')->nullable()->comment('e220_1');

            // IPA o similar
            $table->string('asistencia_fdocente')->nullable()->comment('e215');
            $table->boolean('finalizo_fdocente')->default(false)->comment('e215_1');
            $table->string('tipo_fdocente')->nullable()->comment('e216');
            $table->string('años_aprobados_fd')->nullable()->comment('e51_8');
            $table->string('carrera_fd')->nullable()->comment('e217_1');

            // Dejaron trabajo
            $table->boolean('trabajo_antes')->default(false)->comment('f116');
            $table->string('tiempo_dejo_empleo')->nullable()->comment('f118_1 and f118_2');
            $table->string('categoria_anterior_trabajo')->nullable()->comment('f121');
            $table->boolean('aportaba_caja')->default(false)->comment('f123');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->boolean('aportaba_caja')->default(false)->comment('f123');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->string('categoria_anterior_trabajo')->nullable()->comment('f121');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->string('tiempo_dejo_empleo')->nullable()->comment('f118_1 and f118_2');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->string('trabajo_antes')->nullable()->comment('f118_1 and f118_2');
        });

        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('carrera_fd');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('años_aprobados_fd');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('tipo_fdocente');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('finalizo_fdocente');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('asistencia_fdocente');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('carrera');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('años_aprobados');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('tipo_universidad');
        });
    }
}
