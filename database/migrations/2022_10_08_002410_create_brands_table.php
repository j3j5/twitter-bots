<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ind-prop')->create('brands', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->unsignedBigInteger('requestNumber');
            $table->string('brand');
            $table->json('niceClasses');
            $table->string('requestType');
            $table->string('requestSubType')->nullable();
            $table->string('signType')->nullable();
            $table->timestamp('receptionDate');
            $table->string('status')->nullable();
            $table->string('owner');
            $table->string('ownerNationality')->nullable();
            $table->text('ownerAddress')->nullable();

            $table->text('logo')->nullable();
            $table->string('requestUrl')->nullable();

            $table->string('tweet_id')->nullable();
            $table->timestamp('tweeted_at')->nullable();

            $table->index('requestNumber');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ind-prop')->dropIfExists('brands');
    }
};
