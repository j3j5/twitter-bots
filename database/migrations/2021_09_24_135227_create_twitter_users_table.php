<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTwitterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pol-alert')->create('twitter_users', function (Blueprint $table) {
            $table->id();
            $table->string('twitter_id');
            $table->string('username');
            $table->string('name');
            $table->text('description');
            $table->string('location');
            $table->string('url');
            $table->boolean('protected');
            $table->boolean('verified');
            $table->unsignedBigInteger('followers_count');
            $table->unsignedBigInteger('friends_count');
            $table->unsignedBigInteger('listed_count');
            $table->timestamp('created_at');
            $table->unsignedBigInteger('favourites_count');
            $table->unsignedBigInteger('statuses_count');
            $table->string('avatar');
            $table->string('banner');
            $table->json('withheld_in_countries');

            $table->unique('twitter_id');
            $table->index('username');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pol-alert')->dropIfExists('twitter_users');
    }
}
