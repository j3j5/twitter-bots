<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('everypurchase')->create('purchases', function (Blueprint $table) {
            $table->id();
            // RSS
            $table->unsignedBigInteger('arce_id');
            $table->string('purchase_id');
            $table->text('description');
            $table->string('buyer');
            $table->string('buyer_sub');
            $table->string('type');
            $table->string('link');
            $table->timestamp('published_at');

            // Detail page
            $table->string('total_amount')->nullable();
            $table->timestamp('bought_at')->nullable();

            // twitter
            $table->string('user_id')->nullable();
            $table->string('username')->nullable();
            $table->string('tweet_id')->nullable();
            $table->timestamp('tweeted_at')->nullable();

            $table->unique('link');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('everypurchase')->dropIfExists('purchases');
    }
}
