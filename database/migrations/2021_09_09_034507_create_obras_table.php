<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('autoresuy')->create('obras', function (Blueprint $table) {
            $table->id();
            $table->string('path');
            $table->string('titulo');
            $table->string('subtitulo')->nullable();
            $table->string('autores')->nullable();
            $table->string('fecha_publicacion')->nullable();
            $table->string('tipo_obra');
            $table->string('estado_derechos_obra')->nullable();

            // Extra
            $table->string('ubicacion')->nullable();
            $table->string('materiales')->nullable();
            $table->string('alto')->nullable();
            $table->string('ancho')->nullable();
            $table->string('calidad')->nullable();
            $table->text('asset')->nullable();
            $table->boolean('scraped')->default(false);

            // Twitter
            $table->string('user_id')->nullable();
            $table->string('username')->nullable();
            $table->string('tweet_id')->nullable();
            $table->timestamp('tweeted_at')->nullable();

            $table->unique('path');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('autoresuy')->dropIfExists('obras');
    }
}
