<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('everypurchase')->create('purchase_sources', function (Blueprint $table) {
            $table->id();
            $table->string('slug');
            $table->string('name');
            $table->text('rss');
            $table->string('twitter_config');
            $table->boolean('active')->default(1);

            $table->index('slug');
        });

        Schema::connection('everypurchase')->table('purchases', function (Blueprint $table) {
            $table->unsignedInteger('source_id')->nullable();
        });

        Schema::connection('everypurchase')->table('purchases', function (Blueprint $table) {
            $table->foreign('source_id')->references('id')->on('purchase_sources');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::connection('everypurchase')->table('purchases', function (Blueprint $table) {
        // $table->dropColumn('source_id');
        // $table->dropForeign(['source_id']);
        // });
        // SQLite doesn't support dropping foreign keys (you would need to re-create the table).
        $statement = 'PRAGMA foreign_keys=off';
        DB::connection('everypurchase')->statement(DB::raw($statement));

        DB::connection('everypurchase')->beginTransaction();
        // $statement = 'BEGIN TRANSACTION';
        // DB::connection('everypurchase')->statement(DB::raw($statement));

        $statement = 'ALTER TABLE purchases RENAME TO _purchases_old';
        DB::connection('everypurchase')->statement(DB::raw($statement));

        $statement = 'CREATE TABLE IF NOT EXISTS "purchases" (
            "id" integer not null primary key autoincrement,
            "arce_id" integer not null,
            "purchase_id" varchar not null,
            "description" text not null,
            "buyer" varchar not null,
            "buyer_sub" varchar not null,
            "type" varchar not null,
            "link" varchar not null,
            "published_at" datetime not null,
            "total_amount" varchar,
            "bought_at" datetime,
            "user_id" varchar,
            "username" varchar,
            "tweet_id" varchar,
            "tweeted_at" datetime
        )';
        DB::connection('everypurchase')->statement(DB::raw($statement));

        $statement = 'DROP INDEX IF EXISTS "purchases_link_unique"';
        DB::connection('everypurchase')->statement(DB::raw($statement));

        $statement = 'CREATE UNIQUE INDEX "purchases_link_unique" on "purchases" ("link")';
        DB::connection('everypurchase')->statement(DB::raw($statement));

        $statement = 'INSERT INTO purchases SELECT "id", "arce_id", "purchase_id", "description", "buyer", "buyer_sub", "type", "link", "published_at", "total_amount", "bought_at", "user_id", "username", "tweet_id", "tweeted_at" FROM _purchases_old';
        DB::connection('everypurchase')->statement(DB::raw($statement));

        $statement = 'DROP TABLE IF EXISTS "_purchases_old"';
        DB::connection('everypurchase')->statement(DB::raw($statement));

        // $statement = 'COMMIT';
        // DB::connection('everypurchase')->statement(DB::raw($statement));

        DB::connection('everypurchase')->commit();

        $statement = 'PRAGMA foreign_keys=on';
        DB::connection('everypurchase')->statement(DB::raw($statement));

        Schema::connection('everypurchase')->dropIfExists('purchase_sources');
    }
}
