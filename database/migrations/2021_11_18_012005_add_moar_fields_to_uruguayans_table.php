<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMoarFieldsToUruguayansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            // pre-escolar
            $table->string('asistio_preescolar')->nullable()->comment('e193');
            $table->string('tipo_centro_preescolar')->nullable()->comment('e194');
            $table->boolean('recibe_comida_preescolar')->default(false)->comment('e196');
            $table->integer('cantidad_comidas_semanales_desayuno_pre')->default(0)->comment('e196_1');
            $table->integer('cantidad_comidas_semanales_almuerzo_pre')->default(0)->comment('e196_2');
            $table->integer('cantidad_comidas_semanales_merienda_pre')->default(0)->comment('e196_3');

            // primaria
            $table->string('asistio_primaria')->nullable()->comment('e197');
            $table->boolean('finalizo_primaria')->default('false')->comment('e197_1');
            $table->string('años_aprobados_primaria')->default(0)->comment('e51_2');
            $table->string('años_aprobados_primaria_especial')->default(0)->comment('e51_3');
            $table->string('tipo_centro_primaria')->nullable()->comment('e198');
            $table->boolean('recibe_comida_primaria')->default(false)->comment('e200');
            $table->integer('cantidad_comidas_semanales_desayuno_pri')->default(0)->comment('e200_1');
            $table->integer('cantidad_comidas_semanales_almuerzo_pri')->default(0)->comment('e200_2');
            $table->integer('cantidad_comidas_semanales_merienda_pri')->default(0)->comment('e200_3');

            // MEDIA
            $table->string('asistio_media')->nullable()->comment('e201');
            // $table->boolean('finalizo_media')->default(false)->comment('e201_1');
            $table->string('razon_no_termino_media')->nullable()->comment('e202_7');
            $table->string('tipo_liceo')->nullable()->comment('e210_2');
            $table->string('años_aprobados_media_basico')->nullable()->comment('e51_4');
            $table->string('años_aprobados_bachillerato')->nullable()->comment('e51_5');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // pre-escolar
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('asistio_preescolar');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('tipo_centro_preescolar');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('recibe_comida_preescolar');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('cantidad_comidas_semanales_desayuno_pre');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('cantidad_comidas_semanales_almuerzo_pre');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('cantidad_comidas_semanales_merienda_pre');
        });

        // primaria
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('asistio_primaria');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('finalizo_primaria');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('años_aprobados_primaria');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('años_aprobados_primaria_especial');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('tipo_centro_primaria');
        });

        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('recibe_comida_primaria');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('cantidad_comidas_semanales_desayuno_pri');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('cantidad_comidas_semanales_almuerzo_pri');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('cantidad_comidas_semanales_merienda_pri');
        });

        // MEDIA
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('asistio_media');
        });
        // Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
        //     $table->dropColumn('finalizo_media');
        // });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('razon_no_termino_media');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('tipo_liceo');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('años_aprobados_media_basico');
        });
        Schema::connection('everyuruguayan')->table('uruguayans', function (Blueprint $table) {
            $table->dropColumn('años_aprobados_bachillerato');
        });
    }
}
