<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNomenclatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('everylot')->create('nomenclators', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('via');
            $table->string('nombre_abreviado');
            $table->string('especificacion');
            $table->string('nombre');
            $table->string('nombre_clasificacion');
            $table->text('significado');
            $table->integer('comienzo_num');
            $table->integer('fin_num');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('everylot')->dropIfExists('nomenclators');
    }
}
