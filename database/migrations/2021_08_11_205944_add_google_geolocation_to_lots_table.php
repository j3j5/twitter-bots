<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGoogleGeolocationToLotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('everylot')->table('lots', function (Blueprint $table) {
            $table->string('google_lat')->nullable();
            $table->string('google_long')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('everylot')->table('lots', function (Blueprint $table) {
            $table->dropColumn('google_long');
            $table->dropColumn('google_lat');
        });
    }
}
