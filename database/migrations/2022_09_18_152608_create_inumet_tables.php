<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('inumet')->create('stations', function (Blueprint $table) {
            $table->id();
            $table->string('codename');
            $table->string('name');
            $table->string('lat')->nullable();
            $table->string('long')->nullable();
            $table->string('altitude')->nullable();
            $table->string('department')->nullable();

            $table->string("OMM")->nullable();
            $table->string("OACI")->nullable();
            $table->string("codigoPluviometrico")->nullable();

            $table->boolean("active")->nullable();
            $table->string("administrator")->nullable();
            $table->string("ISO3166")->nullable();

            $table->boolean("tipomet")->nullable();
            $table->boolean("tipopluvio")->nullable();
            $table->boolean("tipoExterna")->nullable();
            $table->boolean("tipoAeronautica")->nullable();
            $table->boolean("automatica")->nullable();
            $table->string("timezone")->nullable();
            $table->string("adminShort")->nullable();

            $table->index('code');
            $table->index('codename');
        });

        Schema::connection('inumet')->create('variables', function (Blueprint $table) {
            $table->id();
            $table->string("variable");
            $table->string("name");
            $table->string("unit")->nullable();

            $table->index('variable');
        });

        Schema::connection('inumet')->create('observations', function (Blueprint $table) {
            $table->id();
            $table->timestamp('updated_at');
            $table->unsignedBigInteger('station_id');

            $table->string("Visibilidad")->nullable();
            $table->string("DirViento")->nullable();
            $table->string("IntViento")->nullable();
            $table->string("IntRafaga")->nullable();
            $table->string("IntVientMaxHora")->nullable();
            $table->string("TempAire")->nullable();
            $table->string("HumRelativa")->nullable();
            $table->string("TempPtoRocio")->nullable();
            $table->string("TensionVapAgua")->nullable();
            $table->string("PresAtmEst")->nullable();
            $table->string("PresAtmMar")->nullable();
            $table->string("IconoEstadoActual")->nullable();
            $table->string("Nubes")->nullable();
            $table->string("Cielo")->nullable();
            $table->string("precipHoraria")->nullable();

            $table->foreign('station_id')->references('id')->on('stations');
            $table->unique(['station_id', 'updated_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('inumet')->dropIfExists('observations');
        Schema::connection('inumet')->dropIfExists('variables');
        Schema::connection('inumet')->dropIfExists('stations');
    }
};
