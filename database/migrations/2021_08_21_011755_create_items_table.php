<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('everypurchase')->create('items', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('provider')->nullable();
            $table->text('variation')->nullable();
            $table->string('quantity')->nullable();
            $table->string('amount_per_unit')->nullable();
            $table->string('total_amount')->nullable();
            $table->unsignedBigInteger('purchase_id');

            $table->foreign('purchase_id')->references('id')->on('purchases');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('everypurchase')->dropIfExists('items');
    }
}
