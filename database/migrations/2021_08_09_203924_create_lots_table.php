<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('everylot')->create('lots', function (Blueprint $table) {
            $table->id();
            $table->string('lat');
            $table->string('long');
            $table->string('punto_wkb');
            $table->string('codigo_postal');
            $table->string('nombre_via');
            $table->string('num_puerta');
            $table->string('letra_puerta');
            $table->string('km');
            $table->string('manzana');
            $table->string('solar');
            $table->string('nombre_inmueble');
            $table->string('localidad');
            $table->string('departamento');
            // Google Maps
            $table->boolean('maps_tried')->default(0);
            $table->string('geocode_lat')->nullable();
            $table->string('geocode_long')->nullable();
            // $table->string('panoid')->nullable();
            // Twitter
            $table->string('user_id')->nullable();
            $table->string('username')->nullable();
            $table->string('tweet_id')->nullable();
            $table->timestamp('tweeted_at')->nullable();

            $table->index('punto_wkb');
            $table->unique(['lat', 'long']);
            $table->index(['num_puerta', 'letra_puerta']);
            $table->index(['tweeted_at', 'maps_tried', 'departamento']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('everylot')->dropIfExists('lots');
    }
}
